<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="dots" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="silk_top" color="7" fill="1" visible="no" active="no"/>
<layer number="135" name="silk_bottom" color="7" fill="1" visible="no" active="no"/>
<layer number="136" name="silktop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="silkbottom" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="DrillLegend_01-20" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SparkFun-PowerSymbols" urn="urn:adsk.eagle:library:530">
<description>&lt;h3&gt;SparkFun Power Symbols&lt;/h3&gt;
This library contains power, ground, and voltage-supply symbols.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="DGND" urn="urn:adsk.eagle:symbol:39415/1" library_version="1">
<description>&lt;h3&gt;Digital Ground Supply&lt;/h3&gt;</description>
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<text x="0" y="-0.254" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:39439/1" prefix="GND" library_version="1">
<description>&lt;h3&gt;Ground Supply Symbol&lt;/h3&gt;
&lt;p&gt;Generic signal ground supply symbol.&lt;/p&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Power Distribution Board">
<packages>
<package name="DO-214AA" urn="urn:adsk.eagle:footprint:32501/1">
<wire x1="2.16" y1="-1.78" x2="-2.14" y2="-1.78" width="0.2032" layer="21"/>
<wire x1="-2.14" y1="-1.78" x2="-2.16" y2="-1.78" width="0.2032" layer="21"/>
<wire x1="-2.16" y1="-1.78" x2="-2.16" y2="1.78" width="0.2032" layer="51"/>
<wire x1="-2.16" y1="1.78" x2="-2.14" y2="1.78" width="0.2032" layer="21"/>
<wire x1="-2.14" y1="1.78" x2="2.16" y2="1.78" width="0.2032" layer="21"/>
<wire x1="2.16" y1="1.78" x2="2.16" y2="-1.78" width="0.2032" layer="51"/>
<wire x1="0.508" y1="0" x2="0.381" y2="-0.127" width="0.127" layer="21"/>
<wire x1="0.381" y1="-0.127" x2="0.254" y2="-0.254" width="0.127" layer="21"/>
<wire x1="0.254" y1="-0.254" x2="0.127" y2="-0.381" width="0.127" layer="21"/>
<wire x1="-0.381" y1="-0.889" x2="-0.508" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-1.016" x2="-0.508" y2="1.016" width="0.127" layer="21"/>
<wire x1="-0.508" y1="1.016" x2="-0.381" y2="0.889" width="0.127" layer="21"/>
<wire x1="-0.381" y1="0.889" x2="-0.254" y2="0.762" width="0.127" layer="21"/>
<wire x1="-0.381" y1="0.889" x2="-0.381" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-0.381" y1="-0.889" x2="-0.254" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-0.762" x2="-0.127" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-0.762" x2="-0.254" y2="0.762" width="0.127" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.127" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.127" y1="0.635" x2="0" y2="0.508" width="0.127" layer="21"/>
<wire x1="-0.127" y1="0.635" x2="-0.127" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-0.127" y1="-0.635" x2="0" y2="-0.508" width="0.127" layer="21"/>
<wire x1="0" y1="-0.508" x2="0.127" y2="-0.381" width="0.127" layer="21"/>
<wire x1="0" y1="-0.508" x2="0" y2="0.508" width="0.127" layer="21"/>
<wire x1="0" y1="0.508" x2="0.127" y2="0.381" width="0.127" layer="21"/>
<wire x1="0.127" y1="0.381" x2="0.254" y2="0.254" width="0.127" layer="21"/>
<wire x1="0.127" y1="0.381" x2="0.127" y2="-0.381" width="0.127" layer="21"/>
<wire x1="0.254" y1="-0.254" x2="0.254" y2="0.254" width="0.127" layer="21"/>
<wire x1="0.254" y1="0.254" x2="0.381" y2="0.127" width="0.127" layer="21"/>
<wire x1="0.381" y1="0.127" x2="0.508" y2="0" width="0.127" layer="21"/>
<wire x1="0.381" y1="0.127" x2="0.381" y2="-0.127" width="0.127" layer="21"/>
<wire x1="2.16" y1="-1.78" x2="2.16" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="2.16" y1="1.52" x2="2.16" y2="1.78" width="0.2032" layer="21"/>
<wire x1="-2.14" y1="1.52" x2="-2.14" y2="1.78" width="0.2032" layer="21"/>
<wire x1="-2.14" y1="-1.78" x2="-2.14" y2="-1.5" width="0.2032" layer="21"/>
<rectangle x1="-3.429" y1="-1.905" x2="3.429" y2="1.905" layer="39" rot="R180"/>
<smd name="+" x="-2.21" y="0" dx="2.743" dy="2.159" layer="1" rot="R270"/>
<smd name="-" x="2.21" y="0" dx="2.743" dy="2.159" layer="1" rot="R270"/>
<text x="-2.159" y="2.032" size="0.889" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.651" y="-2.794" size="0.635" layer="27" ratio="10">&gt;VALUE</text>
<text x="1.27" y="0" size="0.5" layer="33" ratio="10" rot="R180">&gt;NAME</text>
</package>
<package name="TO-252">
<wire x1="0.508" y1="3.556" x2="0.508" y2="3.048" width="0.127" layer="51"/>
<wire x1="0.508" y1="3.048" x2="0.508" y2="1.524" width="0.127" layer="51"/>
<wire x1="0.508" y1="1.524" x2="0.508" y2="0.508" width="0.127" layer="51"/>
<wire x1="0.508" y1="0.508" x2="0.508" y2="-0.508" width="0.127" layer="51"/>
<wire x1="0.508" y1="-0.508" x2="0.508" y2="-1.524" width="0.127" layer="51"/>
<wire x1="0.508" y1="-1.524" x2="0.508" y2="-3.048" width="0.127" layer="51"/>
<wire x1="0.508" y1="-3.048" x2="0.508" y2="-3.556" width="0.127" layer="51"/>
<wire x1="0.508" y1="-3.048" x2="4.064" y2="-3.048" width="0.127" layer="51"/>
<wire x1="4.064" y1="-3.048" x2="4.064" y2="-1.524" width="0.127" layer="51"/>
<wire x1="4.064" y1="-1.524" x2="0.508" y2="-1.524" width="0.127" layer="51"/>
<wire x1="0.508" y1="1.524" x2="4.064" y2="1.524" width="0.127" layer="51"/>
<wire x1="4.064" y1="1.524" x2="4.064" y2="3.048" width="0.127" layer="51"/>
<wire x1="4.064" y1="3.048" x2="0.508" y2="3.048" width="0.127" layer="51"/>
<wire x1="0.508" y1="0.508" x2="1.016" y2="0.508" width="0.127" layer="51"/>
<wire x1="1.016" y1="0.508" x2="1.016" y2="-0.508" width="0.127" layer="51"/>
<wire x1="1.016" y1="-0.508" x2="0.508" y2="-0.508" width="0.127" layer="51"/>
<wire x1="-5.334" y1="3.556" x2="-6.096" y2="2.794" width="0.127" layer="51"/>
<wire x1="-6.096" y1="2.794" x2="-6.096" y2="-2.794" width="0.127" layer="51"/>
<wire x1="-6.096" y1="-2.794" x2="-5.334" y2="-3.556" width="0.127" layer="51"/>
<wire x1="0.508" y1="3.556" x2="-5.334" y2="3.556" width="0.127" layer="21"/>
<wire x1="-5.334" y1="-3.556" x2="0.508" y2="-3.556" width="0.127" layer="21"/>
<wire x1="4.826" y1="3.302" x2="0.508" y2="3.302" width="0.127" layer="39"/>
<wire x1="0.508" y1="3.302" x2="0.508" y2="3.556" width="0.127" layer="39"/>
<wire x1="0.508" y1="3.556" x2="-7.112" y2="3.556" width="0.127" layer="39"/>
<wire x1="-7.112" y1="3.556" x2="-7.112" y2="-3.556" width="0.127" layer="39"/>
<wire x1="-7.112" y1="-3.556" x2="0.508" y2="-3.556" width="0.127" layer="39"/>
<wire x1="0.508" y1="-3.556" x2="0.508" y2="-3.302" width="0.127" layer="39"/>
<wire x1="0.508" y1="-3.302" x2="4.826" y2="-3.302" width="0.127" layer="39"/>
<wire x1="4.826" y1="-3.302" x2="4.826" y2="-1.27" width="0.127" layer="39"/>
<wire x1="4.826" y1="-1.27" x2="1.524" y2="-1.27" width="0.127" layer="39"/>
<wire x1="1.524" y1="-1.27" x2="1.524" y2="1.27" width="0.127" layer="39"/>
<wire x1="1.524" y1="1.27" x2="4.826" y2="1.27" width="0.127" layer="39"/>
<wire x1="4.826" y1="1.27" x2="4.826" y2="3.302" width="0.127" layer="39"/>
<smd name="P$2" x="-3.5" y="0" dx="6.7" dy="6.7" layer="1"/>
<smd name="P$3" x="3.1" y="2.3" dx="3" dy="1.6" layer="1"/>
<smd name="P$1" x="3.1" y="-2.3" dx="3" dy="1.6" layer="1"/>
<text x="-7" y="6.5" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-7" y="4.5" size="1.27" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="FUSE_HOLDER">
<wire x1="-7.62" y1="-3.81" x2="8.38" y2="-3.81" width="0.127" layer="21"/>
<wire x1="8.38" y1="-3.81" x2="8.38" y2="2.92" width="0.127" layer="21"/>
<wire x1="8.38" y1="2.92" x2="-7.62" y2="2.92" width="0.127" layer="21"/>
<wire x1="-7.62" y1="2.92" x2="-7.62" y2="-3.81" width="0.127" layer="21"/>
<pad name="1" x="-4.58" y="-2.145" drill="1.6"/>
<pad name="2" x="-4.58" y="1.255" drill="1.6"/>
<pad name="3" x="5.34" y="1.255" drill="1.6"/>
<pad name="4" x="5.35" y="-2.145" drill="1.6"/>
<text x="-7.62" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-7.62" y="-6.35" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="FUSE-CARTRIDGE">
<text x="0" y="0" size="0.508" layer="21">&gt;Name</text>
</package>
<package name="D24V150FX">
<wire x1="-21.59" y1="-17.78" x2="21.59" y2="-17.78" width="0.127" layer="21"/>
<wire x1="21.59" y1="-17.78" x2="21.59" y2="13.97" width="0.127" layer="21"/>
<wire x1="21.59" y1="13.97" x2="-21.59" y2="13.97" width="0.127" layer="21"/>
<wire x1="-21.59" y1="13.97" x2="-21.59" y2="-17.78" width="0.127" layer="21"/>
<pad name="1" x="-15.24" y="-12.7" drill="1.016"/>
<pad name="2" x="-12.7" y="-12.7" drill="1.016"/>
<pad name="3" x="-10.16" y="-12.7" drill="1.016" shape="square"/>
<pad name="4" x="-7.62" y="-12.7" drill="1.016" shape="square"/>
<pad name="5" x="-5.08" y="-15.24" drill="1.016"/>
<pad name="6" x="-2.54" y="-15.24" drill="1.016"/>
<pad name="7" x="0" y="-15.24" drill="1.016"/>
<pad name="8" x="2.54" y="-15.24" drill="1.016"/>
<pad name="9" x="5.08" y="-15.24" drill="1.016"/>
<pad name="10" x="7.62" y="-12.7" drill="1.016" shape="square"/>
<pad name="11" x="10.16" y="-12.7" drill="1.016" shape="square"/>
<pad name="12" x="12.7" y="-12.7" drill="1.016"/>
<pad name="13" x="15.24" y="-12.7" drill="1.016"/>
<hole x="-19.431" y="-13.589" drill="2.1844"/>
<hole x="19.431" y="-13.589" drill="2.1844"/>
<hole x="19.431" y="11.811" drill="2.1844"/>
<hole x="-19.431" y="11.811" drill="2.1844"/>
<text x="-1.27" y="15.24" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-20.32" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="POWERPAK_SO-8L">
<smd name="P$1" x="-1.905" y="-2.7178" dx="0.41" dy="0.72" layer="1"/>
<smd name="P$2" x="-0.635" y="-2.7178" dx="0.41" dy="0.72" layer="1"/>
<smd name="P$3" x="0.635" y="-2.7178" dx="0.41" dy="0.72" layer="1"/>
<smd name="P$4" x="1.905" y="-2.7178" dx="0.41" dy="0.72" layer="1"/>
<smd name="P$5" x="1.1176" y="1.0795" dx="1.7272" dy="3.9878" layer="1"/>
<smd name="P$6" x="-1.1176" y="1.0795" dx="1.7272" dy="3.9878" layer="1"/>
<wire x1="-3.3782" y1="3.8735" x2="-3.3782" y2="-3.8735" width="0.0254" layer="21"/>
<wire x1="-3.3782" y1="-3.8735" x2="3.3782" y2="-3.8735" width="0.0254" layer="21"/>
<wire x1="3.3782" y1="-3.8735" x2="3.3782" y2="3.8735" width="0.0254" layer="21"/>
<wire x1="3.3782" y1="3.8735" x2="-3.3782" y2="3.8735" width="0.0254" layer="21"/>
<polygon width="0.000253125" layer="1">
<vertex x="0.254" y="-0.9144"/>
<vertex x="0.254" y="3.0734"/>
<vertex x="2.4638" y="3.0734"/>
<vertex x="2.4638" y="2.667"/>
<vertex x="1.9812" y="2.667"/>
<vertex x="1.9812" y="0.254"/>
<vertex x="2.5654" y="0.254"/>
<vertex x="2.5654" y="-0.254"/>
<vertex x="1.9812" y="-0.254"/>
<vertex x="1.9812" y="-0.9144"/>
</polygon>
<polygon width="0.000253125" layer="1">
<vertex x="-0.254" y="-0.9144"/>
<vertex x="-0.254" y="3.0734"/>
<vertex x="-2.4638" y="3.0734"/>
<vertex x="-2.4638" y="2.667"/>
<vertex x="-1.9812" y="2.667"/>
<vertex x="-1.9812" y="0.254"/>
<vertex x="-2.5654" y="0.254"/>
<vertex x="-2.5654" y="-0.254"/>
<vertex x="-1.9812" y="-0.254"/>
<vertex x="-1.9812" y="-0.9144"/>
</polygon>
<polygon width="0.000253125" layer="31">
<vertex x="0.254" y="-0.9144"/>
<vertex x="0.254" y="3.0734"/>
<vertex x="2.4638" y="3.0734"/>
<vertex x="2.4638" y="2.667"/>
<vertex x="1.9812" y="2.667"/>
<vertex x="1.9812" y="0.254"/>
<vertex x="2.5654" y="0.254"/>
<vertex x="2.5654" y="-0.254"/>
<vertex x="1.9812" y="-0.254"/>
<vertex x="1.9812" y="-0.9144"/>
</polygon>
<polygon width="0.000253125" layer="31">
<vertex x="-0.254" y="-0.9144"/>
<vertex x="-0.254" y="3.0734"/>
<vertex x="-2.4638" y="3.0734"/>
<vertex x="-2.4638" y="2.667"/>
<vertex x="-1.9812" y="2.667"/>
<vertex x="-1.9812" y="0.254"/>
<vertex x="-2.5654" y="0.254"/>
<vertex x="-2.5654" y="-0.254"/>
<vertex x="-1.9812" y="-0.254"/>
<vertex x="-1.9812" y="-0.9144"/>
</polygon>
<polygon width="0.000253125" layer="29">
<vertex x="0.254" y="-0.9144"/>
<vertex x="0.254" y="3.0734"/>
<vertex x="2.4638" y="3.0734"/>
<vertex x="2.4638" y="2.667"/>
<vertex x="1.9812" y="2.667"/>
<vertex x="1.9812" y="0.254"/>
<vertex x="2.5654" y="0.254"/>
<vertex x="2.5654" y="-0.254"/>
<vertex x="1.9812" y="-0.254"/>
<vertex x="1.9812" y="-0.9144"/>
</polygon>
<polygon width="0.000253125" layer="29">
<vertex x="-0.254" y="-0.9144"/>
<vertex x="-0.254" y="3.0734"/>
<vertex x="-2.4638" y="3.0734"/>
<vertex x="-2.4638" y="2.667"/>
<vertex x="-1.9812" y="2.667"/>
<vertex x="-1.9812" y="0.254"/>
<vertex x="-2.5654" y="0.254"/>
<vertex x="-2.5654" y="-0.254"/>
<vertex x="-1.9812" y="-0.254"/>
<vertex x="-1.9812" y="-0.9144"/>
</polygon>
</package>
<package name="SC70-6_6-TSSOP_SC-88_SOT-363" urn="urn:adsk.eagle:footprint:38440/1" locally_modified="yes">
<description>&lt;h3&gt;SC-88/SC70-6/SOT-363 6-pin Package&lt;/h3&gt;
&lt;p&gt;&lt;a href="http://www.onsemi.com/pub_link/Collateral/MBT3904DW1T1-D.PDF"&gt;Example Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<circle x="-1.3335" y="-1.016" radius="0.254" width="0" layer="21"/>
<wire x1="1" y1="0.625" x2="-1" y2="0.625" width="0.127" layer="51"/>
<wire x1="-1" y1="0.625" x2="-1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.625" x2="1" y2="-0.625" width="0.127" layer="51"/>
<wire x1="1" y1="-0.625" x2="1" y2="0.625" width="0.127" layer="51"/>
<wire x1="-1.1176" y1="0.7493" x2="-1.1176" y2="-0.6223" width="0.2032" layer="21"/>
<wire x1="1.1176" y1="-0.7493" x2="1.1176" y2="0.7493" width="0.2032" layer="21"/>
<wire x1="-0.9779" y1="0.7493" x2="-1.1176" y2="0.7493" width="0.2032" layer="21"/>
<wire x1="1.1176" y1="0.7493" x2="0.9779" y2="0.7493" width="0.2032" layer="21"/>
<wire x1="-1.1176" y1="-0.6223" x2="-0.9779" y2="-0.7493" width="0.2032" layer="21"/>
<wire x1="0.9779" y1="-0.7493" x2="1.1176" y2="-0.7493" width="0.2032" layer="21"/>
<rectangle x1="-0.1016" y1="0.6858" x2="0.1016" y2="1.0414" layer="51"/>
<rectangle x1="-0.1016" y1="-1.0414" x2="0.1016" y2="-0.6858" layer="51"/>
<rectangle x1="0.5484" y1="0.6858" x2="0.7516" y2="1.0414" layer="51"/>
<rectangle x1="-0.7516" y1="0.6858" x2="-0.5484" y2="1.0414" layer="51"/>
<rectangle x1="-0.7516" y1="-1.0414" x2="-0.5484" y2="-0.6858" layer="51"/>
<rectangle x1="0.5484" y1="-1.0414" x2="0.7516" y2="-0.6858" layer="51"/>
<smd name="4" x="0.65" y="0.92" dx="0.3" dy="0.66" layer="1"/>
<smd name="2" x="0" y="-0.92" dx="0.3" dy="0.66" layer="1" rot="R180"/>
<smd name="1" x="-0.65" y="-0.92" dx="0.3" dy="0.66" layer="1" rot="R180"/>
<smd name="3" x="0.65" y="-0.92" dx="0.3" dy="0.66" layer="1" rot="R180"/>
<smd name="5" x="0" y="0.92" dx="0.3" dy="0.66" layer="1"/>
<smd name="6" x="-0.65" y="0.92" dx="0.3" dy="0.66" layer="1"/>
<text x="0" y="1.397" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.397" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="R0805" urn="urn:adsk.eagle:footprint:5829373/1">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.762" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="PHX-2X1">
<wire x1="-6.35" y1="-10.16" x2="4.61" y2="-10.16" width="0.127" layer="21"/>
<wire x1="4.61" y1="-10.16" x2="4.61" y2="7.84" width="0.127" layer="21"/>
<wire x1="4.61" y1="7.84" x2="-6.35" y2="7.84" width="0.127" layer="21"/>
<wire x1="-6.35" y1="7.84" x2="-6.35" y2="-10.16" width="0.127" layer="21"/>
<pad name="P$1" x="-3.41" y="-3.11" drill="1.4"/>
<pad name="P$2" x="1.67" y="-3.11" drill="1.4"/>
<text x="-3.81" y="8.89" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-12.7" size="1.27" layer="27">&gt;VALUE</text>
<pad name="P$3" x="1.67" y="5.59" drill="1.4"/>
<pad name="P$4" x="-3.41" y="5.59" drill="1.4"/>
</package>
<package name="EX-PACKAGE">
<description>&lt;b&gt;EX-Package&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.35" y="0.5" dx="1.27" dy="0.8" layer="1"/>
<smd name="2" x="-1.35" y="-0.5" dx="1.27" dy="0.8" layer="1"/>
<smd name="3" x="0.15" y="-1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="4" x="0.65" y="-1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="9" x="0.65" y="1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<smd name="10" x="0.15" y="1.45" dx="0.85" dy="0.3" layer="1" rot="R90"/>
<text x="-0.37331875" y="2.605040625" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-0.37331875" y="-2.474959375" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.254" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.254" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.254" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="1.5" width="0.254" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="-0.3" y2="1.5" width="0.254" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-0.3" y2="-1.5" width="0.254" layer="21"/>
<wire x1="1.5" y1="1.5" x2="1.12" y2="1.5" width="0.254" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="1.12" y2="-1.5" width="0.254" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="1.225" width="0.254" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="-1.225" width="0.254" layer="21"/>
<circle x="-2.197" y="0.513" radius="0.029" width="0.254" layer="25"/>
<smd name="5" x="1.35" y="-0.75" dx="0.85" dy="0.3" layer="1" rot="R180"/>
<smd name="6" x="1.37" y="-0.25" dx="0.85" dy="0.3" layer="1" rot="R180"/>
<smd name="7" x="1.37" y="0.25" dx="0.85" dy="0.3" layer="1" rot="R180"/>
<smd name="8" x="1.37" y="0.75" dx="0.85" dy="0.3" layer="1" rot="R180"/>
</package>
<package name="LED_DISPLAY_MODULE_LDT-N2804RI">
<wire x1="-11.265" y1="5" x2="-11.265" y2="-5" width="0.127" layer="21"/>
<wire x1="-11.265" y1="-5" x2="11.265" y2="-5" width="0.127" layer="21"/>
<wire x1="11.265" y1="-5" x2="11.265" y2="5" width="0.127" layer="21"/>
<wire x1="11.265" y1="5" x2="-11.265" y2="5" width="0.127" layer="21"/>
<pad name="P$1" x="-6.35" y="-3.81" drill="0.6"/>
<pad name="P$2" x="-3.81" y="-3.81" drill="0.6"/>
<pad name="P$3" x="-1.27" y="-3.81" drill="0.6"/>
<pad name="P$4" x="1.27" y="-3.81" drill="0.6"/>
<pad name="P$5" x="3.81" y="-3.81" drill="0.6"/>
<pad name="P$7" x="6.35" y="3.81" drill="0.6"/>
<pad name="P$8" x="3.81" y="3.81" drill="0.6"/>
<pad name="P$9" x="1.27" y="3.81" drill="0.6"/>
<pad name="P$10" x="-1.27" y="3.81" drill="0.6"/>
<pad name="P$11" x="-3.81" y="3.81" drill="0.6"/>
<pad name="P$12" x="-6.35" y="3.81" drill="0.6"/>
</package>
<package name="SOIC20R" urn="urn:adsk.eagle:footprint:3858/1">
<description>&lt;b&gt;20-LeadStandard Small Outline Package SOIC Wide Body (R-20)&lt;/b&gt;&lt;p&gt;
Source: http://www.analog.com/static/imported-files/data_sheets/AD811.pdf</description>
<wire x1="6.41" y1="-3.7" x2="-6.41" y2="-3.7" width="0.2032" layer="51"/>
<wire x1="-6.41" y1="-3.7" x2="-6.41" y2="-2.775" width="0.2032" layer="21"/>
<wire x1="-6.41" y1="-2.775" x2="-6.41" y2="3.7" width="0.2032" layer="21"/>
<wire x1="-6.41" y1="3.7" x2="6.41" y2="3.7" width="0.2032" layer="51"/>
<wire x1="6.41" y1="-2.775" x2="-6.41" y2="-2.775" width="0.2032" layer="21"/>
<wire x1="6.41" y1="3.7" x2="6.41" y2="-2.775" width="0.2032" layer="21"/>
<wire x1="6.41" y1="-2.775" x2="6.41" y2="-3.7" width="0.2032" layer="21"/>
<smd name="2" x="-4.445" y="-4.775" dx="0.6" dy="2.2" layer="1"/>
<smd name="13" x="3.175" y="4.8" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-5.715" y="-4.775" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="-3.175" y="-4.775" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="-1.905" y="-4.775" dx="0.6" dy="2.2" layer="1"/>
<smd name="14" x="1.905" y="4.8" dx="0.6" dy="2.2" layer="1"/>
<smd name="12" x="4.445" y="4.8" dx="0.6" dy="2.2" layer="1"/>
<smd name="11" x="5.715" y="4.8" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="0.635" y="-4.775" dx="0.6" dy="2.2" layer="1"/>
<smd name="9" x="4.445" y="-4.8" dx="0.6" dy="2.2" layer="1" rot="R180"/>
<smd name="5" x="-0.635" y="-4.775" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="1.905" y="-4.775" dx="0.6" dy="2.2" layer="1"/>
<smd name="10" x="5.715" y="-4.8" dx="0.6" dy="2.2" layer="1" rot="R180"/>
<smd name="8" x="3.175" y="-4.775" dx="0.6" dy="2.2" layer="1"/>
<smd name="15" x="0.635" y="4.8" dx="0.6" dy="2.2" layer="1"/>
<smd name="16" x="-0.635" y="4.8" dx="0.6" dy="2.2" layer="1"/>
<smd name="17" x="-1.905" y="4.8" dx="0.6" dy="2.2" layer="1"/>
<smd name="18" x="-3.175" y="4.8" dx="0.6" dy="2.2" layer="1"/>
<smd name="19" x="-4.445" y="4.8" dx="0.6" dy="2.2" layer="1"/>
<smd name="20" x="-5.715" y="4.8" dx="0.6" dy="2.2" layer="1"/>
<text x="-6.985" y="-3.175" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="-5.715" y="-0.635" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.96" y1="-5.275" x2="-5.47" y2="-3.8" layer="51"/>
<rectangle x1="-4.69" y1="-5.275" x2="-4.2" y2="-3.8" layer="51"/>
<rectangle x1="-3.42" y1="-5.275" x2="-2.93" y2="-3.8" layer="51"/>
<rectangle x1="-2.15" y1="-5.275" x2="-1.66" y2="-3.8" layer="51"/>
<rectangle x1="5.47" y1="3.8" x2="5.96" y2="5.3" layer="51"/>
<rectangle x1="4.2" y1="3.8" x2="4.69" y2="5.3" layer="51"/>
<rectangle x1="2.93" y1="3.8" x2="3.42" y2="5.3" layer="51"/>
<rectangle x1="1.66" y1="3.8" x2="2.15" y2="5.3" layer="51"/>
<rectangle x1="-0.88" y1="-5.275" x2="-0.39" y2="-3.8" layer="51"/>
<rectangle x1="0.39" y1="-5.275" x2="0.88" y2="-3.8" layer="51"/>
<rectangle x1="1.66" y1="-5.275" x2="2.15" y2="-3.8" layer="51"/>
<rectangle x1="2.93" y1="-5.275" x2="3.42" y2="-4.175" layer="51"/>
<rectangle x1="4.2" y1="-5.3" x2="4.69" y2="-3.8" layer="51" rot="R180"/>
<rectangle x1="5.47" y1="-5.3" x2="5.96" y2="-3.8" layer="51" rot="R180"/>
<rectangle x1="0.39" y1="3.8" x2="0.88" y2="5.3" layer="51"/>
<rectangle x1="-0.88" y1="3.8" x2="-0.39" y2="5.3" layer="51"/>
<rectangle x1="-2.15" y1="3.8" x2="-1.66" y2="5.3" layer="51"/>
<rectangle x1="-3.42" y1="3.8" x2="-2.93" y2="5.3" layer="51"/>
<rectangle x1="-4.69" y1="3.8" x2="-4.2" y2="5.3" layer="51"/>
<rectangle x1="-5.96" y1="3.8" x2="-5.47" y2="5.3" layer="51"/>
</package>
<package name="QFP50P1200X1200X160-64N">
<wire x1="-4.2164" y1="5.0038" x2="-5.0038" y2="5.0038" width="0.1524" layer="21"/>
<wire x1="5.0038" y1="4.2164" x2="5.0038" y2="5.0038" width="0.1524" layer="21"/>
<wire x1="4.2164" y1="-5.0038" x2="5.0038" y2="-5.0038" width="0.1524" layer="21"/>
<wire x1="-5.0038" y1="-5.0038" x2="-4.2164" y2="-5.0038" width="0.1524" layer="21"/>
<wire x1="5.0038" y1="-5.0038" x2="5.0038" y2="-4.2164" width="0.1524" layer="21"/>
<wire x1="5.0038" y1="5.0038" x2="4.2164" y2="5.0038" width="0.1524" layer="21"/>
<wire x1="-5.0038" y1="5.0038" x2="-5.0038" y2="4.2164" width="0.1524" layer="21"/>
<wire x1="-5.0038" y1="-4.2164" x2="-5.0038" y2="-5.0038" width="0.1524" layer="21"/>
<text x="-5.61893125" y="-10.4751" size="2.08485" layer="21" ratio="10" rot="SR0">&gt;NAME</text>
<wire x1="-5.0038" y1="3.6068" x2="-5.0038" y2="3.7338" width="0" layer="51"/>
<wire x1="-5.0038" y1="3.7338" x2="-5.0038" y2="3.8862" width="0" layer="51"/>
<wire x1="-5.0038" y1="-5.0038" x2="-3.8862" y2="-5.0038" width="0" layer="51"/>
<wire x1="-3.6068" y1="-5.0038" x2="-3.3782" y2="-5.0038" width="0" layer="51"/>
<wire x1="-3.1242" y1="-5.0038" x2="-2.8956" y2="-5.0038" width="0" layer="51"/>
<wire x1="-2.6162" y1="-5.0038" x2="-2.3876" y2="-5.0038" width="0" layer="51"/>
<wire x1="-2.1082" y1="-5.0038" x2="-1.8796" y2="-5.0038" width="0" layer="51"/>
<wire x1="-1.6256" y1="-5.0038" x2="-1.397" y2="-5.0038" width="0" layer="51"/>
<wire x1="-1.1176" y1="-5.0038" x2="-0.889" y2="-5.0038" width="0" layer="51"/>
<wire x1="-0.6096" y1="-5.0038" x2="-0.381" y2="-5.0038" width="0" layer="51"/>
<wire x1="-0.127" y1="-5.0038" x2="0.127" y2="-5.0038" width="0" layer="51"/>
<wire x1="0.381" y1="-5.0038" x2="0.6096" y2="-5.0038" width="0" layer="51"/>
<wire x1="0.889" y1="-5.0038" x2="1.1176" y2="-5.0038" width="0" layer="51"/>
<wire x1="1.397" y1="-5.0038" x2="1.6256" y2="-5.0038" width="0" layer="51"/>
<wire x1="1.8796" y1="-5.0038" x2="2.1082" y2="-5.0038" width="0" layer="51"/>
<wire x1="2.3876" y1="-5.0038" x2="2.6162" y2="-5.0038" width="0" layer="51"/>
<wire x1="2.8956" y1="-5.0038" x2="3.1242" y2="-5.0038" width="0" layer="51"/>
<wire x1="3.3782" y1="-5.0038" x2="3.6068" y2="-5.0038" width="0" layer="51"/>
<wire x1="3.8862" y1="-5.0038" x2="5.0038" y2="-5.0038" width="0" layer="51"/>
<wire x1="5.0038" y1="-5.0038" x2="5.0038" y2="-3.8862" width="0" layer="51"/>
<wire x1="5.0038" y1="-3.6068" x2="5.0038" y2="-3.3782" width="0" layer="51"/>
<wire x1="5.0038" y1="-3.1242" x2="5.0038" y2="-2.8956" width="0" layer="51"/>
<wire x1="5.0038" y1="-2.6162" x2="5.0038" y2="-2.3876" width="0" layer="51"/>
<wire x1="5.0038" y1="-2.1082" x2="5.0038" y2="-1.8796" width="0" layer="51"/>
<wire x1="5.0038" y1="-1.6256" x2="5.0038" y2="-1.397" width="0" layer="51"/>
<wire x1="5.0038" y1="-1.1176" x2="5.0038" y2="-0.889" width="0" layer="51"/>
<wire x1="5.0038" y1="-0.6096" x2="5.0038" y2="-0.381" width="0" layer="51"/>
<wire x1="5.0038" y1="-0.127" x2="5.0038" y2="0.127" width="0" layer="51"/>
<wire x1="5.0038" y1="0.381" x2="5.0038" y2="0.6096" width="0" layer="51"/>
<wire x1="5.0038" y1="0.889" x2="5.0038" y2="1.1176" width="0" layer="51"/>
<wire x1="5.0038" y1="1.397" x2="5.0038" y2="1.6256" width="0" layer="51"/>
<wire x1="5.0038" y1="1.8796" x2="5.0038" y2="2.1082" width="0" layer="51"/>
<wire x1="5.0038" y1="2.3876" x2="5.0038" y2="2.6162" width="0" layer="51"/>
<wire x1="5.0038" y1="2.8956" x2="5.0038" y2="3.1242" width="0" layer="51"/>
<wire x1="5.0038" y1="3.3782" x2="5.0038" y2="3.6068" width="0" layer="51"/>
<wire x1="5.0038" y1="3.8862" x2="5.0038" y2="5.0038" width="0" layer="51"/>
<wire x1="5.0038" y1="5.0038" x2="3.8862" y2="5.0038" width="0" layer="51"/>
<wire x1="3.6068" y1="5.0038" x2="3.3782" y2="5.0038" width="0" layer="51"/>
<wire x1="3.1242" y1="5.0038" x2="2.8956" y2="5.0038" width="0" layer="51"/>
<wire x1="2.6162" y1="5.0038" x2="2.3876" y2="5.0038" width="0" layer="51"/>
<wire x1="2.1082" y1="5.0038" x2="1.8796" y2="5.0038" width="0" layer="51"/>
<wire x1="1.6256" y1="5.0038" x2="1.397" y2="5.0038" width="0" layer="51"/>
<wire x1="1.1176" y1="5.0038" x2="0.889" y2="5.0038" width="0" layer="51"/>
<wire x1="0.6096" y1="5.0038" x2="0.381" y2="5.0038" width="0" layer="51"/>
<wire x1="0.127" y1="5.0038" x2="-0.127" y2="5.0038" width="0" layer="51"/>
<wire x1="-0.381" y1="5.0038" x2="-0.6096" y2="5.0038" width="0" layer="51"/>
<wire x1="-0.889" y1="5.0038" x2="-1.1176" y2="5.0038" width="0" layer="51"/>
<wire x1="-1.397" y1="5.0038" x2="-1.6256" y2="5.0038" width="0" layer="51"/>
<wire x1="-1.8796" y1="5.0038" x2="-2.1082" y2="5.0038" width="0" layer="51"/>
<wire x1="-2.3876" y1="5.0038" x2="-2.6162" y2="5.0038" width="0" layer="51"/>
<wire x1="-2.8956" y1="5.0038" x2="-3.1242" y2="5.0038" width="0" layer="51"/>
<wire x1="-3.3782" y1="5.0038" x2="-3.6068" y2="5.0038" width="0" layer="51"/>
<wire x1="-3.8862" y1="5.0038" x2="-5.0038" y2="5.0038" width="0" layer="51"/>
<wire x1="-5.0038" y1="5.0038" x2="-5.0038" y2="3.8862" width="0" layer="51"/>
<wire x1="-5.0038" y1="3.6068" x2="-5.0038" y2="3.3782" width="0" layer="51"/>
<wire x1="-5.0038" y1="3.1242" x2="-5.0038" y2="2.8956" width="0" layer="51"/>
<wire x1="-5.0038" y1="2.6162" x2="-5.0038" y2="2.3876" width="0" layer="51"/>
<wire x1="-5.0038" y1="2.1082" x2="-5.0038" y2="1.8796" width="0" layer="51"/>
<wire x1="-5.0038" y1="1.6256" x2="-5.0038" y2="1.397" width="0" layer="51"/>
<wire x1="-5.0038" y1="1.1176" x2="-5.0038" y2="0.889" width="0" layer="51"/>
<wire x1="-5.0038" y1="0.6096" x2="-5.0038" y2="0.381" width="0" layer="51"/>
<wire x1="-5.0038" y1="0.127" x2="-5.0038" y2="-0.127" width="0" layer="51"/>
<wire x1="-5.0038" y1="-0.381" x2="-5.0038" y2="-0.6096" width="0" layer="51"/>
<wire x1="-5.0038" y1="-0.889" x2="-5.0038" y2="-1.1176" width="0" layer="51"/>
<wire x1="-5.0038" y1="-1.397" x2="-5.0038" y2="-1.6256" width="0" layer="51"/>
<wire x1="-5.0038" y1="-1.8796" x2="-5.0038" y2="-2.1082" width="0" layer="51"/>
<wire x1="-5.0038" y1="-2.3876" x2="-5.0038" y2="-2.6162" width="0" layer="51"/>
<wire x1="-5.0038" y1="-2.8956" x2="-5.0038" y2="-3.1242" width="0" layer="51"/>
<wire x1="-5.0038" y1="-3.3782" x2="-5.0038" y2="-3.6068" width="0" layer="51"/>
<wire x1="-5.0038" y1="-3.8862" x2="-5.0038" y2="-5.0038" width="0" layer="51"/>
<wire x1="3.6068" y1="5.0038" x2="3.8862" y2="5.0038" width="0" layer="51"/>
<wire x1="3.8862" y1="5.0038" x2="3.8862" y2="5.9944" width="0" layer="51"/>
<wire x1="3.8862" y1="5.9944" x2="3.6068" y2="5.9944" width="0" layer="51"/>
<wire x1="3.6068" y1="5.9944" x2="3.6068" y2="5.0038" width="0" layer="51"/>
<wire x1="3.1242" y1="5.0038" x2="3.3782" y2="5.0038" width="0" layer="51"/>
<wire x1="3.3782" y1="5.0038" x2="3.3782" y2="5.9944" width="0" layer="51"/>
<wire x1="3.3782" y1="5.9944" x2="3.1242" y2="5.9944" width="0" layer="51"/>
<wire x1="3.1242" y1="5.9944" x2="3.1242" y2="5.0038" width="0" layer="51"/>
<wire x1="2.6162" y1="5.0038" x2="2.8956" y2="5.0038" width="0" layer="51"/>
<wire x1="2.8956" y1="5.0038" x2="2.8956" y2="5.9944" width="0" layer="51"/>
<wire x1="2.8956" y1="5.9944" x2="2.6162" y2="5.9944" width="0" layer="51"/>
<wire x1="2.6162" y1="5.9944" x2="2.6162" y2="5.0038" width="0" layer="51"/>
<wire x1="2.1082" y1="5.0038" x2="2.3876" y2="5.0038" width="0" layer="51"/>
<wire x1="2.3876" y1="5.0038" x2="2.3876" y2="5.9944" width="0" layer="51"/>
<wire x1="2.3876" y1="5.9944" x2="2.1082" y2="5.9944" width="0" layer="51"/>
<wire x1="2.1082" y1="5.9944" x2="2.1082" y2="5.0038" width="0" layer="51"/>
<wire x1="1.6256" y1="5.0038" x2="1.8796" y2="5.0038" width="0" layer="51"/>
<wire x1="1.8796" y1="5.0038" x2="1.8796" y2="5.9944" width="0" layer="51"/>
<wire x1="1.8796" y1="5.9944" x2="1.6256" y2="5.9944" width="0" layer="51"/>
<wire x1="1.6256" y1="5.9944" x2="1.6256" y2="5.0038" width="0" layer="51"/>
<wire x1="1.1176" y1="5.0038" x2="1.397" y2="5.0038" width="0" layer="51"/>
<wire x1="1.397" y1="5.0038" x2="1.397" y2="5.9944" width="0" layer="51"/>
<wire x1="1.397" y1="5.9944" x2="1.1176" y2="5.9944" width="0" layer="51"/>
<wire x1="1.1176" y1="5.9944" x2="1.1176" y2="5.0038" width="0" layer="51"/>
<wire x1="0.6096" y1="5.0038" x2="0.889" y2="5.0038" width="0" layer="51"/>
<wire x1="0.889" y1="5.0038" x2="0.889" y2="5.9944" width="0" layer="51"/>
<wire x1="0.889" y1="5.9944" x2="0.6096" y2="5.9944" width="0" layer="51"/>
<wire x1="0.6096" y1="5.9944" x2="0.6096" y2="5.0038" width="0" layer="51"/>
<wire x1="0.127" y1="5.0038" x2="0.381" y2="5.0038" width="0" layer="51"/>
<wire x1="0.381" y1="5.0038" x2="0.381" y2="5.9944" width="0" layer="51"/>
<wire x1="0.381" y1="5.9944" x2="0.127" y2="5.9944" width="0" layer="51"/>
<wire x1="0.127" y1="5.9944" x2="0.127" y2="5.0038" width="0" layer="51"/>
<wire x1="-0.381" y1="5.0038" x2="-0.127" y2="5.0038" width="0" layer="51"/>
<wire x1="-0.127" y1="5.0038" x2="-0.127" y2="5.9944" width="0" layer="51"/>
<wire x1="-0.127" y1="5.9944" x2="-0.381" y2="5.9944" width="0" layer="51"/>
<wire x1="-0.381" y1="5.9944" x2="-0.381" y2="5.0038" width="0" layer="51"/>
<wire x1="-0.889" y1="5.0038" x2="-0.6096" y2="5.0038" width="0" layer="51"/>
<wire x1="-0.6096" y1="5.0038" x2="-0.6096" y2="5.9944" width="0" layer="51"/>
<wire x1="-0.6096" y1="5.9944" x2="-0.889" y2="5.9944" width="0" layer="51"/>
<wire x1="-0.889" y1="5.9944" x2="-0.889" y2="5.0038" width="0" layer="51"/>
<wire x1="-1.397" y1="5.0038" x2="-1.1176" y2="5.0038" width="0" layer="51"/>
<wire x1="-1.1176" y1="5.0038" x2="-1.1176" y2="5.9944" width="0" layer="51"/>
<wire x1="-1.1176" y1="5.9944" x2="-1.397" y2="5.9944" width="0" layer="51"/>
<wire x1="-1.397" y1="5.9944" x2="-1.397" y2="5.0038" width="0" layer="51"/>
<wire x1="-1.8796" y1="5.0038" x2="-1.6256" y2="5.0038" width="0" layer="51"/>
<wire x1="-1.6256" y1="5.0038" x2="-1.6256" y2="5.9944" width="0" layer="51"/>
<wire x1="-1.6256" y1="5.9944" x2="-1.8796" y2="5.9944" width="0" layer="51"/>
<wire x1="-1.8796" y1="5.9944" x2="-1.8796" y2="5.0038" width="0" layer="51"/>
<wire x1="-2.3876" y1="5.0038" x2="-2.1082" y2="5.0038" width="0" layer="51"/>
<wire x1="-2.1082" y1="5.0038" x2="-2.1082" y2="5.9944" width="0" layer="51"/>
<wire x1="-2.1082" y1="5.9944" x2="-2.3876" y2="5.9944" width="0" layer="51"/>
<wire x1="-2.3876" y1="5.9944" x2="-2.3876" y2="5.0038" width="0" layer="51"/>
<wire x1="-2.8956" y1="5.0038" x2="-2.6162" y2="5.0038" width="0" layer="51"/>
<wire x1="-2.6162" y1="5.0038" x2="-2.6162" y2="5.9944" width="0" layer="51"/>
<wire x1="-2.6162" y1="5.9944" x2="-2.8956" y2="5.9944" width="0" layer="51"/>
<wire x1="-2.8956" y1="5.9944" x2="-2.8956" y2="5.0038" width="0" layer="51"/>
<wire x1="-3.3782" y1="5.0038" x2="-3.1242" y2="5.0038" width="0" layer="51"/>
<wire x1="-3.1242" y1="5.0038" x2="-3.1242" y2="5.9944" width="0" layer="51"/>
<wire x1="-3.1242" y1="5.9944" x2="-3.3782" y2="5.9944" width="0" layer="51"/>
<wire x1="-3.3782" y1="5.9944" x2="-3.3782" y2="5.0038" width="0" layer="51"/>
<wire x1="-3.8862" y1="5.0038" x2="-3.7338" y2="5.0038" width="0" layer="51"/>
<wire x1="-3.7338" y1="5.0038" x2="-3.6068" y2="5.0038" width="0" layer="51"/>
<wire x1="-3.6068" y1="5.0038" x2="-3.6068" y2="5.9944" width="0" layer="51"/>
<wire x1="-3.6068" y1="5.9944" x2="-3.8862" y2="5.9944" width="0" layer="51"/>
<wire x1="-3.8862" y1="5.9944" x2="-3.8862" y2="5.0038" width="0" layer="51"/>
<wire x1="-5.0038" y1="3.8862" x2="-5.9944" y2="3.8862" width="0" layer="51"/>
<wire x1="-5.9944" y1="3.8862" x2="-5.9944" y2="3.6068" width="0" layer="51"/>
<wire x1="-5.9944" y1="3.6068" x2="-5.0038" y2="3.6068" width="0" layer="51"/>
<wire x1="-5.0038" y1="3.1242" x2="-5.0038" y2="3.3782" width="0" layer="51"/>
<wire x1="-5.0038" y1="3.3782" x2="-5.9944" y2="3.3782" width="0" layer="51"/>
<wire x1="-5.9944" y1="3.3782" x2="-5.9944" y2="3.1242" width="0" layer="51"/>
<wire x1="-5.9944" y1="3.1242" x2="-5.0038" y2="3.1242" width="0" layer="51"/>
<wire x1="-5.0038" y1="2.6162" x2="-5.0038" y2="2.8956" width="0" layer="51"/>
<wire x1="-5.0038" y1="2.8956" x2="-5.9944" y2="2.8956" width="0" layer="51"/>
<wire x1="-5.9944" y1="2.8956" x2="-5.9944" y2="2.6162" width="0" layer="51"/>
<wire x1="-5.9944" y1="2.6162" x2="-5.0038" y2="2.6162" width="0" layer="51"/>
<wire x1="-5.0038" y1="2.1082" x2="-5.0038" y2="2.3876" width="0" layer="51"/>
<wire x1="-5.0038" y1="2.3876" x2="-5.9944" y2="2.3876" width="0" layer="51"/>
<wire x1="-5.9944" y1="2.3876" x2="-5.9944" y2="2.1082" width="0" layer="51"/>
<wire x1="-5.9944" y1="2.1082" x2="-5.0038" y2="2.1082" width="0" layer="51"/>
<wire x1="-5.0038" y1="1.6256" x2="-5.0038" y2="1.8796" width="0" layer="51"/>
<wire x1="-5.0038" y1="1.8796" x2="-5.9944" y2="1.8796" width="0" layer="51"/>
<wire x1="-5.9944" y1="1.8796" x2="-5.9944" y2="1.6256" width="0" layer="51"/>
<wire x1="-5.9944" y1="1.6256" x2="-5.0038" y2="1.6256" width="0" layer="51"/>
<wire x1="-5.0038" y1="1.1176" x2="-5.0038" y2="1.397" width="0" layer="51"/>
<wire x1="-5.0038" y1="1.397" x2="-5.9944" y2="1.397" width="0" layer="51"/>
<wire x1="-5.9944" y1="1.397" x2="-5.9944" y2="1.1176" width="0" layer="51"/>
<wire x1="-5.9944" y1="1.1176" x2="-5.0038" y2="1.1176" width="0" layer="51"/>
<wire x1="-5.0038" y1="0.6096" x2="-5.0038" y2="0.889" width="0" layer="51"/>
<wire x1="-5.0038" y1="0.889" x2="-5.9944" y2="0.889" width="0" layer="51"/>
<wire x1="-5.9944" y1="0.889" x2="-5.9944" y2="0.6096" width="0" layer="51"/>
<wire x1="-5.9944" y1="0.6096" x2="-5.0038" y2="0.6096" width="0" layer="51"/>
<wire x1="-5.0038" y1="0.127" x2="-5.0038" y2="0.381" width="0" layer="51"/>
<wire x1="-5.0038" y1="0.381" x2="-5.9944" y2="0.381" width="0" layer="51"/>
<wire x1="-5.9944" y1="0.381" x2="-5.9944" y2="0.127" width="0" layer="51"/>
<wire x1="-5.9944" y1="0.127" x2="-5.0038" y2="0.127" width="0" layer="51"/>
<wire x1="-5.0038" y1="-0.381" x2="-5.0038" y2="-0.127" width="0" layer="51"/>
<wire x1="-5.0038" y1="-0.127" x2="-5.9944" y2="-0.127" width="0" layer="51"/>
<wire x1="-5.9944" y1="-0.127" x2="-5.9944" y2="-0.381" width="0" layer="51"/>
<wire x1="-5.9944" y1="-0.381" x2="-5.0038" y2="-0.381" width="0" layer="51"/>
<wire x1="-5.0038" y1="-0.889" x2="-5.0038" y2="-0.6096" width="0" layer="51"/>
<wire x1="-5.0038" y1="-0.6096" x2="-5.9944" y2="-0.6096" width="0" layer="51"/>
<wire x1="-5.9944" y1="-0.6096" x2="-5.9944" y2="-0.889" width="0" layer="51"/>
<wire x1="-5.9944" y1="-0.889" x2="-5.0038" y2="-0.889" width="0" layer="51"/>
<wire x1="-5.0038" y1="-1.397" x2="-5.0038" y2="-1.1176" width="0" layer="51"/>
<wire x1="-5.0038" y1="-1.1176" x2="-5.9944" y2="-1.1176" width="0" layer="51"/>
<wire x1="-5.9944" y1="-1.1176" x2="-5.9944" y2="-1.397" width="0" layer="51"/>
<wire x1="-5.9944" y1="-1.397" x2="-5.0038" y2="-1.397" width="0" layer="51"/>
<wire x1="-5.0038" y1="-1.8796" x2="-5.0038" y2="-1.6256" width="0" layer="51"/>
<wire x1="-5.0038" y1="-1.6256" x2="-5.9944" y2="-1.6256" width="0" layer="51"/>
<wire x1="-5.9944" y1="-1.6256" x2="-5.9944" y2="-1.8796" width="0" layer="51"/>
<wire x1="-5.9944" y1="-1.8796" x2="-5.0038" y2="-1.8796" width="0" layer="51"/>
<wire x1="-5.0038" y1="-2.3876" x2="-5.0038" y2="-2.1082" width="0" layer="51"/>
<wire x1="-5.0038" y1="-2.1082" x2="-5.9944" y2="-2.1082" width="0" layer="51"/>
<wire x1="-5.9944" y1="-2.1082" x2="-5.9944" y2="-2.3876" width="0" layer="51"/>
<wire x1="-5.9944" y1="-2.3876" x2="-5.0038" y2="-2.3876" width="0" layer="51"/>
<wire x1="-5.0038" y1="-2.8956" x2="-5.0038" y2="-2.6162" width="0" layer="51"/>
<wire x1="-5.0038" y1="-2.6162" x2="-5.9944" y2="-2.6162" width="0" layer="51"/>
<wire x1="-5.9944" y1="-2.6162" x2="-5.9944" y2="-2.8956" width="0" layer="51"/>
<wire x1="-5.9944" y1="-2.8956" x2="-5.0038" y2="-2.8956" width="0" layer="51"/>
<wire x1="-5.0038" y1="-3.3782" x2="-5.0038" y2="-3.1242" width="0" layer="51"/>
<wire x1="-5.0038" y1="-3.1242" x2="-5.9944" y2="-3.1242" width="0" layer="51"/>
<wire x1="-5.9944" y1="-3.1242" x2="-5.9944" y2="-3.3782" width="0" layer="51"/>
<wire x1="-5.9944" y1="-3.3782" x2="-5.0038" y2="-3.3782" width="0" layer="51"/>
<wire x1="-5.0038" y1="-3.8862" x2="-5.0038" y2="-3.6068" width="0" layer="51"/>
<wire x1="-5.0038" y1="-3.6068" x2="-5.9944" y2="-3.6068" width="0" layer="51"/>
<wire x1="-5.9944" y1="-3.6068" x2="-5.9944" y2="-3.8862" width="0" layer="51"/>
<wire x1="-5.9944" y1="-3.8862" x2="-5.0038" y2="-3.8862" width="0" layer="51"/>
<wire x1="-3.6068" y1="-5.0038" x2="-3.8862" y2="-5.0038" width="0" layer="51"/>
<wire x1="-3.8862" y1="-5.0038" x2="-3.8862" y2="-5.9944" width="0" layer="51"/>
<wire x1="-3.8862" y1="-5.9944" x2="-3.6068" y2="-5.9944" width="0" layer="51"/>
<wire x1="-3.6068" y1="-5.9944" x2="-3.6068" y2="-5.0038" width="0" layer="51"/>
<wire x1="-3.1242" y1="-5.0038" x2="-3.3782" y2="-5.0038" width="0" layer="51"/>
<wire x1="-3.3782" y1="-5.0038" x2="-3.3782" y2="-5.9944" width="0" layer="51"/>
<wire x1="-3.3782" y1="-5.9944" x2="-3.1242" y2="-5.9944" width="0" layer="51"/>
<wire x1="-3.1242" y1="-5.9944" x2="-3.1242" y2="-5.0038" width="0" layer="51"/>
<wire x1="-2.6162" y1="-5.0038" x2="-2.8956" y2="-5.0038" width="0" layer="51"/>
<wire x1="-2.8956" y1="-5.0038" x2="-2.8956" y2="-5.9944" width="0" layer="51"/>
<wire x1="-2.8956" y1="-5.9944" x2="-2.6162" y2="-5.9944" width="0" layer="51"/>
<wire x1="-2.6162" y1="-5.9944" x2="-2.6162" y2="-5.0038" width="0" layer="51"/>
<wire x1="-2.1082" y1="-5.0038" x2="-2.3876" y2="-5.0038" width="0" layer="51"/>
<wire x1="-2.3876" y1="-5.0038" x2="-2.3876" y2="-5.9944" width="0" layer="51"/>
<wire x1="-2.3876" y1="-5.9944" x2="-2.1082" y2="-5.9944" width="0" layer="51"/>
<wire x1="-2.1082" y1="-5.9944" x2="-2.1082" y2="-5.0038" width="0" layer="51"/>
<wire x1="-1.6256" y1="-5.0038" x2="-1.8796" y2="-5.0038" width="0" layer="51"/>
<wire x1="-1.8796" y1="-5.0038" x2="-1.8796" y2="-5.9944" width="0" layer="51"/>
<wire x1="-1.8796" y1="-5.9944" x2="-1.6256" y2="-5.9944" width="0" layer="51"/>
<wire x1="-1.6256" y1="-5.9944" x2="-1.6256" y2="-5.0038" width="0" layer="51"/>
<wire x1="-1.1176" y1="-5.0038" x2="-1.397" y2="-5.0038" width="0" layer="51"/>
<wire x1="-1.397" y1="-5.0038" x2="-1.397" y2="-5.9944" width="0" layer="51"/>
<wire x1="-1.397" y1="-5.9944" x2="-1.1176" y2="-5.9944" width="0" layer="51"/>
<wire x1="-1.1176" y1="-5.9944" x2="-1.1176" y2="-5.0038" width="0" layer="51"/>
<wire x1="-0.6096" y1="-5.0038" x2="-0.889" y2="-5.0038" width="0" layer="51"/>
<wire x1="-0.889" y1="-5.0038" x2="-0.889" y2="-5.9944" width="0" layer="51"/>
<wire x1="-0.889" y1="-5.9944" x2="-0.6096" y2="-5.9944" width="0" layer="51"/>
<wire x1="-0.6096" y1="-5.9944" x2="-0.6096" y2="-5.0038" width="0" layer="51"/>
<wire x1="-0.127" y1="-5.0038" x2="-0.381" y2="-5.0038" width="0" layer="51"/>
<wire x1="-0.381" y1="-5.0038" x2="-0.381" y2="-5.9944" width="0" layer="51"/>
<wire x1="-0.381" y1="-5.9944" x2="-0.127" y2="-5.9944" width="0" layer="51"/>
<wire x1="-0.127" y1="-5.9944" x2="-0.127" y2="-5.0038" width="0" layer="51"/>
<wire x1="0.381" y1="-5.0038" x2="0.127" y2="-5.0038" width="0" layer="51"/>
<wire x1="0.127" y1="-5.0038" x2="0.127" y2="-5.9944" width="0" layer="51"/>
<wire x1="0.127" y1="-5.9944" x2="0.381" y2="-5.9944" width="0" layer="51"/>
<wire x1="0.381" y1="-5.9944" x2="0.381" y2="-5.0038" width="0" layer="51"/>
<wire x1="0.889" y1="-5.0038" x2="0.6096" y2="-5.0038" width="0" layer="51"/>
<wire x1="0.6096" y1="-5.0038" x2="0.6096" y2="-5.9944" width="0" layer="51"/>
<wire x1="0.6096" y1="-5.9944" x2="0.889" y2="-5.9944" width="0" layer="51"/>
<wire x1="0.889" y1="-5.9944" x2="0.889" y2="-5.0038" width="0" layer="51"/>
<wire x1="1.397" y1="-5.0038" x2="1.1176" y2="-5.0038" width="0" layer="51"/>
<wire x1="1.1176" y1="-5.0038" x2="1.1176" y2="-5.9944" width="0" layer="51"/>
<wire x1="1.1176" y1="-5.9944" x2="1.397" y2="-5.9944" width="0" layer="51"/>
<wire x1="1.397" y1="-5.9944" x2="1.397" y2="-5.0038" width="0" layer="51"/>
<wire x1="1.8796" y1="-5.0038" x2="1.6256" y2="-5.0038" width="0" layer="51"/>
<wire x1="1.6256" y1="-5.0038" x2="1.6256" y2="-5.9944" width="0" layer="51"/>
<wire x1="1.6256" y1="-5.9944" x2="1.8796" y2="-5.9944" width="0" layer="51"/>
<wire x1="1.8796" y1="-5.9944" x2="1.8796" y2="-5.0038" width="0" layer="51"/>
<wire x1="2.3876" y1="-5.0038" x2="2.1082" y2="-5.0038" width="0" layer="51"/>
<wire x1="2.1082" y1="-5.0038" x2="2.1082" y2="-5.9944" width="0" layer="51"/>
<wire x1="2.1082" y1="-5.9944" x2="2.3876" y2="-5.9944" width="0" layer="51"/>
<wire x1="2.3876" y1="-5.9944" x2="2.3876" y2="-5.0038" width="0" layer="51"/>
<wire x1="2.8956" y1="-5.0038" x2="2.6162" y2="-5.0038" width="0" layer="51"/>
<wire x1="2.6162" y1="-5.0038" x2="2.6162" y2="-5.9944" width="0" layer="51"/>
<wire x1="2.6162" y1="-5.9944" x2="2.8956" y2="-5.9944" width="0" layer="51"/>
<wire x1="2.8956" y1="-5.9944" x2="2.8956" y2="-5.0038" width="0" layer="51"/>
<wire x1="3.3782" y1="-5.0038" x2="3.1242" y2="-5.0038" width="0" layer="51"/>
<wire x1="3.1242" y1="-5.0038" x2="3.1242" y2="-5.9944" width="0" layer="51"/>
<wire x1="3.1242" y1="-5.9944" x2="3.3782" y2="-5.9944" width="0" layer="51"/>
<wire x1="3.3782" y1="-5.9944" x2="3.3782" y2="-5.0038" width="0" layer="51"/>
<wire x1="3.8862" y1="-5.0038" x2="3.6068" y2="-5.0038" width="0" layer="51"/>
<wire x1="3.6068" y1="-5.0038" x2="3.6068" y2="-5.9944" width="0" layer="51"/>
<wire x1="3.6068" y1="-5.9944" x2="3.8862" y2="-5.9944" width="0" layer="51"/>
<wire x1="3.8862" y1="-5.9944" x2="3.8862" y2="-5.0038" width="0" layer="51"/>
<wire x1="5.0038" y1="-3.6068" x2="5.0038" y2="-3.8862" width="0" layer="51"/>
<wire x1="5.0038" y1="-3.8862" x2="5.9944" y2="-3.8862" width="0" layer="51"/>
<wire x1="5.9944" y1="-3.8862" x2="5.9944" y2="-3.6068" width="0" layer="51"/>
<wire x1="5.9944" y1="-3.6068" x2="5.0038" y2="-3.6068" width="0" layer="51"/>
<wire x1="5.0038" y1="-3.1242" x2="5.0038" y2="-3.3782" width="0" layer="51"/>
<wire x1="5.0038" y1="-3.3782" x2="5.9944" y2="-3.3782" width="0" layer="51"/>
<wire x1="5.9944" y1="-3.3782" x2="5.9944" y2="-3.1242" width="0" layer="51"/>
<wire x1="5.9944" y1="-3.1242" x2="5.0038" y2="-3.1242" width="0" layer="51"/>
<wire x1="5.0038" y1="-2.6162" x2="5.0038" y2="-2.8956" width="0" layer="51"/>
<wire x1="5.0038" y1="-2.8956" x2="5.9944" y2="-2.8956" width="0" layer="51"/>
<wire x1="5.9944" y1="-2.8956" x2="5.9944" y2="-2.6162" width="0" layer="51"/>
<wire x1="5.9944" y1="-2.6162" x2="5.0038" y2="-2.6162" width="0" layer="51"/>
<wire x1="5.0038" y1="-2.1082" x2="5.0038" y2="-2.3876" width="0" layer="51"/>
<wire x1="5.0038" y1="-2.3876" x2="5.9944" y2="-2.3876" width="0" layer="51"/>
<wire x1="5.9944" y1="-2.3876" x2="5.9944" y2="-2.1082" width="0" layer="51"/>
<wire x1="5.9944" y1="-2.1082" x2="5.0038" y2="-2.1082" width="0" layer="51"/>
<wire x1="5.0038" y1="-1.6256" x2="5.0038" y2="-1.8796" width="0" layer="51"/>
<wire x1="5.0038" y1="-1.8796" x2="5.9944" y2="-1.8796" width="0" layer="51"/>
<wire x1="5.9944" y1="-1.8796" x2="5.9944" y2="-1.6256" width="0" layer="51"/>
<wire x1="5.9944" y1="-1.6256" x2="5.0038" y2="-1.6256" width="0" layer="51"/>
<wire x1="5.0038" y1="-1.1176" x2="5.0038" y2="-1.397" width="0" layer="51"/>
<wire x1="5.0038" y1="-1.397" x2="5.9944" y2="-1.397" width="0" layer="51"/>
<wire x1="5.9944" y1="-1.397" x2="5.9944" y2="-1.1176" width="0" layer="51"/>
<wire x1="5.9944" y1="-1.1176" x2="5.0038" y2="-1.1176" width="0" layer="51"/>
<wire x1="5.0038" y1="-0.6096" x2="5.0038" y2="-0.889" width="0" layer="51"/>
<wire x1="5.0038" y1="-0.889" x2="5.9944" y2="-0.889" width="0" layer="51"/>
<wire x1="5.9944" y1="-0.889" x2="5.9944" y2="-0.6096" width="0" layer="51"/>
<wire x1="5.9944" y1="-0.6096" x2="5.0038" y2="-0.6096" width="0" layer="51"/>
<wire x1="5.0038" y1="-0.127" x2="5.0038" y2="-0.381" width="0" layer="51"/>
<wire x1="5.0038" y1="-0.381" x2="5.9944" y2="-0.381" width="0" layer="51"/>
<wire x1="5.9944" y1="-0.381" x2="5.9944" y2="-0.127" width="0" layer="51"/>
<wire x1="5.9944" y1="-0.127" x2="5.0038" y2="-0.127" width="0" layer="51"/>
<wire x1="5.0038" y1="0.381" x2="5.0038" y2="0.127" width="0" layer="51"/>
<wire x1="5.0038" y1="0.127" x2="5.9944" y2="0.127" width="0" layer="51"/>
<wire x1="5.9944" y1="0.127" x2="5.9944" y2="0.381" width="0" layer="51"/>
<wire x1="5.9944" y1="0.381" x2="5.0038" y2="0.381" width="0" layer="51"/>
<wire x1="5.0038" y1="0.889" x2="5.0038" y2="0.6096" width="0" layer="51"/>
<wire x1="5.0038" y1="0.6096" x2="5.9944" y2="0.6096" width="0" layer="51"/>
<wire x1="5.9944" y1="0.6096" x2="5.9944" y2="0.889" width="0" layer="51"/>
<wire x1="5.9944" y1="0.889" x2="5.0038" y2="0.889" width="0" layer="51"/>
<wire x1="5.0038" y1="1.397" x2="5.0038" y2="1.1176" width="0" layer="51"/>
<wire x1="5.0038" y1="1.1176" x2="5.9944" y2="1.1176" width="0" layer="51"/>
<wire x1="5.9944" y1="1.1176" x2="5.9944" y2="1.397" width="0" layer="51"/>
<wire x1="5.9944" y1="1.397" x2="5.0038" y2="1.397" width="0" layer="51"/>
<wire x1="5.0038" y1="1.8796" x2="5.0038" y2="1.6256" width="0" layer="51"/>
<wire x1="5.0038" y1="1.6256" x2="5.9944" y2="1.6256" width="0" layer="51"/>
<wire x1="5.9944" y1="1.6256" x2="5.9944" y2="1.8796" width="0" layer="51"/>
<wire x1="5.9944" y1="1.8796" x2="5.0038" y2="1.8796" width="0" layer="51"/>
<wire x1="5.0038" y1="2.3876" x2="5.0038" y2="2.1082" width="0" layer="51"/>
<wire x1="5.0038" y1="2.1082" x2="5.9944" y2="2.1082" width="0" layer="51"/>
<wire x1="5.9944" y1="2.1082" x2="5.9944" y2="2.3876" width="0" layer="51"/>
<wire x1="5.9944" y1="2.3876" x2="5.0038" y2="2.3876" width="0" layer="51"/>
<wire x1="5.0038" y1="2.8956" x2="5.0038" y2="2.6162" width="0" layer="51"/>
<wire x1="5.0038" y1="2.6162" x2="5.9944" y2="2.6162" width="0" layer="51"/>
<wire x1="5.9944" y1="2.6162" x2="5.9944" y2="2.8956" width="0" layer="51"/>
<wire x1="5.9944" y1="2.8956" x2="5.0038" y2="2.8956" width="0" layer="51"/>
<wire x1="5.0038" y1="3.3782" x2="5.0038" y2="3.1242" width="0" layer="51"/>
<wire x1="5.0038" y1="3.1242" x2="5.9944" y2="3.1242" width="0" layer="51"/>
<wire x1="5.9944" y1="3.1242" x2="5.9944" y2="3.3782" width="0" layer="51"/>
<wire x1="5.9944" y1="3.3782" x2="5.0038" y2="3.3782" width="0" layer="51"/>
<wire x1="5.0038" y1="3.8862" x2="5.0038" y2="3.6068" width="0" layer="51"/>
<wire x1="5.0038" y1="3.6068" x2="5.9944" y2="3.6068" width="0" layer="51"/>
<wire x1="5.9944" y1="3.6068" x2="5.9944" y2="3.8862" width="0" layer="51"/>
<wire x1="5.9944" y1="3.8862" x2="5.0038" y2="3.8862" width="0" layer="51"/>
<wire x1="-5.0038" y1="3.7338" x2="-3.7338" y2="5.0038" width="0" layer="51"/>
<wire x1="-3.1242" y1="-5.0038" x2="3.1242" y2="-5.0038" width="0" layer="51"/>
<wire x1="5.0038" y1="-3.3782" x2="5.0038" y2="3.3782" width="0" layer="51"/>
<wire x1="3.6068" y1="5.0038" x2="-3.7338" y2="5.0038" width="0" layer="51"/>
<wire x1="-5.0038" y1="3.8862" x2="-5.0038" y2="-3.8862" width="0" layer="51"/>
<text x="-3.4583" y="-11.4429" size="2.08515" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
<text x="-3.454990625" y="8.89151875" size="2.083159375" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<smd name="1" x="-5.6134" y="3.7592" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="2" x="-5.6134" y="3.2512" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="3" x="-5.6134" y="2.7432" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="4" x="-5.6134" y="2.2606" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="5" x="-5.6134" y="1.7526" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="6" x="-5.6134" y="1.2446" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="7" x="-5.6134" y="0.762" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="8" x="-5.6134" y="0.254" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="9" x="-5.6134" y="-0.254" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="10" x="-5.6134" y="-0.762" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="11" x="-5.6134" y="-1.2446" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="12" x="-5.6134" y="-1.7526" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="13" x="-5.6134" y="-2.2606" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="14" x="-5.6134" y="-2.7432" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="15" x="-5.6134" y="-3.2512" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="16" x="-5.6134" y="-3.7592" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="17" x="-3.7592" y="-5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="18" x="-3.2512" y="-5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="19" x="-2.7432" y="-5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="20" x="-2.2606" y="-5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="21" x="-1.7526" y="-5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="22" x="-1.2446" y="-5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="23" x="-0.762" y="-5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="24" x="-0.254" y="-5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="25" x="0.254" y="-5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="26" x="0.762" y="-5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="27" x="1.2446" y="-5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="28" x="1.7526" y="-5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="29" x="2.2606" y="-5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="30" x="2.7432" y="-5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="31" x="3.2512" y="-5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="32" x="3.7592" y="-5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="33" x="5.6134" y="-3.7592" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="34" x="5.6134" y="-3.2512" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="35" x="5.6134" y="-2.7432" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="36" x="5.6134" y="-2.2606" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="37" x="5.6134" y="-1.7526" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="38" x="5.6134" y="-1.2446" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="39" x="5.6134" y="-0.762" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="40" x="5.6134" y="-0.254" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="41" x="5.6134" y="0.254" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="42" x="5.6134" y="0.762" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="43" x="5.6134" y="1.2446" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="44" x="5.6134" y="1.7526" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="45" x="5.6134" y="2.2606" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="46" x="5.6134" y="2.7432" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="47" x="5.6134" y="3.2512" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="48" x="5.6134" y="3.7592" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="49" x="3.7592" y="5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="50" x="3.2512" y="5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="51" x="2.7432" y="5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="52" x="2.2606" y="5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="53" x="1.7526" y="5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="54" x="1.2446" y="5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="55" x="0.762" y="5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="56" x="0.254" y="5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="57" x="-0.254" y="5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="58" x="-0.762" y="5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="59" x="-1.2446" y="5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="60" x="-1.7526" y="5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="61" x="-2.2606" y="5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="62" x="-2.7432" y="5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="63" x="-3.2512" y="5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="64" x="-3.7592" y="5.6134" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
</package>
<package name="2X05" urn="urn:adsk.eagle:footprint:22358/1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-6.35" y1="-1.905" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-1.905" x2="-6.35" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="1.905" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.35" y2="1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.905" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-5.08" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="5.08" y="1.27" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-1.524" x2="-4.826" y2="-1.016" layer="51"/>
<rectangle x1="-5.334" y1="1.016" x2="-4.826" y2="1.524" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<rectangle x1="4.826" y1="1.016" x2="5.334" y2="1.524" layer="51"/>
<rectangle x1="4.826" y1="-1.524" x2="5.334" y2="-1.016" layer="51"/>
</package>
<package name="SD_0472192001">
<description>&lt;b&gt;0472192001-2&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="3.205" y="-2.1" dx="1.5" dy="0.8" layer="1" rot="R90"/>
<smd name="2" x="2.105" y="-2.1" dx="1.5" dy="0.8" layer="1" rot="R90"/>
<smd name="3" x="1.005" y="-2.1" dx="1.5" dy="0.8" layer="1" rot="R90"/>
<smd name="4" x="-0.095" y="-2.1" dx="1.5" dy="0.8" layer="1" rot="R90"/>
<smd name="5" x="-1.195" y="-2.1" dx="1.5" dy="0.8" layer="1" rot="R90"/>
<smd name="6" x="-2.295" y="-2.1" dx="1.5" dy="0.8" layer="1" rot="R90"/>
<smd name="7" x="-3.395" y="-2.1" dx="1.5" dy="0.8" layer="1" rot="R90"/>
<smd name="8" x="-4.495" y="-2.1" dx="1.5" dy="0.8" layer="1" rot="R90"/>
<smd name="G1" x="6.875" y="-4.7" dx="2" dy="1.45" layer="1" rot="R90"/>
<smd name="G2" x="6.875" y="3.6" dx="2" dy="1.45" layer="1" rot="R90"/>
<smd name="G3" x="-6.875" y="3.6" dx="2" dy="1.45" layer="1" rot="R90"/>
<smd name="G4" x="-6.875" y="-4.7" dx="2" dy="1.45" layer="1" rot="R90"/>
<text x="0" y="-0.275" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="-0.275" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-6.8" y1="7.25" x2="6.8" y2="7.25" width="0.2" layer="51"/>
<wire x1="6.8" y1="7.25" x2="6.8" y2="-7.25" width="0.2" layer="51"/>
<wire x1="6.8" y1="-7.25" x2="-6.8" y2="-7.25" width="0.2" layer="51"/>
<wire x1="-6.8" y1="-7.25" x2="-6.8" y2="7.25" width="0.2" layer="51"/>
<wire x1="-8.6" y1="8.25" x2="8.6" y2="8.25" width="0.1" layer="51"/>
<wire x1="8.6" y1="8.25" x2="8.6" y2="-8.8" width="0.1" layer="51"/>
<wire x1="8.6" y1="-8.8" x2="-8.6" y2="-8.8" width="0.1" layer="51"/>
<wire x1="-8.6" y1="-8.8" x2="-8.6" y2="8.25" width="0.1" layer="51"/>
<wire x1="-6.8" y1="-6.25" x2="-6.8" y2="-7.25" width="0.1" layer="21"/>
<wire x1="-6.8" y1="-7.25" x2="6.8" y2="-7.25" width="0.1" layer="21"/>
<wire x1="6.8" y1="-7.25" x2="6.8" y2="-6.25" width="0.1" layer="21"/>
<wire x1="-6.8" y1="1.45" x2="-6.8" y2="-2.55" width="0.1" layer="21"/>
<wire x1="6.8" y1="1.45" x2="6.8" y2="-2.55" width="0.1" layer="21"/>
<wire x1="-6.8" y1="5.45" x2="-6.8" y2="7.25" width="0.1" layer="21"/>
<wire x1="-6.8" y1="7.25" x2="6.8" y2="7.25" width="0.1" layer="21"/>
<wire x1="6.8" y1="7.25" x2="6.8" y2="5.45" width="0.1" layer="21"/>
<wire x1="3.2" y1="-7.7" x2="3.2" y2="-7.7" width="0.2" layer="21"/>
<wire x1="3.2" y1="-7.7" x2="3.4" y2="-7.7" width="0.2" layer="21" curve="-180"/>
<wire x1="3.4" y1="-7.7" x2="3.4" y2="-7.7" width="0.2" layer="21"/>
<wire x1="3.4" y1="-7.7" x2="3.2" y2="-7.7" width="0.2" layer="21" curve="-180"/>
</package>
<package name="XT60">
<pad name="+" x="4.5" y="0" drill="5" shape="octagon"/>
<pad name="-" x="11.5" y="0" drill="5" shape="octagon"/>
<wire x1="0" y1="4" x2="12" y2="4" width="0.127" layer="21"/>
<wire x1="15.5" y1="1.5" x2="15.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="12" y1="4" x2="15.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="15.5" y1="-1.5" x2="12" y2="-4" width="0.127" layer="21"/>
<wire x1="12" y1="-4" x2="0" y2="-4" width="0.127" layer="21"/>
<wire x1="0" y1="-4" x2="0" y2="4" width="0.127" layer="21"/>
</package>
<package name="B5B-PH-SM4-TB">
<description>&lt;b&gt;B5B-PH-SM4-TB&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="4" y="0" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="2" x="2" y="0" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="3" x="0" y="0" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="4" x="-2" y="0" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="5" x="-4" y="0" dx="5.5" dy="1" layer="1" rot="R90"/>
<smd name="6" x="-6.45" y="2.2" dx="3.1" dy="1.7" layer="1" rot="R90"/>
<smd name="7" x="6.45" y="2.2" dx="3.1" dy="1.7" layer="1" rot="R90"/>
<text x="-1.432" y="6.878" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-0.162" y="-4.552" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-7" y1="4.725" x2="7" y2="4.725" width="0.2" layer="51"/>
<wire x1="7" y1="4.725" x2="7" y2="-0.275" width="0.2" layer="51"/>
<wire x1="7" y1="-0.275" x2="-7" y2="-0.275" width="0.2" layer="51"/>
<wire x1="-7" y1="-0.275" x2="-7" y2="4.725" width="0.2" layer="51"/>
<circle x="5.195" y="-3.067" radius="0.205640625" width="0.4" layer="25"/>
<wire x1="-7" y1="4.725" x2="7" y2="4.725" width="0.2" layer="21"/>
<wire x1="-7" y1="-0.275" x2="-4.815" y2="-0.275" width="0.2" layer="21"/>
<wire x1="7" y1="-0.275" x2="4.782" y2="-0.275" width="0.2" layer="21"/>
</package>
<package name="PHX-12X2">
<pad name="P$1" x="-19.49" y="-4.53" drill="1.3"/>
<pad name="P$2" x="-15.99" y="-4.53" drill="1.3"/>
<pad name="P$3" x="-12.49" y="-4.53" drill="1.3"/>
<pad name="P$4" x="-8.99" y="-4.53" drill="1.3"/>
<pad name="P$5" x="-5.49" y="-4.53" drill="1.3"/>
<pad name="P$6" x="-1.99" y="-4.53" drill="1.3"/>
<pad name="P$7" x="1.51" y="-4.53" drill="1.3"/>
<pad name="P$8" x="5.01" y="-4.53" drill="1.3"/>
<pad name="P$9" x="8.51" y="-4.53" drill="1.3"/>
<pad name="P$10" x="12.01" y="-4.53" drill="1.3"/>
<pad name="P$11" x="15.51" y="-4.53" drill="1.3"/>
<pad name="P$12" x="19.01" y="-4.53" drill="1.3"/>
<pad name="P$13" x="19.01" y="4.82" drill="1.3" rot="R180"/>
<pad name="P$14" x="15.51" y="4.82" drill="1.3" rot="R180"/>
<pad name="P$15" x="12.01" y="4.82" drill="1.3" rot="R180"/>
<pad name="P$16" x="8.51" y="4.82" drill="1.3" rot="R180"/>
<pad name="P$17" x="5.01" y="4.82" drill="1.3" rot="R180"/>
<pad name="P$18" x="1.51" y="4.82" drill="1.3" rot="R180"/>
<pad name="P$19" x="-1.99" y="4.82" drill="1.3" rot="R180"/>
<pad name="P$20" x="-5.49" y="4.82" drill="1.3" rot="R180"/>
<pad name="P$21" x="-8.99" y="4.82" drill="1.3" rot="R180"/>
<pad name="P$22" x="-12.49" y="4.82" drill="1.3" rot="R180"/>
<pad name="P$23" x="-15.99" y="4.82" drill="1.3" rot="R180"/>
<pad name="P$24" x="-19.49" y="4.82" drill="1.3" rot="R180"/>
<wire x1="-21.59" y1="-11.43" x2="21.11" y2="-11.43" width="0.127" layer="21"/>
<wire x1="21.11" y1="-11.43" x2="21.11" y2="6.57" width="0.127" layer="21"/>
<wire x1="21.11" y1="6.57" x2="-21.59" y2="6.57" width="0.127" layer="21"/>
<wire x1="-21.59" y1="6.57" x2="-21.59" y2="-11.43" width="0.127" layer="21"/>
<text x="-2.54" y="7.62" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-13.97" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="PHX-10X2">
<pad name="P$1" x="-16.95" y="-4.53" drill="1.3"/>
<pad name="P$2" x="-13.45" y="-4.53" drill="1.3"/>
<pad name="P$3" x="-9.95" y="-4.53" drill="1.3"/>
<pad name="P$4" x="-6.45" y="-4.53" drill="1.3"/>
<pad name="P$5" x="-2.95" y="-4.53" drill="1.3"/>
<pad name="P$6" x="0.55" y="-4.53" drill="1.3"/>
<pad name="P$7" x="4.05" y="-4.53" drill="1.3"/>
<pad name="P$8" x="7.55" y="-4.53" drill="1.3"/>
<pad name="P$9" x="11.05" y="-4.53" drill="1.3"/>
<pad name="P$10" x="14.55" y="-4.53" drill="1.3"/>
<wire x1="-19.05" y1="-11.43" x2="16.65" y2="-11.43" width="0.127" layer="21"/>
<wire x1="16.65" y1="-11.43" x2="16.65" y2="6.57" width="0.127" layer="21"/>
<wire x1="16.65" y1="6.57" x2="-19.05" y2="6.57" width="0.127" layer="21"/>
<wire x1="-19.05" y1="6.57" x2="-19.05" y2="-11.43" width="0.127" layer="21"/>
<text x="-2.54" y="7.62" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-13.97" size="1.27" layer="27">&gt;VALUE</text>
<pad name="P$11" x="14.55" y="4.82" drill="1.3"/>
<pad name="P$12" x="11.05" y="4.82" drill="1.3"/>
<pad name="P$13" x="7.55" y="4.82" drill="1.3"/>
<pad name="P$14" x="4.05" y="4.82" drill="1.3"/>
<pad name="P$15" x="0.55" y="4.82" drill="1.3"/>
<pad name="P$16" x="-2.95" y="4.82" drill="1.3"/>
<pad name="P$17" x="-6.45" y="4.82" drill="1.3"/>
<pad name="P$18" x="-9.95" y="4.82" drill="1.3"/>
<pad name="P$19" x="-13.45" y="4.82" drill="1.3"/>
<pad name="P$20" x="-16.95" y="4.82" drill="1.3"/>
</package>
<package name="PHX-6X2">
<pad name="P$1" x="-9.33" y="-4.53" drill="1.3"/>
<pad name="P$2" x="-5.83" y="-4.53" drill="1.3"/>
<pad name="P$3" x="-2.33" y="-4.53" drill="1.3"/>
<pad name="P$4" x="1.17" y="-4.53" drill="1.3"/>
<pad name="P$5" x="4.67" y="-4.53" drill="1.3"/>
<pad name="P$6" x="8.17" y="-4.53" drill="1.3"/>
<wire x1="-11.43" y1="-11.43" x2="10.27" y2="-11.43" width="0.127" layer="21"/>
<wire x1="10.27" y1="-11.43" x2="10.27" y2="6.57" width="0.127" layer="21"/>
<wire x1="10.27" y1="6.57" x2="-11.43" y2="6.57" width="0.127" layer="21"/>
<wire x1="-11.43" y1="6.57" x2="-11.43" y2="-11.43" width="0.127" layer="21"/>
<text x="-3" y="7.62" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.27" y="-13.97" size="1.27" layer="27">&gt;VALUE</text>
<pad name="P$7" x="8.17" y="4.82" drill="1.3"/>
<pad name="P$8" x="4.67" y="4.82" drill="1.3"/>
<pad name="P$9" x="1.17" y="4.82" drill="1.3"/>
<pad name="P$10" x="-2.33" y="4.82" drill="1.3"/>
<pad name="P$11" x="-5.83" y="4.82" drill="1.3"/>
<pad name="P$12" x="-9.33" y="4.82" drill="1.3"/>
</package>
<package name="SOIC127P600X144-8" urn="urn:adsk.eagle:footprint:9256955/1">
<description>8-SOIC, 1.27 mm pitch, 6.00 mm span, 4.90 X 3.90 X 1.44 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 6.00 mm span with body size 4.90 X 3.90 X 1.44 mm&lt;/p&gt;</description>
<circle x="-2.678" y="2.7086" radius="0.25" width="0" layer="21"/>
<wire x1="-1.95" y1="2.5186" x2="1.95" y2="2.5186" width="0.12" layer="21"/>
<wire x1="-1.95" y1="-2.5186" x2="1.95" y2="-2.5186" width="0.12" layer="21"/>
<wire x1="1.95" y1="-2.45" x2="-1.95" y2="-2.45" width="0.12" layer="51"/>
<wire x1="-1.95" y1="-2.45" x2="-1.95" y2="2.45" width="0.12" layer="51"/>
<wire x1="-1.95" y1="2.45" x2="1.95" y2="2.45" width="0.12" layer="51"/>
<wire x1="1.95" y1="2.45" x2="1.95" y2="-2.45" width="0.12" layer="51"/>
<smd name="1" x="-2.4554" y="1.905" dx="1.901" dy="0.5991" layer="1"/>
<smd name="2" x="-2.4554" y="0.635" dx="1.901" dy="0.5991" layer="1"/>
<smd name="3" x="-2.4554" y="-0.635" dx="1.901" dy="0.5991" layer="1"/>
<smd name="4" x="-2.4554" y="-1.905" dx="1.901" dy="0.5991" layer="1"/>
<smd name="5" x="2.4554" y="-1.905" dx="1.901" dy="0.5991" layer="1"/>
<smd name="6" x="2.4554" y="-0.635" dx="1.901" dy="0.5991" layer="1"/>
<smd name="7" x="2.4554" y="0.635" dx="1.901" dy="0.5991" layer="1"/>
<smd name="8" x="2.4554" y="1.905" dx="1.901" dy="0.5991" layer="1"/>
<text x="0" y="3.5936" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.1536" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="LED-1206" urn="urn:adsk.eagle:footprint:39304/1">
<description>&lt;h3&gt;LED 1206 SMT&lt;/h3&gt;

1206, surface mount. 

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: &lt;/li&gt;
&lt;li&gt;Area: 0.125" x 0.06"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="2.4" y1="0.6825" x2="2.4" y2="-0.6825" width="0.2032" layer="21"/>
<wire x1="0.65375" y1="0.6825" x2="0.65375" y2="-0.6825" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="0.15875" y2="0.47625" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="0.15875" y2="-0.47625" width="0.2032" layer="51"/>
<smd name="A" x="-1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<smd name="C" x="1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<text x="0" y="0.9525" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.9525" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="SOT-23-6" urn="urn:adsk.eagle:footprint:32558/1">
<wire x1="1.49" y1="0.83" x2="1.49" y2="-0.83" width="0.127" layer="21"/>
<wire x1="-1.2598" y1="-0.83" x2="-1.49" y2="-0.5998" width="0.127" layer="21"/>
<wire x1="-1.49" y1="-0.5998" x2="-1.49" y2="0.83" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0.83" x2="-1.4" y2="0.83" width="0.127" layer="51"/>
<wire x1="1.4" y1="0.83" x2="1.5" y2="0.83" width="0.127" layer="51"/>
<circle x="-1.905" y="-0.635" radius="0.254" width="0" layer="21"/>
<smd name="1" x="-0.95" y="-1.3" dx="1" dy="0.69" layer="1" rot="R90"/>
<smd name="2" x="0" y="-1.3" dx="1" dy="0.69" layer="1" rot="R90"/>
<smd name="3" x="0.95" y="-1.3" dx="1" dy="0.69" layer="1" rot="R90"/>
<smd name="4" x="0.95" y="1.3" dx="1" dy="0.69" layer="1" rot="R90"/>
<smd name="5" x="0" y="1.3" dx="1" dy="0.69" layer="1" rot="R90"/>
<smd name="6" x="-0.95" y="1.3" dx="1" dy="0.69" layer="1" rot="R90"/>
<text x="-1.905" y="2.54" size="0.889" layer="25" ratio="11">&gt;name</text>
<text x="-1.905" y="-3.175" size="0.889" layer="27" ratio="11">&gt;value</text>
<rectangle x1="-1.524" y1="-0.83" x2="1.524" y2="0.83" layer="39"/>
</package>
<package name="INDUCTOR_3.9UH_1255AY-3R9N=P3">
<smd name="P$1" x="2.35" y="0" dx="1.6" dy="5.7" layer="1"/>
<smd name="P$2" x="-2.35" y="0" dx="1.6" dy="5.7" layer="1"/>
<wire x1="-3" y1="3" x2="-3" y2="-3" width="0.127" layer="51"/>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.127" layer="51"/>
<wire x1="3" y1="-3" x2="3" y2="3" width="0.127" layer="51"/>
<wire x1="3" y1="3" x2="-3" y2="3" width="0.127" layer="51"/>
<text x="0" y="4" size="1" layer="25" align="center">&gt;NAME</text>
<text x="0" y="-4" size="1" layer="27" align="center">&gt;VALUE</text>
</package>
<package name="CAPACITOR-φ6.3">
<smd name="P$1" x="2.8" y="0" dx="3.5" dy="1.6" layer="1"/>
<smd name="P$2" x="-2.8" y="0" dx="3.5" dy="1.6" layer="1"/>
<circle x="0" y="0" radius="3.15" width="0.127" layer="51"/>
<wire x1="-3.81" y1="3.81" x2="-3.81" y2="-3.81" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-3.81" x2="2.54" y2="-3.81" width="0.127" layer="51"/>
<wire x1="2.54" y1="-3.81" x2="3.81" y2="-2.54" width="0.127" layer="51"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="2.54" width="0.127" layer="51"/>
<wire x1="3.81" y1="2.54" x2="2.54" y2="3.81" width="0.127" layer="51"/>
<wire x1="2.54" y1="3.81" x2="-3.81" y2="3.81" width="0.127" layer="51"/>
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="3.81" width="0.127" layer="21"/>
<wire x1="-3.81" y1="3.81" x2="2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="3.81" x2="3.81" y2="2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="-3.81" x2="-3.81" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="-1.27" width="0.127" layer="21"/>
<polygon width="0.1016" layer="21">
<vertex x="-3.1" y="0"/>
<vertex x="-2.9" y="1"/>
<vertex x="-2.5" y="1.8"/>
<vertex x="-1.8" y="2.5"/>
<vertex x="-1.8" y="-2.5"/>
<vertex x="-2.6" y="-1.7"/>
<vertex x="-3" y="-0.7"/>
</polygon>
<text x="0" y="2" size="0.7" layer="25" align="center">&gt;NAME</text>
<text x="0" y="-2" size="0.7" layer="27" align="center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="DO-214AA" urn="urn:adsk.eagle:package:32511/1" type="box">
<packageinstances>
<packageinstance name="DO-214AA"/>
</packageinstances>
</package3d>
<package3d name="R0805" urn="urn:adsk.eagle:package:5829815/2" type="model">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<packageinstances>
<packageinstance name="R0805"/>
</packageinstances>
</package3d>
<package3d name="2X05" urn="urn:adsk.eagle:package:22470/2" type="model">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="2X05"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="DIODE-ZENER">
<description>&lt;h3&gt;Zener Diode&lt;/h3&gt;
Allows current flow in one direction, but allows reverse flow when above breakdown voltage.</description>
<wire x1="1.27" y1="0.889" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-0.889" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.889" x2="1.778" y2="1.397" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-0.889" x2="0.762" y2="-1.397" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.032" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="point" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
<polygon width="0.254" layer="94">
<vertex x="-1.27" y="1.27"/>
<vertex x="1.27" y="0"/>
<vertex x="-1.27" y="-1.27"/>
</polygon>
</symbol>
<symbol name="DIODE">
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<text x="-3.81" y="2.54" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="+" x="-3.81" y="0" visible="off" length="short" direction="pas"/>
<pin name="-" x="3.81" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="FUSE">
<wire x1="-5.08" y1="0" x2="-3.556" y2="1.524" width="0.254" layer="94"/>
<wire x1="0" y1="-1.524" x2="-2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.889" y1="-1.4986" x2="2.4892" y2="0" width="0.254" layer="94"/>
<wire x1="-3.5992" y1="1.4912" x2="-3.048" y2="1.7272" width="0.254" layer="94" curve="-46.337037" cap="flat"/>
<wire x1="-3.048" y1="1.7272" x2="-2.496" y2="1.491" width="0.254" layer="94" curve="-46.403624" cap="flat"/>
<wire x1="0.4572" y1="-1.778" x2="0.8965" y2="-1.4765" width="0.254" layer="94" curve="63.169357" cap="flat"/>
<wire x1="-0.0178" y1="-1.508" x2="0.4572" y2="-1.7778" width="0.254" layer="94" curve="64.986119" cap="flat"/>
<text x="-5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-7.62" y="0" visible="off" length="short" direction="pas" function="dot" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" function="dot" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="FUSE-CARTRIDGE">
<wire x1="-5.08" y1="0" x2="-3.556" y2="1.524" width="0.254" layer="94"/>
<wire x1="0" y1="-1.524" x2="-2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.889" y1="-1.4986" x2="2.4892" y2="0" width="0.254" layer="94"/>
<wire x1="-3.5992" y1="1.4912" x2="-3.048" y2="1.7272" width="0.254" layer="94" curve="-46.337037" cap="flat"/>
<wire x1="-3.048" y1="1.7272" x2="-2.496" y2="1.491" width="0.254" layer="94" curve="-46.403624" cap="flat"/>
<wire x1="0.4572" y1="-1.778" x2="0.8965" y2="-1.4765" width="0.254" layer="94" curve="63.169357" cap="flat"/>
<wire x1="-0.0178" y1="-1.508" x2="0.4572" y2="-1.7778" width="0.254" layer="94" curve="64.986119" cap="flat"/>
<text x="-5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-5.08" y1="0" x2="-7.62" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
</symbol>
<symbol name="D24V150FX">
<wire x1="7.62" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<pin name="VIN1" x="-15.24" y="5.08" length="middle"/>
<pin name="VIN2" x="-15.24" y="2.54" length="middle"/>
<pin name="GIN1" x="-15.24" y="0" length="middle"/>
<pin name="GIN2" x="-15.24" y="-2.54" length="middle"/>
<pin name="EN" x="-15.24" y="-5.08" length="middle"/>
<pin name="VOUT1" x="12.7" y="5.08" length="middle" rot="R180"/>
<pin name="VOUT2" x="12.7" y="2.54" length="middle" rot="R180"/>
<pin name="GOUT1" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="GOUT2" x="12.7" y="-2.54" length="middle" rot="R180"/>
<text x="-5.08" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="LABELED-PMOS">
<description>&lt;h3&gt;P-channel MOSFET&lt;/h3&gt;
Switches electrical signals</description>
<text x="5.08" y="0" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short"/>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" rot="R90"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" rot="R270"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="0.6858" x2="-1.9812" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="0" x2="-1.9812" y2="-0.8382" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="-1.2954" x2="-1.9812" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="-1.905" x2="-1.9812" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="2.54" x2="-1.9812" y2="1.8034" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="1.8034" x2="-1.9812" y2="1.0922" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="-1.9812" y1="1.8034" x2="2.54" y2="1.8034" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-0.762" x2="3.302" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.8034" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-0.762" x2="1.6002" y2="-0.9398" width="0.1524" layer="94"/>
<wire x1="3.4798" y1="-0.5842" x2="3.302" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="-1.9812" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="-0.7874" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.8034" x2="2.54" y2="0.5842" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="3.302" y="0.508"/>
<vertex x="2.54" y="-0.762"/>
<vertex x="1.778" y="0.508"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-0.1778" y="0"/>
<vertex x="-0.9398" y="-0.254"/>
<vertex x="-0.9398" y="0.254"/>
</polygon>
</symbol>
<symbol name="NPN">
<description>&lt;h3&gt; NPN Transistor&lt;/h3&gt;
Allows current flow when high potential at base.</description>
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.54" y1="-2.04" x2="0.308" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-2.413" x2="2.286" y2="-2.413" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.413" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.778" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.286" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="2.54" y="-2.286" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="2" rot="R270"/>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="PHX-2X1">
<wire x1="-2.54" y1="7.62" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<pin name="P$1" x="10.16" y="5.08" length="middle" rot="R180"/>
<pin name="P$2" x="10.16" y="0" length="middle" rot="R180"/>
<pin name="P$3" x="10.16" y="-2.54" length="middle" rot="R180"/>
<pin name="P$4" x="10.16" y="2.54" length="middle" rot="R180"/>
<text x="-2.54" y="10.16" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-7.62" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="ACS711KEXLT-31AB-T">
<wire x1="-10.16" y1="5.08" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="5.08" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<text x="24.13" y="17.78" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="24.13" y="15.24" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="IP+" x="-12.7" y="2.54" length="short" direction="pwr"/>
<pin name="IP-" x="12.7" y="-2.54" length="short" direction="pwr" rot="R180"/>
<pin name="GND" x="-12.7" y="-2.54" length="short" direction="pwr"/>
<pin name="!FAULT" x="12.7" y="2.54" length="short" direction="out" rot="R180"/>
<pin name="VIOUT" x="12.7" y="0" length="short" direction="out" rot="R180"/>
<pin name="VCC" x="-12.7" y="0" length="short" direction="pwr"/>
</symbol>
<symbol name="LED_DISPLAY_MODULE_LDT-N2804RI">
<pin name="A" x="-10.16" y="7.62" length="short"/>
<pin name="B" x="-10.16" y="5.08" length="short"/>
<pin name="C" x="-10.16" y="2.54" length="short"/>
<pin name="D" x="-10.16" y="0" length="short"/>
<pin name="E" x="-10.16" y="-2.54" length="short"/>
<pin name="F" x="-10.16" y="-5.08" length="short"/>
<pin name="G" x="-10.16" y="-7.62" length="short"/>
<pin name="DP" x="10.16" y="-2.54" length="short" rot="R180"/>
<pin name="DIG1" x="10.16" y="0" length="short" rot="R180"/>
<pin name="DIG2" x="10.16" y="2.54" length="short" rot="R180"/>
<pin name="DIG3" x="10.16" y="5.08" length="short" rot="R180"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<text x="0" y="12.7" size="1.27" layer="95" align="center">&gt;NAME</text>
<text x="0" y="-12.7" size="1.27" layer="95" align="center">&gt;VALUE</text>
</symbol>
<symbol name="LED_DRIVER_AS1108WL-T">
<pin name="DOUT" x="-15.24" y="5.08" length="short" direction="out"/>
<pin name="DIN" x="-15.24" y="2.54" length="short" direction="in"/>
<pin name="DIG0" x="15.24" y="-7.62" length="short" direction="out" rot="R180"/>
<pin name="GND" x="-15.24" y="-15.24" length="short" direction="pwr"/>
<pin name="DIG2" x="15.24" y="-12.7" length="short" direction="out" rot="R180"/>
<pin name="DIG3" x="15.24" y="-15.24" length="short" direction="out" rot="R180"/>
<pin name="DIG1" x="15.24" y="-10.16" length="short" direction="out" rot="R180"/>
<pin name="LOAD/CSN" x="-15.24" y="-5.08" length="short" direction="in"/>
<pin name="CLK" x="-15.24" y="-7.62" length="short" direction="in"/>
<pin name="SEG_A" x="15.24" y="15.24" length="short" direction="out" rot="R180"/>
<pin name="SEG_F" x="15.24" y="2.54" length="short" direction="out" rot="R180"/>
<pin name="SEG_B" x="15.24" y="12.7" length="short" direction="out" rot="R180"/>
<pin name="SEG_G" x="15.24" y="0" length="short" direction="out" rot="R180"/>
<pin name="ISET" x="-15.24" y="12.7" length="short" direction="in"/>
<pin name="VDD" x="-15.24" y="15.24" length="short" direction="pwr"/>
<pin name="SEG_C" x="15.24" y="10.16" length="short" direction="out" rot="R180"/>
<pin name="SEG_E" x="15.24" y="5.08" length="short" direction="out" rot="R180"/>
<pin name="SEG_DP" x="15.24" y="-2.54" length="short" direction="out" rot="R180"/>
<pin name="SEG_D" x="15.24" y="7.62" length="short" direction="out" rot="R180"/>
<wire x1="-12.7" y1="17.78" x2="-12.7" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-17.78" x2="12.7" y2="-17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="-17.78" x2="12.7" y2="17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="17.78" x2="-12.7" y2="17.78" width="0.254" layer="94"/>
<text x="0" y="20.32" size="1.27" layer="95" align="center">&gt;NAME</text>
<text x="0" y="-20.32" size="1.27" layer="95" align="center">&gt;VALUE</text>
</symbol>
<symbol name="STM32F103RCT6">
<wire x1="-25.4" y1="60.96" x2="-25.4" y2="-68.58" width="0.4064" layer="94"/>
<wire x1="-25.4" y1="-68.58" x2="25.4" y2="-68.58" width="0.4064" layer="94"/>
<wire x1="25.4" y1="-68.58" x2="25.4" y2="60.96" width="0.4064" layer="94"/>
<wire x1="25.4" y1="60.96" x2="-25.4" y2="60.96" width="0.4064" layer="94"/>
<text x="-5.366859375" y="65.0891" size="2.0857" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-4.142140625" y="-73.2116" size="2.08376875" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
<pin name="VDDA" x="-30.48" y="55.88" length="middle" direction="pwr"/>
<pin name="VDD_2" x="-30.48" y="53.34" length="middle" direction="pwr"/>
<pin name="VDD_3" x="-30.48" y="50.8" length="middle" direction="pwr"/>
<pin name="VDD_4" x="-30.48" y="48.26" length="middle" direction="pwr"/>
<pin name="VDD_1" x="-30.48" y="45.72" length="middle" direction="pwr"/>
<pin name="VBAT" x="-30.48" y="40.64" length="middle" direction="in"/>
<pin name="BOOT0" x="-30.48" y="38.1" length="middle" direction="pas"/>
<pin name="PA0-WKUP" x="-30.48" y="33.02" length="middle"/>
<pin name="PA1" x="-30.48" y="30.48" length="middle"/>
<pin name="PA2" x="-30.48" y="27.94" length="middle"/>
<pin name="PA3" x="-30.48" y="25.4" length="middle"/>
<pin name="PA4" x="-30.48" y="22.86" length="middle"/>
<pin name="PA5" x="-30.48" y="20.32" length="middle"/>
<pin name="PA6" x="-30.48" y="17.78" length="middle"/>
<pin name="PA7" x="-30.48" y="15.24" length="middle" direction="pwr"/>
<pin name="PA8" x="-30.48" y="12.7" length="middle"/>
<pin name="PA9" x="-30.48" y="10.16" length="middle"/>
<pin name="PA10" x="-30.48" y="7.62" length="middle"/>
<pin name="PA11" x="-30.48" y="5.08" length="middle"/>
<pin name="PA12" x="-30.48" y="2.54" length="middle"/>
<pin name="PA13" x="-30.48" y="0" length="middle"/>
<pin name="PA14" x="-30.48" y="-2.54" length="middle"/>
<pin name="PA15" x="-30.48" y="-5.08" length="middle"/>
<pin name="PB0" x="-30.48" y="-10.16" length="middle"/>
<pin name="PB1_2" x="-30.48" y="-12.7" length="middle"/>
<pin name="PB1" x="-30.48" y="-15.24" length="middle"/>
<pin name="PB3" x="-30.48" y="-17.78" length="middle"/>
<pin name="PB4" x="-30.48" y="-20.32" length="middle"/>
<pin name="PB5" x="-30.48" y="-22.86" length="middle"/>
<pin name="PB6" x="-30.48" y="-25.4" length="middle"/>
<pin name="PB7" x="-30.48" y="-27.94" length="middle" direction="pwr"/>
<pin name="PB8" x="-30.48" y="-30.48" length="middle"/>
<pin name="PB9" x="-30.48" y="-33.02" length="middle"/>
<pin name="PB10" x="-30.48" y="-35.56" length="middle"/>
<pin name="PB11" x="-30.48" y="-38.1" length="middle"/>
<pin name="PB12" x="-30.48" y="-40.64" length="middle"/>
<pin name="PB13" x="-30.48" y="-43.18" length="middle"/>
<pin name="PB14" x="-30.48" y="-45.72" length="middle"/>
<pin name="PB15" x="-30.48" y="-48.26" length="middle"/>
<pin name="VSS_1" x="-30.48" y="-53.34" length="middle" direction="pas"/>
<pin name="VSS_4" x="-30.48" y="-55.88" length="middle" direction="pas"/>
<pin name="VSSA" x="-30.48" y="-58.42" length="middle" direction="pas"/>
<pin name="VSS_2" x="-30.48" y="-60.96" length="middle" direction="pas"/>
<pin name="VSS_3" x="-30.48" y="-63.5" length="middle" direction="pas"/>
<pin name="NRST" x="30.48" y="55.88" length="middle" direction="out" rot="R180"/>
<pin name="PD0_OSC_IN" x="30.48" y="50.8" length="middle" direction="in" rot="R180"/>
<pin name="PD1_OSC_OUT" x="30.48" y="48.26" length="middle" direction="out" rot="R180"/>
<pin name="PD2" x="30.48" y="45.72" length="middle" rot="R180"/>
<pin name="PC0" x="30.48" y="40.64" length="middle" rot="R180"/>
<pin name="PC1" x="30.48" y="38.1" length="middle" rot="R180"/>
<pin name="PC2" x="30.48" y="35.56" length="middle" rot="R180"/>
<pin name="PC3" x="30.48" y="33.02" length="middle" rot="R180"/>
<pin name="PC4" x="30.48" y="30.48" length="middle" direction="pas" rot="R180"/>
<pin name="PC5" x="30.48" y="27.94" length="middle" rot="R180"/>
<pin name="PC6" x="30.48" y="25.4" length="middle" rot="R180"/>
<pin name="PC7" x="30.48" y="22.86" length="middle" rot="R180"/>
<pin name="PC8" x="30.48" y="20.32" length="middle" rot="R180"/>
<pin name="PC9" x="30.48" y="17.78" length="middle" rot="R180"/>
<pin name="PC10" x="30.48" y="15.24" length="middle" rot="R180"/>
<pin name="PC11" x="30.48" y="12.7" length="middle" rot="R180"/>
<pin name="PC12" x="30.48" y="10.16" length="middle" rot="R180"/>
<pin name="PC13-TAMPER-RTC" x="30.48" y="5.08" length="middle" direction="in" rot="R180"/>
<pin name="PC14-OSC32_IN" x="30.48" y="2.54" length="middle" direction="in" rot="R180"/>
<pin name="PC15-OSC32_OUT" x="30.48" y="0" length="middle" direction="out" rot="R180"/>
</symbol>
<symbol name="JTAG">
<pin name="VCC" x="-17.78" y="5.08" length="short"/>
<pin name="GND1" x="-17.78" y="2.54" length="short"/>
<pin name="GND2" x="-17.78" y="0" length="short"/>
<pin name="KEY" x="-17.78" y="-2.54" length="short"/>
<pin name="GND_DETECT" x="-17.78" y="-5.08" length="short"/>
<pin name="SWDIO/TMS" x="17.78" y="5.08" length="short" rot="R180"/>
<pin name="SWCLK/TCK" x="17.78" y="2.54" length="short" rot="R180"/>
<pin name="SWO/TDO" x="17.78" y="0" length="short" rot="R180"/>
<pin name="NC/TDI" x="17.78" y="-2.54" length="short" rot="R180"/>
<pin name="NRESET" x="17.78" y="-5.08" length="short" rot="R180"/>
<wire x1="-15.24" y1="7.62" x2="-15.24" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-7.62" x2="15.24" y2="-7.62" width="0.254" layer="94"/>
<wire x1="15.24" y1="-7.62" x2="15.24" y2="7.62" width="0.254" layer="94"/>
<wire x1="15.24" y1="7.62" x2="-15.24" y2="7.62" width="0.254" layer="94"/>
<text x="0" y="10.16" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="0" y="-10.16" size="1.778" layer="96" align="center">&gt;VALUE</text>
</symbol>
<symbol name="SD_0472192001">
<wire x1="-10.16" y1="15.24" x2="5.08" y2="15.24" width="0.254" layer="94"/>
<wire x1="5.08" y1="-12.7" x2="5.08" y2="15.24" width="0.254" layer="94"/>
<wire x1="5.08" y1="-12.7" x2="-10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-10.16" y1="15.24" x2="-10.16" y2="-12.7" width="0.254" layer="94"/>
<text x="-2.54" y="17.78" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="-2.54" y="-15.24" size="1.778" layer="96" align="center">&gt;VALUE</text>
<pin name="DAT2" x="-12.7" y="12.7" length="short"/>
<pin name="CD/DAT3" x="-12.7" y="10.16" length="short"/>
<pin name="CMD" x="-12.7" y="7.62" length="short"/>
<pin name="VDD" x="-12.7" y="5.08" length="short"/>
<pin name="CLK" x="-12.7" y="2.54" length="short"/>
<pin name="VSS" x="-12.7" y="0" length="short"/>
<pin name="DAT0" x="-12.7" y="-2.54" length="short"/>
<pin name="DAT1" x="-12.7" y="-5.08" length="short"/>
<pin name="SHIELD" x="-12.7" y="-10.16" length="short"/>
</symbol>
<symbol name="XT60">
<pin name="VCC" x="-5.08" y="-2.54" length="middle"/>
<pin name="GND" x="-5.08" y="2.54" length="middle"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.254" layer="97"/>
<wire x1="-2.54" y1="-5.08" x2="10.16" y2="-5.08" width="0.254" layer="97"/>
<wire x1="10.16" y1="-5.08" x2="10.16" y2="5.08" width="0.254" layer="97"/>
<wire x1="10.16" y1="5.08" x2="-2.54" y2="5.08" width="0.254" layer="97"/>
<text x="-0.635" y="5.715" size="2.54" layer="97">XT60</text>
</symbol>
<symbol name="JST-5">
<pin name="1" x="-10.16" y="-2.54" length="short"/>
<pin name="2" x="-10.16" y="0" length="short"/>
<pin name="3" x="-10.16" y="2.54" length="short"/>
<pin name="4" x="-10.16" y="5.08" length="short"/>
<pin name="5" x="-10.16" y="7.62" length="short"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="5.08" y2="10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<text x="-2.54" y="12.7" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="-2.54" y="-12.7" size="1.778" layer="96" align="center">&gt;NAME</text>
<pin name="SHIELD" x="-10.16" y="-7.62" length="short"/>
</symbol>
<symbol name="PHX-12X2">
<wire x1="7.62" y1="15.24" x2="-7.62" y2="15.24" width="0.254" layer="94"/>
<wire x1="-7.62" y1="15.24" x2="-7.62" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-17.78" x2="7.62" y2="-17.78" width="0.254" layer="94"/>
<wire x1="7.62" y1="-17.78" x2="7.62" y2="15.24" width="0.254" layer="94"/>
<pin name="P$1" x="-12.7" y="12.7" length="middle"/>
<pin name="P$2" x="-12.7" y="10.16" length="middle"/>
<pin name="P$3" x="-12.7" y="7.62" length="middle"/>
<pin name="P$4" x="-12.7" y="5.08" length="middle"/>
<pin name="P$5" x="-12.7" y="2.54" length="middle"/>
<pin name="P$6" x="-12.7" y="0" length="middle"/>
<pin name="P$7" x="-12.7" y="-2.54" length="middle"/>
<pin name="P$8" x="-12.7" y="-5.08" length="middle"/>
<pin name="P$9" x="-12.7" y="-7.62" length="middle"/>
<pin name="P$10" x="-12.7" y="-10.16" length="middle"/>
<pin name="P$11" x="-12.7" y="-12.7" length="middle"/>
<pin name="P$12" x="-12.7" y="-15.24" length="middle"/>
<pin name="P$13" x="12.7" y="-15.24" length="middle" rot="R180"/>
<pin name="P$14" x="12.7" y="-12.7" length="middle" rot="R180"/>
<pin name="P$15" x="12.7" y="-10.16" length="middle" rot="R180"/>
<pin name="P$16" x="12.7" y="-7.62" length="middle" rot="R180"/>
<pin name="P$17" x="12.7" y="-5.08" length="middle" rot="R180"/>
<pin name="P$18" x="12.7" y="-2.54" length="middle" rot="R180"/>
<pin name="P$19" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="P$20" x="12.7" y="2.54" length="middle" rot="R180"/>
<pin name="P$21" x="12.7" y="5.08" length="middle" rot="R180"/>
<pin name="P$22" x="12.7" y="7.62" length="middle" rot="R180"/>
<pin name="P$23" x="12.7" y="10.16" length="middle" rot="R180"/>
<pin name="P$24" x="12.7" y="12.7" length="middle" rot="R180"/>
<text x="-2.54" y="17.78" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-20.32" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="PHX-10X2">
<wire x1="7.62" y1="12.7" x2="-7.62" y2="12.7" width="0.254" layer="94"/>
<wire x1="-7.62" y1="12.7" x2="-7.62" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-15.24" x2="7.62" y2="-15.24" width="0.254" layer="94"/>
<wire x1="7.62" y1="-15.24" x2="7.62" y2="12.7" width="0.254" layer="94"/>
<pin name="P$1" x="-12.7" y="10.16" length="middle"/>
<pin name="P$2" x="-12.7" y="7.62" length="middle"/>
<pin name="P$3" x="-12.7" y="5.08" length="middle"/>
<pin name="P$4" x="-12.7" y="2.54" length="middle"/>
<pin name="P$5" x="-12.7" y="0" length="middle"/>
<pin name="P$6" x="-12.7" y="-2.54" length="middle"/>
<pin name="P$7" x="-12.7" y="-5.08" length="middle"/>
<pin name="P$8" x="-12.7" y="-7.62" length="middle"/>
<pin name="P$9" x="-12.7" y="-10.16" length="middle"/>
<pin name="P$10" x="-12.7" y="-12.7" length="middle"/>
<pin name="P$11" x="12.7" y="-12.7" length="middle" rot="R180"/>
<pin name="P$12" x="12.7" y="-10.16" length="middle" rot="R180"/>
<pin name="P$13" x="12.7" y="-7.62" length="middle" rot="R180"/>
<pin name="P$14" x="12.7" y="-5.08" length="middle" rot="R180"/>
<pin name="P$15" x="12.7" y="-2.54" length="middle" rot="R180"/>
<pin name="P$16" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="P$17" x="12.7" y="2.54" length="middle" rot="R180"/>
<pin name="P$18" x="12.7" y="5.08" length="middle" rot="R180"/>
<pin name="P$19" x="12.7" y="7.62" length="middle" rot="R180"/>
<pin name="P$20" x="12.7" y="10.16" length="middle" rot="R180"/>
<text x="-2.54" y="15.24" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-17.78" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="PHX-6X2">
<wire x1="7.62" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<pin name="P$1" x="-12.7" y="7.62" length="middle"/>
<pin name="P$2" x="-12.7" y="5.08" length="middle"/>
<pin name="P$3" x="-12.7" y="2.54" length="middle"/>
<pin name="P$4" x="-12.7" y="0" length="middle"/>
<pin name="P$5" x="-12.7" y="-2.54" length="middle"/>
<pin name="P$6" x="-12.7" y="-5.08" length="middle"/>
<pin name="P$7" x="12.7" y="-5.08" length="middle" rot="R180"/>
<pin name="P$8" x="12.7" y="-2.54" length="middle" rot="R180"/>
<pin name="P$9" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="P$10" x="12.7" y="2.54" length="middle" rot="R180"/>
<pin name="P$11" x="12.7" y="5.08" length="middle" rot="R180"/>
<pin name="P$12" x="12.7" y="7.62" length="middle" rot="R180"/>
<text x="-2.54" y="12.7" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-10.16" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="OPAMP_2">
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="0" width="0.4064" layer="94"/>
<wire x1="5.08" y1="0" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<text x="-4.318" y="2.032" size="1.27" layer="94" font="vector">-</text>
<text x="-4.318" y="-3.302" size="1.27" layer="94" font="vector">+</text>
<text x="2.032" y="2.54" size="1.27" layer="94" font="vector" rot="R90">+V</text>
<text x="2.032" y="-5.588" size="1.27" layer="94" font="vector" rot="R90">-V</text>
<text x="-1.27" y="5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<text x="-7.62" y="5.08" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<pin name="-IN1" x="-7.62" y="2.54" visible="pad" length="short" direction="in"/>
<pin name="+IN1" x="-7.62" y="-2.54" visible="pad" length="short" direction="in"/>
<pin name="OUT1" x="7.62" y="0" visible="pad" length="short" direction="out" rot="R180"/>
<pin name="+V" x="0" y="5.08" visible="pad" length="short" direction="pwr" rot="R270"/>
<pin name="-V" x="0" y="-5.08" visible="pad" length="short" direction="pwr" rot="R90"/>
</symbol>
<symbol name="OPAMP_2_1">
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="0" width="0.4064" layer="94"/>
<wire x1="5.08" y1="0" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<text x="-4.318" y="2.032" size="1.27" layer="94" font="vector">-</text>
<text x="-4.318" y="-3.302" size="1.27" layer="94" font="vector">+</text>
<text x="-6.35" y="6.35" size="1.778" layer="95">&gt;NAME</text>
<pin name="OUT2" x="7.62" y="0" visible="pad" length="short" direction="out" rot="R180"/>
<pin name="-IN2" x="-7.62" y="2.54" visible="pad" length="short" direction="in"/>
<pin name="+IN2" x="-7.62" y="-2.54" visible="pad" length="short" direction="in"/>
</symbol>
<symbol name="LED-BIDIRECTIONAL">
<description>&lt;h3&gt;LED&lt;/h3&gt;
&lt;p&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="0" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-3.81" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-3.81" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-3.81" y2="0" width="0.254" layer="94"/>
<wire x1="-4.572" y1="-0.762" x2="-5.969" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-5.842" y2="-3.302" width="0.1524" layer="94"/>
<text x="-7.62" y="0" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="7.62" y="0" size="1.778" layer="96" font="vector" rot="R90" align="top-center">&gt;VALUE</text>
<pin name="C" x="0" y="-7.62" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-5.969" y="-2.159"/>
<vertex x="-5.588" y="-1.27"/>
<vertex x="-5.08" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-5.842" y="-3.302"/>
<vertex x="-5.461" y="-2.413"/>
<vertex x="-4.953" y="-2.921"/>
</polygon>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="3.81" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="3.81" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.81" y2="-2.54" width="0.254" layer="94"/>
<wire x1="4.572" y1="-1.778" x2="5.969" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="4.445" y1="-0.635" x2="5.842" y2="0.762" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="5.969" y="-0.381"/>
<vertex x="5.588" y="-1.27"/>
<vertex x="5.08" y="-0.762"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="5.842" y="0.762"/>
<vertex x="5.461" y="-0.127"/>
<vertex x="4.953" y="0.381"/>
</polygon>
<wire x1="-2.54" y1="0" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="SWITCHING-REGULATOR-AP6320X">
<pin name="VIN" x="-10.16" y="2.54" length="short"/>
<pin name="EN" x="-10.16" y="0" length="short"/>
<pin name="GND" x="-10.16" y="-2.54" length="short"/>
<pin name="FB" x="10.16" y="-2.54" length="short" rot="R180"/>
<pin name="SW" x="10.16" y="0" length="short" rot="R180"/>
<pin name="BST" x="10.16" y="2.54" length="short" rot="R180"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="0" y="7.62" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="0" y="-7.62" size="1.778" layer="96" align="center">&gt;VALUE</text>
</symbol>
<symbol name="INDUCTOR">
<description>&lt;h3&gt;Inductors&lt;/h3&gt;
&lt;p&gt;Resist changes in electrical current. Basically a coil of wire.&lt;/p&gt;</description>
<text x="1.27" y="2.54" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="1.27" y="-2.54" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="1.27" width="0.1524" layer="94" curve="-180"/>
<wire x1="0" y1="1.27" x2="0" y2="0" width="0.1524" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="0" y2="-1.27" width="0.1524" layer="94" curve="-180"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.1524" layer="94" curve="-180"/>
</symbol>
<symbol name="CAPACITOR-P">
<rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94"/>
<rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-1.0161" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.0161" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.376341" cap="flat"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ZENER_17_DO-214AA" prefix="D" uservalue="yes">
<description>304020021</description>
<gates>
<gate name="G$1" symbol="DIODE-ZENER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DO-214AA">
<connects>
<connect gate="G$1" pin="A" pad="+"/>
<connect gate="G$1" pin="C" pad="-"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32511/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MPN" value="SS54B" constant="no"/>
<attribute name="VALUE" value="28V-5A"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SCHOTTKY_DIODE_SDT30B100D1-13" prefix="D">
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO-252">
<connects>
<connect gate="G$1" pin="+" pad="P$1 P$3"/>
<connect gate="G$1" pin="-" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="KEYSTONE_FUSE_HOLDER_3568" prefix="F">
<gates>
<gate name="G$1" symbol="FUSE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FUSE_HOLDER">
<connects>
<connect gate="G$1" pin="1" pad="1 2"/>
<connect gate="G$1" pin="2" pad="3 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FUSE" prefix="F'">
<gates>
<gate name="G$1" symbol="FUSE-CARTRIDGE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FUSE-CARTRIDGE">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="D24V150F6" prefix="A">
<description>Pololu 6V, 15A Step-Down Voltage Regulator D24V150F6</description>
<gates>
<gate name="G$1" symbol="D24V150FX" x="0" y="0"/>
</gates>
<devices>
<device name="" package="D24V150FX">
<connects>
<connect gate="G$1" pin="EN" pad="9"/>
<connect gate="G$1" pin="GIN1" pad="3"/>
<connect gate="G$1" pin="GIN2" pad="4"/>
<connect gate="G$1" pin="GOUT1" pad="10"/>
<connect gate="G$1" pin="GOUT2" pad="11"/>
<connect gate="G$1" pin="VIN1" pad="1"/>
<connect gate="G$1" pin="VIN2" pad="2"/>
<connect gate="G$1" pin="VOUT1" pad="12"/>
<connect gate="G$1" pin="VOUT2" pad="13"/>
</connects>
<technologies>
<technology name="">
<attribute name="LINK" value="https://www.pololu.com/product/2882"/>
<attribute name="MANUFACTURER" value="Pololu"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="D24V150F6"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="D24V150F12" prefix="A">
<description>Pololu 12V, 15A Step-Down Voltage Regulator D24V150F12</description>
<gates>
<gate name="G$1" symbol="D24V150FX" x="0" y="0"/>
</gates>
<devices>
<device name="" package="D24V150FX">
<connects>
<connect gate="G$1" pin="EN" pad="9"/>
<connect gate="G$1" pin="GIN1" pad="3"/>
<connect gate="G$1" pin="GIN2" pad="4"/>
<connect gate="G$1" pin="GOUT1" pad="10"/>
<connect gate="G$1" pin="GOUT2" pad="11"/>
<connect gate="G$1" pin="VIN1" pad="1"/>
<connect gate="G$1" pin="VIN2" pad="2"/>
<connect gate="G$1" pin="VOUT1" pad="12"/>
<connect gate="G$1" pin="VOUT2" pad="13"/>
</connects>
<technologies>
<technology name="">
<attribute name="LINK" value="https://www.pololu.com/product/2885"/>
<attribute name="MANUFACTURER" value="Pololu"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="D24V150F12"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET_ARRAY_SQJ951EP-T1_GE3" prefix="Q">
<gates>
<gate name="G$1" symbol="LABELED-PMOS" x="0" y="0"/>
<gate name="G$2" symbol="LABELED-PMOS" x="20.32" y="0"/>
</gates>
<devices>
<device name="" package="POWERPAK_SO-8L">
<connects>
<connect gate="G$1" pin="D" pad="P$6"/>
<connect gate="G$1" pin="G" pad="P$2"/>
<connect gate="G$1" pin="S" pad="P$1"/>
<connect gate="G$2" pin="D" pad="P$5"/>
<connect gate="G$2" pin="G" pad="P$4"/>
<connect gate="G$2" pin="S" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BJT_ARRAY_MBT3904DW1T1G" prefix="Q">
<gates>
<gate name="G$1" symbol="NPN" x="0" y="0"/>
<gate name="G$2" symbol="NPN" x="20.32" y="0"/>
</gates>
<devices>
<device name="" package="SC70-6_6-TSSOP_SC-88_SOT-363">
<connects>
<connect gate="G$1" pin="B" pad="5"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="4"/>
<connect gate="G$2" pin="B" pad="2"/>
<connect gate="G$2" pin="C" pad="6"/>
<connect gate="G$2" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829815/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PHX-2X1" prefix="J">
<description>TERM BLK 2P SIDE ENT 5.08MM PCB</description>
<gates>
<gate name="G$1" symbol="PHX-2X1" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="PHX-2X1">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="277-17514-ND"/>
<attribute name="LINK" value="https://www.digikey.ca/product-detail/en/1017503/277-17514-ND/9346604/?itemSeq=289167062"/>
<attribute name="MANUFACTURER" value="Phoenix Contact"/>
<attribute name="MANUFACTURER_NUMBER" value="1017503"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ACS711KEXLT-31AB-T" prefix="IC">
<description>&lt;b&gt;Hall-Effect Linear Current Sensor with Overcurrent Fault Output for &lt; 100 V Isolation Applications&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.allegromicro.com/en/Products/Current-Sensor-ICs/Zero-To-Fifty-Amp-Integrated-Conductor-Sensor-ICs/~/media/Files/Datasheets/ACS711-Datasheet.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="ACS711KEXLT-31AB-T" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EX-PACKAGE">
<connects>
<connect gate="G$1" pin="!FAULT" pad="4"/>
<connect gate="G$1" pin="GND" pad="3 5 6 7 8"/>
<connect gate="G$1" pin="IP+" pad="1"/>
<connect gate="G$1" pin="IP-" pad="2"/>
<connect gate="G$1" pin="VCC" pad="10"/>
<connect gate="G$1" pin="VIOUT" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Hall-Effect Linear Current Sensor with Overcurrent Fault Output for &lt; 100 V Isolation Applications" constant="no"/>
<attribute name="HEIGHT" value="mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Allegro Microsystems" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="ACS711KEXLT-31AB-T" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED_DISPLAY_MODULE_LDT-N2804RI" prefix="D">
<gates>
<gate name="G$1" symbol="LED_DISPLAY_MODULE_LDT-N2804RI" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED_DISPLAY_MODULE_LDT-N2804RI">
<connects>
<connect gate="G$1" pin="A" pad="P$11"/>
<connect gate="G$1" pin="B" pad="P$7"/>
<connect gate="G$1" pin="C" pad="P$4"/>
<connect gate="G$1" pin="D" pad="P$2"/>
<connect gate="G$1" pin="DIG1" pad="P$12"/>
<connect gate="G$1" pin="DIG2" pad="P$9"/>
<connect gate="G$1" pin="DIG3" pad="P$8"/>
<connect gate="G$1" pin="DP" pad="P$3"/>
<connect gate="G$1" pin="E" pad="P$1"/>
<connect gate="G$1" pin="F" pad="P$10"/>
<connect gate="G$1" pin="G" pad="P$5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED_DRIVER_AS1108WL-T" prefix="IC">
<gates>
<gate name="G$1" symbol="LED_DRIVER_AS1108WL-T" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC20R">
<connects>
<connect gate="G$1" pin="CLK" pad="10"/>
<connect gate="G$1" pin="DIG0" pad="3"/>
<connect gate="G$1" pin="DIG1" pad="8"/>
<connect gate="G$1" pin="DIG2" pad="5"/>
<connect gate="G$1" pin="DIG3" pad="6"/>
<connect gate="G$1" pin="DIN" pad="2"/>
<connect gate="G$1" pin="DOUT" pad="1"/>
<connect gate="G$1" pin="GND" pad="4 7"/>
<connect gate="G$1" pin="ISET" pad="15"/>
<connect gate="G$1" pin="LOAD/CSN" pad="9"/>
<connect gate="G$1" pin="SEG_A" pad="11"/>
<connect gate="G$1" pin="SEG_B" pad="13"/>
<connect gate="G$1" pin="SEG_C" pad="17"/>
<connect gate="G$1" pin="SEG_D" pad="20"/>
<connect gate="G$1" pin="SEG_DP" pad="19"/>
<connect gate="G$1" pin="SEG_E" pad="18"/>
<connect gate="G$1" pin="SEG_F" pad="12"/>
<connect gate="G$1" pin="SEG_G" pad="14"/>
<connect gate="G$1" pin="VDD" pad="16"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="STM32F103RCT6" prefix="U">
<description>ARM-based 32-bit MCU</description>
<gates>
<gate name="A" symbol="STM32F103RCT6" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFP50P1200X1200X160-64N">
<connects>
<connect gate="A" pin="BOOT0" pad="60"/>
<connect gate="A" pin="NRST" pad="7"/>
<connect gate="A" pin="PA0-WKUP" pad="14"/>
<connect gate="A" pin="PA1" pad="15"/>
<connect gate="A" pin="PA10" pad="43"/>
<connect gate="A" pin="PA11" pad="44"/>
<connect gate="A" pin="PA12" pad="45"/>
<connect gate="A" pin="PA13" pad="46"/>
<connect gate="A" pin="PA14" pad="49"/>
<connect gate="A" pin="PA15" pad="50"/>
<connect gate="A" pin="PA2" pad="16"/>
<connect gate="A" pin="PA3" pad="17"/>
<connect gate="A" pin="PA4" pad="20"/>
<connect gate="A" pin="PA5" pad="21"/>
<connect gate="A" pin="PA6" pad="22"/>
<connect gate="A" pin="PA7" pad="23"/>
<connect gate="A" pin="PA8" pad="41"/>
<connect gate="A" pin="PA9" pad="42"/>
<connect gate="A" pin="PB0" pad="26"/>
<connect gate="A" pin="PB1" pad="27"/>
<connect gate="A" pin="PB10" pad="29"/>
<connect gate="A" pin="PB11" pad="30"/>
<connect gate="A" pin="PB12" pad="33"/>
<connect gate="A" pin="PB13" pad="34"/>
<connect gate="A" pin="PB14" pad="35"/>
<connect gate="A" pin="PB15" pad="36"/>
<connect gate="A" pin="PB1_2" pad="28"/>
<connect gate="A" pin="PB3" pad="55"/>
<connect gate="A" pin="PB4" pad="56"/>
<connect gate="A" pin="PB5" pad="57"/>
<connect gate="A" pin="PB6" pad="58"/>
<connect gate="A" pin="PB7" pad="59"/>
<connect gate="A" pin="PB8" pad="61"/>
<connect gate="A" pin="PB9" pad="62"/>
<connect gate="A" pin="PC0" pad="8"/>
<connect gate="A" pin="PC1" pad="9"/>
<connect gate="A" pin="PC10" pad="51"/>
<connect gate="A" pin="PC11" pad="52"/>
<connect gate="A" pin="PC12" pad="53"/>
<connect gate="A" pin="PC13-TAMPER-RTC" pad="2"/>
<connect gate="A" pin="PC14-OSC32_IN" pad="3"/>
<connect gate="A" pin="PC15-OSC32_OUT" pad="4"/>
<connect gate="A" pin="PC2" pad="10"/>
<connect gate="A" pin="PC3" pad="11"/>
<connect gate="A" pin="PC4" pad="24"/>
<connect gate="A" pin="PC5" pad="25"/>
<connect gate="A" pin="PC6" pad="37"/>
<connect gate="A" pin="PC7" pad="38"/>
<connect gate="A" pin="PC8" pad="39"/>
<connect gate="A" pin="PC9" pad="40"/>
<connect gate="A" pin="PD0_OSC_IN" pad="5"/>
<connect gate="A" pin="PD1_OSC_OUT" pad="6"/>
<connect gate="A" pin="PD2" pad="54"/>
<connect gate="A" pin="VBAT" pad="1"/>
<connect gate="A" pin="VDDA" pad="13"/>
<connect gate="A" pin="VDD_1" pad="32"/>
<connect gate="A" pin="VDD_2" pad="48"/>
<connect gate="A" pin="VDD_3" pad="64"/>
<connect gate="A" pin="VDD_4" pad="19"/>
<connect gate="A" pin="VSSA" pad="12"/>
<connect gate="A" pin="VSS_1" pad="31"/>
<connect gate="A" pin="VSS_2" pad="47"/>
<connect gate="A" pin="VSS_3" pad="63"/>
<connect gate="A" pin="VSS_4" pad="18"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" ARM® Cortex®-M3 STM32F1 Microcontroller IC 32-Bit 72MHz 256KB _256K x 8_ FLASH "/>
<attribute name="MF" value="STMicroelectronics"/>
<attribute name="MP" value="STM32F103RCT6"/>
<attribute name="PACKAGE" value="LQFP-64 STMicroelectronics"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JTAG" prefix="J">
<gates>
<gate name="G$1" symbol="JTAG" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X05">
<connects>
<connect gate="G$1" pin="GND1" pad="3"/>
<connect gate="G$1" pin="GND2" pad="5"/>
<connect gate="G$1" pin="GND_DETECT" pad="9"/>
<connect gate="G$1" pin="KEY" pad="7"/>
<connect gate="G$1" pin="NC/TDI" pad="8"/>
<connect gate="G$1" pin="NRESET" pad="10"/>
<connect gate="G$1" pin="SWCLK/TCK" pad="4"/>
<connect gate="G$1" pin="SWDIO/TMS" pad="2"/>
<connect gate="G$1" pin="SWO/TDO" pad="6"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22470/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SD_0472192001" prefix="J">
<description>&lt;b&gt;Memory Card Connectors 1.10MM MICRO SD 08P HINGE TYPE&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://www.molex.com/pdm_docs/sd/472192001_sd.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="SD_0472192001" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SD_0472192001">
<connects>
<connect gate="G$1" pin="CD/DAT3" pad="2"/>
<connect gate="G$1" pin="CLK" pad="5"/>
<connect gate="G$1" pin="CMD" pad="3"/>
<connect gate="G$1" pin="DAT0" pad="7"/>
<connect gate="G$1" pin="DAT1" pad="8"/>
<connect gate="G$1" pin="DAT2" pad="1"/>
<connect gate="G$1" pin="SHIELD" pad="G1 G2 G3 G4"/>
<connect gate="G$1" pin="VDD" pad="4"/>
<connect gate="G$1" pin="VSS" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Memory Card Connectors 1.10MM MICRO SD 08P HINGE TYPE" constant="no"/>
<attribute name="HEIGHT" value="1.9mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Molex" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="0472192001" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="N/A" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=N%2FA" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="XT60" prefix="J">
<description>XT60</description>
<gates>
<gate name="G$1" symbol="XT60" x="2.54" y="5.08"/>
</gates>
<devices>
<device name="" package="XT60">
<connects>
<connect gate="G$1" pin="GND" pad="-"/>
<connect gate="G$1" pin="VCC" pad="+"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JST-5" prefix="J">
<gates>
<gate name="G$1" symbol="JST-5" x="0" y="0"/>
</gates>
<devices>
<device name="" package="B5B-PH-SM4-TB">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="SHIELD" pad="6 7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PHX-12X2" prefix="J">
<description>TERM BLK 12P SIDE ENT 3.5MM PCB</description>
<gates>
<gate name="G$1" symbol="PHX-12X2" x="7.62" y="10.16"/>
</gates>
<devices>
<device name="" package="PHX-12X2">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$10" pad="P$10"/>
<connect gate="G$1" pin="P$11" pad="P$11"/>
<connect gate="G$1" pin="P$12" pad="P$12"/>
<connect gate="G$1" pin="P$13" pad="P$13"/>
<connect gate="G$1" pin="P$14" pad="P$14"/>
<connect gate="G$1" pin="P$15" pad="P$15"/>
<connect gate="G$1" pin="P$16" pad="P$16"/>
<connect gate="G$1" pin="P$17" pad="P$17"/>
<connect gate="G$1" pin="P$18" pad="P$18"/>
<connect gate="G$1" pin="P$19" pad="P$19"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$20" pad="P$20"/>
<connect gate="G$1" pin="P$21" pad="P$21"/>
<connect gate="G$1" pin="P$22" pad="P$22"/>
<connect gate="G$1" pin="P$23" pad="P$23"/>
<connect gate="G$1" pin="P$24" pad="P$24"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
<connect gate="G$1" pin="P$5" pad="P$5"/>
<connect gate="G$1" pin="P$6" pad="P$6"/>
<connect gate="G$1" pin="P$7" pad="P$7"/>
<connect gate="G$1" pin="P$8" pad="P$8"/>
<connect gate="G$1" pin="P$9" pad="P$9"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="277-10325-ND"/>
<attribute name="LINK" value="https://www.digikey.ca/product-detail/en/1841597/277-10325-ND/5428705/?itemSeq=289164530"/>
<attribute name="MANUFACTURER" value="Phoenix Contact"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="1841597" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PHX-10X2" prefix="J">
<gates>
<gate name="G$1" symbol="PHX-10X2" x="10.16" y="7.62"/>
</gates>
<devices>
<device name="" package="PHX-10X2">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$10" pad="P$10"/>
<connect gate="G$1" pin="P$11" pad="P$11"/>
<connect gate="G$1" pin="P$12" pad="P$12"/>
<connect gate="G$1" pin="P$13" pad="P$13"/>
<connect gate="G$1" pin="P$14" pad="P$14"/>
<connect gate="G$1" pin="P$15" pad="P$15"/>
<connect gate="G$1" pin="P$16" pad="P$16"/>
<connect gate="G$1" pin="P$17" pad="P$17"/>
<connect gate="G$1" pin="P$18" pad="P$18"/>
<connect gate="G$1" pin="P$19" pad="P$19"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$20" pad="P$20"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
<connect gate="G$1" pin="P$5" pad="P$5"/>
<connect gate="G$1" pin="P$6" pad="P$6"/>
<connect gate="G$1" pin="P$7" pad="P$7"/>
<connect gate="G$1" pin="P$8" pad="P$8"/>
<connect gate="G$1" pin="P$9" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PHX-6X2" prefix="J">
<description>TERM BLK 6POS SIDE ENT 3.5MM PCB</description>
<gates>
<gate name="G$1" symbol="PHX-6X2" x="7.62" y="5.08"/>
</gates>
<devices>
<device name="" package="PHX-6X2">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$10" pad="P$10"/>
<connect gate="G$1" pin="P$11" pad="P$11"/>
<connect gate="G$1" pin="P$12" pad="P$12"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
<connect gate="G$1" pin="P$5" pad="P$5"/>
<connect gate="G$1" pin="P$6" pad="P$6"/>
<connect gate="G$1" pin="P$7" pad="P$7"/>
<connect gate="G$1" pin="P$8" pad="P$8"/>
<connect gate="G$1" pin="P$9" pad="P$9"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="277-10319-ND"/>
<attribute name="LINK" value="https://www.digikey.ca/product-detail/en/1841539/277-10319-ND/5428700/?itemSeq=289164599"/>
<attribute name="MANUFACTURER" value="Phoenix Contact"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="1841539"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="COMPARATOR_LM393DT" prefix="IC">
<gates>
<gate name="G$1" symbol="OPAMP_2" x="0" y="0"/>
<gate name="G$2" symbol="OPAMP_2_1" x="0" y="-17.78"/>
</gates>
<devices>
<device name="" package="SOIC127P600X144-8">
<connects>
<connect gate="G$1" pin="+IN1" pad="3"/>
<connect gate="G$1" pin="+V" pad="8"/>
<connect gate="G$1" pin="-IN1" pad="2"/>
<connect gate="G$1" pin="-V" pad="4"/>
<connect gate="G$1" pin="OUT1" pad="1"/>
<connect gate="G$2" pin="+IN2" pad="5"/>
<connect gate="G$2" pin="-IN2" pad="6"/>
<connect gate="G$2" pin="OUT2" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED-BIDIRECTIONAL-1206" prefix="D">
<gates>
<gate name="G$1" symbol="LED-BIDIRECTIONAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SWITCHING-REGULATOR-AP6320X" prefix="IC">
<gates>
<gate name="G$1" symbol="SWITCHING-REGULATOR-AP6320X" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-23-6">
<connects>
<connect gate="G$1" pin="BST" pad="6"/>
<connect gate="G$1" pin="EN" pad="2"/>
<connect gate="G$1" pin="FB" pad="1"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="SW" pad="5"/>
<connect gate="G$1" pin="VIN" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="INDUCTOR_3.9UH_1255AY-3R9N=P3" prefix="L">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="INDUCTOR_3.9UH_1255AY-3R9N=P3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR-φ6.3" prefix="C">
<gates>
<gate name="G$1" symbol="CAPACITOR-P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPACITOR-φ6.3">
<connects>
<connect gate="G$1" pin="+" pad="P$1"/>
<connect gate="G$1" pin="-" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="DINA4_L" urn="urn:adsk.eagle:symbol:13867/1" library_version="1">
<frame x1="0" y1="0" x2="264.16" y2="180.34" columns="4" rows="4" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD" urn="urn:adsk.eagle:symbol:13864/1" library_version="1">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA4_L" urn="urn:adsk.eagle:component:13919/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with extra doc field</description>
<gates>
<gate name="G$1" symbol="DINA4_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="162.56" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BeagleBone_Blue_R3" urn="urn:adsk.eagle:library:5828899">
<description>Generated from &lt;b&gt;BeagleBone_Blue.sch&lt;/b&gt;&lt;p&gt;
by exp-lbrs.ulp</description>
<packages>
<package name="0402-CAP" urn="urn:adsk.eagle:footprint:5829087/1" library_version="49">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.092" y1="0.483" x2="1.092" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.092" y1="0.483" x2="1.092" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.092" y1="-0.483" x2="-1.092" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.092" y1="-0.483" x2="-1.092" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.4064" layer="21"/>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<smd name="1" x="-0.5588" y="0" dx="0.6096" dy="0.508" layer="1"/>
<smd name="2" x="0.5588" y="0" dx="0.6096" dy="0.508" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
</package>
<package name="C0603" urn="urn:adsk.eagle:footprint:5829372/1" library_version="49">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.127" layer="21"/>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.889" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0805" urn="urn:adsk.eagle:footprint:8076988/1" library_version="49">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="B0,65" urn="urn:adsk.eagle:footprint:5828980/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<wire x1="-0.635" y1="0" x2="0.635" y2="0" width="0.0024" layer="37"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.0024" layer="37"/>
<smd name="TP" x="0" y="0" dx="0.635" dy="0.635" layer="1" roundness="100" cream="no"/>
<text x="-0.635" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-0.635" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="B1,27" urn="urn:adsk.eagle:footprint:5828981/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<wire x1="-0.635" y1="0" x2="0.635" y2="0" width="0.0024" layer="37"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.0024" layer="37"/>
<smd name="TP" x="0" y="0" dx="1.27" dy="1.27" layer="1" roundness="100" cream="no"/>
<text x="-0.635" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-0.635" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="B2,54" urn="urn:adsk.eagle:footprint:5828982/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.635" width="0.254" layer="37"/>
<wire x1="-0.635" y1="0" x2="0.635" y2="0" width="0.0024" layer="37"/>
<wire x1="0" y1="-0.635" x2="0" y2="0.635" width="0.0024" layer="37"/>
<smd name="TP" x="0" y="0" dx="2.54" dy="2.54" layer="1" roundness="100" cream="no"/>
<text x="-1.27" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-1.397" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-3.175" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP06R" urn="urn:adsk.eagle:footprint:5828999/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.6" dy="0.6" layer="1" roundness="100" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.254" y="-0.381" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP06SQ" urn="urn:adsk.eagle:footprint:5829000/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.5996" dy="0.5996" layer="1" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.254" y="-0.381" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP07R" urn="urn:adsk.eagle:footprint:5829001/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.7" dy="0.7" layer="1" roundness="100" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.254" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP07SQ" urn="urn:adsk.eagle:footprint:5829002/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.7" dy="0.7" layer="1" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.381" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP08R" urn="urn:adsk.eagle:footprint:5829003/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.8" dy="0.8" layer="1" roundness="100" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.381" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP08SQ" urn="urn:adsk.eagle:footprint:5829004/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.8" dy="0.8" layer="1" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP09R" urn="urn:adsk.eagle:footprint:5829005/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.9" dy="0.9" layer="1" roundness="100" cream="no"/>
<text x="-0.4501" y="0.5001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP09SQ" urn="urn:adsk.eagle:footprint:5829006/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.8998" dy="0.8998" layer="1" cream="no"/>
<text x="-0.4501" y="0.5001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP10R" urn="urn:adsk.eagle:footprint:5829007/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" cream="no"/>
<text x="-0.5001" y="0.5499" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP10SQ" urn="urn:adsk.eagle:footprint:5829008/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1" dy="1" layer="1" cream="no"/>
<text x="-0.5001" y="0.5499" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.635" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP11R" urn="urn:adsk.eagle:footprint:5829009/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.1" dy="1.1" layer="1" roundness="100" cream="no"/>
<text x="-0.5499" y="0.5999" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP11SQ" urn="urn:adsk.eagle:footprint:5829010/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.1" dy="1.1" layer="1" cream="no"/>
<text x="-0.5499" y="0.5999" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.635" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP12R" urn="urn:adsk.eagle:footprint:5829011/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.2" dy="1.2" layer="1" roundness="100" cream="no"/>
<text x="-0.5999" y="0.65" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.635" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP12SQ" urn="urn:adsk.eagle:footprint:5829012/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.1998" dy="1.1998" layer="1" cream="no"/>
<text x="-0.5999" y="0.65" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.635" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP13R" urn="urn:adsk.eagle:footprint:5829013/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.3" dy="1.3" layer="1" roundness="100" cream="no"/>
<text x="-0.65" y="0.7" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.635" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP13SQ" urn="urn:adsk.eagle:footprint:5829014/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.3" dy="1.3" layer="1" cream="no"/>
<text x="-0.65" y="0.7" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP14R" urn="urn:adsk.eagle:footprint:5829015/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.4" dy="1.4" layer="1" roundness="100" cream="no"/>
<text x="-0.7" y="0.7501" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP14SQ" urn="urn:adsk.eagle:footprint:5829016/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.4" dy="1.4" layer="1" cream="no"/>
<text x="-0.7" y="0.7501" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP15R" urn="urn:adsk.eagle:footprint:5829017/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.5" dy="1.5" layer="1" roundness="100" cream="no"/>
<text x="-0.7501" y="0.8001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP15SQ" urn="urn:adsk.eagle:footprint:5829018/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.5" dy="1.5" layer="1" cream="no"/>
<text x="-0.7501" y="0.8001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP16R" urn="urn:adsk.eagle:footprint:5829019/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.6" dy="1.6" layer="1" roundness="100" cream="no"/>
<text x="-0.8001" y="0.8499" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP16SQ" urn="urn:adsk.eagle:footprint:5829020/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.5996" dy="1.5996" layer="1" cream="no"/>
<text x="-0.8001" y="0.8499" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP17R" urn="urn:adsk.eagle:footprint:5829021/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.7" dy="1.7" layer="1" roundness="100" cream="no"/>
<text x="-0.8499" y="0.8999" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP17SQ" urn="urn:adsk.eagle:footprint:5829022/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.7" dy="1.7" layer="1" cream="no"/>
<text x="-0.8499" y="0.8999" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP18R" urn="urn:adsk.eagle:footprint:5829023/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.8" dy="1.8" layer="1" roundness="100" cream="no"/>
<text x="-0.8999" y="0.95" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP18SQ" urn="urn:adsk.eagle:footprint:5829024/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.8" dy="1.8" layer="1" cream="no"/>
<text x="-0.8999" y="0.95" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-1.016" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP19R" urn="urn:adsk.eagle:footprint:5829025/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100" cream="no"/>
<text x="-0.95" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP19SQ" urn="urn:adsk.eagle:footprint:5829026/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.8998" dy="1.8998" layer="1" cream="no"/>
<text x="-0.95" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-1.016" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP20R" urn="urn:adsk.eagle:footprint:5829027/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="2" dy="2" layer="1" roundness="100" cream="no"/>
<text x="-1" y="1.05" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-1.016" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP20SQ" urn="urn:adsk.eagle:footprint:5829028/1" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="2" dy="2" layer="1" cream="no"/>
<text x="-1" y="1.05" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
</packages>
<packages3d>
<package3d name="0402-CAP" urn="urn:adsk.eagle:package:5829530/3" type="model" library_version="49">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<packageinstances>
<packageinstance name="0402-CAP"/>
</packageinstances>
</package3d>
<package3d name="C0603" urn="urn:adsk.eagle:package:5829814/2" type="model" library_version="49">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<packageinstances>
<packageinstance name="C0603"/>
</packageinstances>
</package3d>
<package3d name="0805" urn="urn:adsk.eagle:package:5829534/4" type="model" library_version="49">
<packageinstances>
<packageinstance name="0805"/>
</packageinstances>
</package3d>
<package3d name="B0,65" urn="urn:adsk.eagle:package:5829430/2" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="B0,65"/>
</packageinstances>
</package3d>
<package3d name="B1,27" urn="urn:adsk.eagle:package:5829429/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="B1,27"/>
</packageinstances>
</package3d>
<package3d name="B2,54" urn="urn:adsk.eagle:package:5829428/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="B2,54"/>
</packageinstances>
</package3d>
<package3d name="TP06R" urn="urn:adsk.eagle:package:5829442/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP06R"/>
</packageinstances>
</package3d>
<package3d name="TP06SQ" urn="urn:adsk.eagle:package:5829443/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP06SQ"/>
</packageinstances>
</package3d>
<package3d name="TP07R" urn="urn:adsk.eagle:package:5829444/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP07R"/>
</packageinstances>
</package3d>
<package3d name="TP07SQ" urn="urn:adsk.eagle:package:5829445/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP07SQ"/>
</packageinstances>
</package3d>
<package3d name="TP08R" urn="urn:adsk.eagle:package:5829446/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP08R"/>
</packageinstances>
</package3d>
<package3d name="TP08SQ" urn="urn:adsk.eagle:package:5829447/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP08SQ"/>
</packageinstances>
</package3d>
<package3d name="TP09R" urn="urn:adsk.eagle:package:5829448/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP09R"/>
</packageinstances>
</package3d>
<package3d name="TP09SQ" urn="urn:adsk.eagle:package:5829449/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP09SQ"/>
</packageinstances>
</package3d>
<package3d name="TP10R" urn="urn:adsk.eagle:package:5829450/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP10R"/>
</packageinstances>
</package3d>
<package3d name="TP10SQ" urn="urn:adsk.eagle:package:5829451/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP10SQ"/>
</packageinstances>
</package3d>
<package3d name="TP11R" urn="urn:adsk.eagle:package:5829452/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP11R"/>
</packageinstances>
</package3d>
<package3d name="TP11SQ" urn="urn:adsk.eagle:package:5829453/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP11SQ"/>
</packageinstances>
</package3d>
<package3d name="TP12R" urn="urn:adsk.eagle:package:5829454/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP12R"/>
</packageinstances>
</package3d>
<package3d name="TP12SQ" urn="urn:adsk.eagle:package:5829455/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP12SQ"/>
</packageinstances>
</package3d>
<package3d name="TP13R" urn="urn:adsk.eagle:package:5829456/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP13R"/>
</packageinstances>
</package3d>
<package3d name="TP13SQ" urn="urn:adsk.eagle:package:5829457/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP13SQ"/>
</packageinstances>
</package3d>
<package3d name="TP14R" urn="urn:adsk.eagle:package:5829458/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP14R"/>
</packageinstances>
</package3d>
<package3d name="TP14SQ" urn="urn:adsk.eagle:package:5829459/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP14SQ"/>
</packageinstances>
</package3d>
<package3d name="TP15R" urn="urn:adsk.eagle:package:5829460/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP15R"/>
</packageinstances>
</package3d>
<package3d name="TP15SQ" urn="urn:adsk.eagle:package:5829461/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP15SQ"/>
</packageinstances>
</package3d>
<package3d name="TP16R" urn="urn:adsk.eagle:package:5829462/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP16R"/>
</packageinstances>
</package3d>
<package3d name="TP16SQ" urn="urn:adsk.eagle:package:5829463/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP16SQ"/>
</packageinstances>
</package3d>
<package3d name="TP17R" urn="urn:adsk.eagle:package:5829464/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP17R"/>
</packageinstances>
</package3d>
<package3d name="TP17SQ" urn="urn:adsk.eagle:package:5829465/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP17SQ"/>
</packageinstances>
</package3d>
<package3d name="TP18R" urn="urn:adsk.eagle:package:5829466/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP18R"/>
</packageinstances>
</package3d>
<package3d name="TP18SQ" urn="urn:adsk.eagle:package:5829467/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP18SQ"/>
</packageinstances>
</package3d>
<package3d name="TP19R" urn="urn:adsk.eagle:package:5829468/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP19R"/>
</packageinstances>
</package3d>
<package3d name="TP19SQ" urn="urn:adsk.eagle:package:5829469/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP19SQ"/>
</packageinstances>
</package3d>
<package3d name="TP20R" urn="urn:adsk.eagle:package:5829470/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP20R"/>
</packageinstances>
</package3d>
<package3d name="TP20SQ" urn="urn:adsk.eagle:package:5829471/1" type="box" library_version="49">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="TP20SQ"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="CAPACITOR-N" urn="urn:adsk.eagle:symbol:5828961/1" library_version="49">
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="TP" urn="urn:adsk.eagle:symbol:5828917/1" library_version="49">
<wire x1="-0.762" y1="-0.762" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="-0.762" x2="0" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0" y1="-1.524" x2="-0.762" y2="-0.762" width="0.254" layer="94"/>
<pin name="TP" x="0" y="-2.54" visible="off" length="short" direction="in" rot="R90"/>
<text x="-1.27" y="1.27" size="1.778" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAPACITOR-N" urn="urn:adsk.eagle:component:5829897/5" prefix="C" library_version="49">
<gates>
<gate name="G$1" symbol="CAPACITOR-N" x="0" y="-2.54"/>
</gates>
<devices>
<device name="0402" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829530/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829814/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829534/4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TP" urn="urn:adsk.eagle:component:5829874/2" prefix="TP" library_version="49">
<description>&lt;b&gt;Test pad&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="TP" x="0" y="0"/>
</gates>
<devices>
<device name="B0,65" package="B0,65">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829430/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B1,27" package="B1,27">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829429/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="B2,54" package="B2,54">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829428/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP06R" package="TP06R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829442/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP06SQ" package="TP06SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829443/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP07R" package="TP07R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829444/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP07SQ" package="TP07SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829445/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP08R" package="TP08R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829446/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP08SQ" package="TP08SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829447/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP09R" package="TP09R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829448/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP09SQ" package="TP09SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829449/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP10R" package="TP10R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829450/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP10SQ" package="TP10SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829451/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP11R" package="TP11R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829452/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP11SQ" package="TP11SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829453/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP12R" package="TP12R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829454/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP12SQ" package="TP12SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829455/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP13R" package="TP13R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829456/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP13SQ" package="TP13SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829457/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP14R" package="TP14R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829458/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP14SQ" package="TP14SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829459/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP15R" package="TP15R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829460/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP15SQ" package="TP15SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829461/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP16R" package="TP16R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829462/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP16SQ" package="TP16SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829463/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP17R" package="TP17R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829464/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP17SQ" package="TP17SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829465/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP18R" package="TP18R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829466/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP18SQ" package="TP18SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829467/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP19R" package="TP19R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829468/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP19SQ" package="TP19SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829469/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP20R" package="TP20R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829470/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP20SQ" package="TP20SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829471/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="adafruit" urn="urn:adsk.eagle:library:420">
<packages>
<package name="1X06" urn="urn:adsk.eagle:footprint:6240065/1" library_version="2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-0.635" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="2" x="-3.81" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="3" x="-1.27" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="4" x="1.27" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="5" x="3.81" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="6" x="6.35" y="0" drill="1.016" shape="octagon" rot="R90"/>
<text x="-7.6962" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-7.62" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
</package>
<package name="1X06-CLEAN" urn="urn:adsk.eagle:footprint:6240066/1" library_version="2">
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="2" x="-3.81" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="3" x="-1.27" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="4" x="1.27" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="5" x="3.81" y="0" drill="1.016" shape="octagon" rot="R90"/>
<pad name="6" x="6.35" y="0" drill="1.016" shape="octagon" rot="R90"/>
<text x="-7.6962" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-7.62" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
</package>
<package name="1X06-CLEANBIG" urn="urn:adsk.eagle:footprint:6240067/1" library_version="2">
<pad name="1" x="-6.35" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="2" x="-3.81" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="3" x="-1.27" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="4" x="1.27" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="5" x="3.81" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="6" x="6.35" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<text x="-7.6962" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-7.62" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
</package>
<package name="1X06-BIG" urn="urn:adsk.eagle:footprint:6240068/1" library_version="2">
<wire x1="-7.62" y1="1.27" x2="7.62" y2="1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="-7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-1.27" x2="-7.62" y2="1.27" width="0.127" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="2" x="-3.81" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="3" x="-1.27" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="4" x="1.27" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="5" x="3.81" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="6" x="6.35" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<text x="-7.6962" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-7.62" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
</package>
<package name="1X06-BIGLOCK" urn="urn:adsk.eagle:footprint:6240069/1" library_version="2">
<wire x1="-7.62" y1="1.27" x2="7.62" y2="1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="7.62" y1="-1.27" x2="-7.62" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-1.27" x2="-7.62" y2="1.27" width="0.127" layer="21"/>
<pad name="1" x="-6.35" y="0.127" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="2" x="-3.81" y="-0.127" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="3" x="-1.27" y="0.127" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="4" x="1.27" y="-0.127" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="5" x="3.81" y="0.127" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="6" x="6.35" y="-0.127" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<text x="-7.6962" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-7.62" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
</package>
<package name="1X06-3.5MM" urn="urn:adsk.eagle:footprint:6240173/1" library_version="2">
<wire x1="-10.5" y1="3.4" x2="-10.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-10.5" y1="-2.5" x2="-10.5" y2="-3.6" width="0.127" layer="21"/>
<wire x1="-10.5" y1="-3.6" x2="10.5" y2="-3.6" width="0.127" layer="21"/>
<wire x1="10.5" y1="-3.6" x2="10.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="10.5" y1="-2.5" x2="10.5" y2="3.4" width="0.127" layer="21"/>
<wire x1="10.5" y1="3.4" x2="-10.5" y2="3.4" width="0.127" layer="21"/>
<wire x1="-10.5" y1="-2.5" x2="10.5" y2="-2.5" width="0.127" layer="21"/>
<pad name="5" x="5.25" y="0" drill="1" diameter="2.1844"/>
<pad name="4" x="1.75" y="0" drill="1" diameter="2.1844"/>
<pad name="3" x="-1.75" y="0" drill="1" diameter="2.1844"/>
<pad name="2" x="-5.25" y="0" drill="1" diameter="2.1844"/>
<pad name="1" x="-8.75" y="0" drill="1" diameter="2.1844"/>
<pad name="6" x="8.75" y="0" drill="1" diameter="2.1844"/>
<text x="7.87" y="-5.81" size="1.27" layer="25" rot="R180">&gt;NAME</text>
</package>
</packages>
<packages3d>
<package3d name="1X06" urn="urn:adsk.eagle:package:6240711/1" type="box" library_version="2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="1X06"/>
</packageinstances>
</package3d>
<package3d name="1X06-CLEAN" urn="urn:adsk.eagle:package:6240712/1" type="box" library_version="2">
<packageinstances>
<packageinstance name="1X06-CLEAN"/>
</packageinstances>
</package3d>
<package3d name="1X06-CLEANBIG" urn="urn:adsk.eagle:package:6240713/1" type="box" library_version="2">
<packageinstances>
<packageinstance name="1X06-CLEANBIG"/>
</packageinstances>
</package3d>
<package3d name="1X06-BIG" urn="urn:adsk.eagle:package:6240714/1" type="box" library_version="2">
<packageinstances>
<packageinstance name="1X06-BIG"/>
</packageinstances>
</package3d>
<package3d name="1X06-BIGLOCK" urn="urn:adsk.eagle:package:6240715/1" type="box" library_version="2">
<packageinstances>
<packageinstance name="1X06-BIGLOCK"/>
</packageinstances>
</package3d>
<package3d name="1X06-3.5MM" urn="urn:adsk.eagle:package:6240818/1" type="box" library_version="2">
<packageinstances>
<packageinstance name="1X06-3.5MM"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PINHD6" urn="urn:adsk.eagle:symbol:6239537/1" library_version="2">
<wire x1="-6.35" y1="-7.62" x2="1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="10.16" x2="-6.35" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="10.16" x2="-6.35" y2="-7.62" width="0.4064" layer="94"/>
<text x="-6.35" y="10.795" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X6" urn="urn:adsk.eagle:component:6241026/1" prefix="JP" uservalue="yes" library_version="2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD6" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="1X06">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6240711/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CLEAN" package="1X06-CLEAN">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6240712/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CB" package="1X06-CLEANBIG">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6240713/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B" package="1X06-BIG">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6240714/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X06-BIGLOCK">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6240715/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3.5MM" package="1X06-3.5MM">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6240818/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-LED" urn="urn:adsk.eagle:library:529">
<description>&lt;h3&gt;SparkFun LEDs&lt;/h3&gt;
This library contains discrete LEDs for illumination or indication, but no displays.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="LED_5MM" urn="urn:adsk.eagle:footprint:39305/1" library_version="1">
<description>&lt;B&gt;LED 5mm PTH&lt;/B&gt;&lt;p&gt;
5 mm, round
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 5mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED-IR-THRU&lt;/li&gt;</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="22"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="0" y="3.3909" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0.0254" y="-3.3909" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
</package>
<package name="LED_3MM" urn="urn:adsk.eagle:footprint:39306/1" library_version="1">
<description>&lt;h3&gt;LED 3MM PTH&lt;/h3&gt;

3 mm, round.

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 3mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="22" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="22" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="22" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="22" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="22"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="22"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="0" y="2.286" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="LED-1206" urn="urn:adsk.eagle:footprint:39304/1" library_version="1">
<description>&lt;h3&gt;LED 1206 SMT&lt;/h3&gt;

1206, surface mount. 

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: &lt;/li&gt;
&lt;li&gt;Area: 0.125" x 0.06"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="2.4" y1="0.6825" x2="2.4" y2="-0.6825" width="0.2032" layer="21"/>
<wire x1="0.65375" y1="0.6825" x2="0.65375" y2="-0.6825" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="0.15875" y2="0.47625" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="0.15875" y2="-0.47625" width="0.2032" layer="51"/>
<smd name="A" x="-1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<smd name="C" x="1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<text x="0" y="0.9525" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.9525" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="LED-0603" urn="urn:adsk.eagle:footprint:39307/1" library_version="1">
<description>&lt;B&gt;LED 0603 SMT&lt;/B&gt;&lt;p&gt;
0603, surface mount.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.075inch &lt;/li&gt;
&lt;li&gt;Area: 0.06" x 0.03"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED - BLUE&lt;/li&gt;</description>
<smd name="C" x="0.877" y="0" dx="1" dy="1" layer="1" roundness="30" rot="R270"/>
<smd name="A" x="-0.877" y="0" dx="1" dy="1" layer="1" roundness="30" rot="R270"/>
<text x="0" y="0.635" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.635" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="1.5875" y1="0.47625" x2="1.5875" y2="-0.47625" width="0.127" layer="21"/>
<wire x1="0.15875" y1="0.47625" x2="0.15875" y2="0" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="0.15875" y2="-0.47625" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="-0.15875" y2="0.3175" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="-0.15875" y2="-0.3175" width="0.127" layer="51"/>
</package>
<package name="LED_10MM" urn="urn:adsk.eagle:footprint:39308/1" library_version="1">
<description>&lt;B&gt;LED 10mm PTH&lt;/B&gt;&lt;p&gt;
10 mm, round
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.2inch&lt;/li&gt;
&lt;li&gt;Diameter: 10mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;</description>
<wire x1="-5" y1="-2" x2="-5" y2="2" width="0.2032" layer="21" curve="316.862624"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="21"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="22"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.2032" layer="51"/>
<pad name="A" x="2.54" y="0" drill="2.4" diameter="3.7"/>
<pad name="C" x="-2.54" y="0" drill="2.4" diameter="3.7"/>
<text x="2.159" y="2.54" size="1.016" layer="51" ratio="15">L</text>
<text x="-2.921" y="2.54" size="1.016" layer="51" ratio="15">S</text>
<text x="0" y="5.715" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-5.715" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="FKIT-LED-1206" urn="urn:adsk.eagle:footprint:39309/1" library_version="1">
<description>&lt;B&gt;LED 1206 SMT&lt;/B&gt;&lt;p&gt;
1206, surface mount
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Area: 0.125"x 0.06" &lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="0.5" x2="-0.55" y2="-0.5" width="0.1016" layer="51" curve="84.547378"/>
<wire x1="0.55" y1="-0.5" x2="0.55" y2="0.5" width="0.1016" layer="51" curve="84.547378"/>
<wire x1="2.54" y1="0.9525" x2="2.54" y2="-0.9525" width="0.2032" layer="21"/>
<wire x1="0.3175" y1="0.635" x2="0.3175" y2="0" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="0.3175" y2="-0.635" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="0" y2="0.3175" width="0.2032" layer="51"/>
<wire x1="0.3175" y1="0" x2="0" y2="-0.3175" width="0.2032" layer="51"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="0" y="1.11125" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.11125" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
</package>
<package name="LED_3MM-NS" urn="urn:adsk.eagle:footprint:39310/1" library_version="1">
<description>&lt;h3&gt;LED 3MM PTH- No Silk&lt;/h3&gt;

3 mm, round, no silk outline of package

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 3mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="51" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="51" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="51" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="51" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="1.5748" y1="1.2954" x2="1.5748" y2="0.7874" width="0.254" layer="51"/>
<wire x1="1.5748" y1="-1.2954" x2="1.5748" y2="-0.8382" width="0.254" layer="51"/>
<wire x1="1.8923" y1="1.2954" x2="1.8923" y2="0.7874" width="0.254" layer="21"/>
<wire x1="1.8923" y1="-1.2954" x2="1.8923" y2="-0.8382" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128"/>
<pad name="K" x="1.27" y="0" drill="0.8128"/>
<text x="0" y="2.286" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="LED_5MM-KIT" urn="urn:adsk.eagle:footprint:39311/1" library_version="1">
<description>&lt;h3&gt;LED 5mm KIT PTH&lt;/h3&gt;
&lt;p&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1inch&lt;/li&gt;
&lt;li&gt;Diameter: 5mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example Device:
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="22"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796" stop="no"/>
<text x="0" y="3.3909" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0.0254" y="-3.3909" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<polygon width="0.127" layer="30">
<vertex x="-1.2675" y="-0.9525" curve="-90"/>
<vertex x="-2.2224" y="-0.0228" curve="-90.011749"/>
<vertex x="-1.27" y="0.9526" curve="-90"/>
<vertex x="-0.32" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="-1.7145" y="-0.0203" curve="-90"/>
<vertex x="-1.27" y="0.447" curve="-90"/>
<vertex x="-0.8281" y="-0.0101" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.2725" y="-0.9525" curve="-90"/>
<vertex x="0.3176" y="-0.0228" curve="-90.011749"/>
<vertex x="1.27" y="0.9526" curve="-90"/>
<vertex x="2.22" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.27" y="-0.4445" curve="-90.012891"/>
<vertex x="0.8255" y="-0.0203" curve="-90"/>
<vertex x="1.27" y="0.447" curve="-90"/>
<vertex x="1.7119" y="-0.0101" curve="-90.012967"/>
</polygon>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
</package>
<package name="LED-1206-BOTTOM" urn="urn:adsk.eagle:footprint:39312/1" library_version="1">
<description>&lt;h3&gt;LED 1206 SMT&lt;/h3&gt;

1206, surface mount. 

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Area: 0.125" x 0.06"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="-2" y1="0.4" x2="-2" y2="-0.4" width="0.127" layer="49"/>
<wire x1="-2.4" y1="0" x2="-1.6" y2="0" width="0.127" layer="49"/>
<wire x1="1.6" y1="0" x2="2.4" y2="0" width="0.127" layer="49"/>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0" x2="-0.381" y2="0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0.635" x2="0.254" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="-0.381" y2="-0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="1.27" y2="0" width="0.127" layer="49"/>
<wire x1="2.7686" y1="1.016" x2="2.7686" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.7686" y1="1.016" x2="2.7686" y2="-1.016" width="0.127" layer="22"/>
<rectangle x1="-0.75" y1="-0.75" x2="0.75" y2="0.75" layer="51"/>
<smd name="A" x="-1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<smd name="C" x="1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<hole x="0" y="0" drill="2.3"/>
<polygon width="0" layer="51">
<vertex x="1.1" y="-0.5"/>
<vertex x="1.1" y="0.5"/>
<vertex x="1.6" y="0.5"/>
<vertex x="1.6" y="0.25" curve="90"/>
<vertex x="1.4" y="0.05"/>
<vertex x="1.4" y="-0.05" curve="90"/>
<vertex x="1.6" y="-0.25"/>
<vertex x="1.6" y="-0.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-1.1" y="0.5"/>
<vertex x="-1.1" y="-0.5"/>
<vertex x="-1.6" y="-0.5"/>
<vertex x="-1.6" y="-0.25" curve="90"/>
<vertex x="-1.4" y="-0.05"/>
<vertex x="-1.4" y="0.05" curve="90"/>
<vertex x="-1.6" y="0.25"/>
<vertex x="-1.6" y="0.5"/>
</polygon>
<text x="0" y="1.27" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="LED_5MM" urn="urn:adsk.eagle:package:39353/1" type="box" library_version="1">
<description>LED 5mm PTH
5 mm, round
Specifications:
Pin count: 2
Pin pitch: 0.1inch
Diameter: 5mm

Example device(s):
LED-IR-THRU</description>
<packageinstances>
<packageinstance name="LED_5MM"/>
</packageinstances>
</package3d>
<package3d name="LED_3MM" urn="urn:adsk.eagle:package:39361/1" type="box" library_version="1">
<description>LED 3MM PTH

3 mm, round.

Specifications:
Pin count: 2
Pin pitch: 0.1inch
Diameter: 3mm

Example device(s):
LED</description>
<packageinstances>
<packageinstance name="LED_3MM"/>
</packageinstances>
</package3d>
<package3d name="LED-1206" urn="urn:adsk.eagle:package:39352/1" type="box" library_version="1">
<description>LED 1206 SMT

1206, surface mount. 

Specifications:
Pin count: 2
Pin pitch: 
Area: 0.125" x 0.06"

Example device(s):
LED</description>
<packageinstances>
<packageinstance name="LED-1206"/>
</packageinstances>
</package3d>
<package3d name="LED-0603" urn="urn:adsk.eagle:package:39354/1" type="box" library_version="1">
<description>LED 0603 SMT
0603, surface mount.
Specifications:
Pin count: 2
Pin pitch:0.075inch 
Area: 0.06" x 0.03"

Example device(s):
LED - BLUE</description>
<packageinstances>
<packageinstance name="LED-0603"/>
</packageinstances>
</package3d>
<package3d name="LED_10MM" urn="urn:adsk.eagle:package:39351/1" type="box" library_version="1">
<description>LED 10mm PTH
10 mm, round
Specifications:
Pin count: 2
Pin pitch: 0.2inch
Diameter: 10mm

Example device(s):
LED</description>
<packageinstances>
<packageinstance name="LED_10MM"/>
</packageinstances>
</package3d>
<package3d name="FKIT-LED-1206" urn="urn:adsk.eagle:package:39355/1" type="box" library_version="1">
<description>LED 1206 SMT
1206, surface mount
Specifications:
Pin count: 2
Area: 0.125"x 0.06" 

Example device(s):
LED</description>
<packageinstances>
<packageinstance name="FKIT-LED-1206"/>
</packageinstances>
</package3d>
<package3d name="LED_3MM-NS" urn="urn:adsk.eagle:package:39356/1" type="box" library_version="1">
<description>LED 3MM PTH- No Silk

3 mm, round, no silk outline of package

Specifications:
Pin count: 2
Pin pitch: 0.1inch
Diameter: 3mm

Example device(s):
LED</description>
<packageinstances>
<packageinstance name="LED_3MM-NS"/>
</packageinstances>
</package3d>
<package3d name="LED_5MM-KIT" urn="urn:adsk.eagle:package:39357/1" type="box" library_version="1">
<description>LED 5mm KIT PTH

Warning: This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.

Specifications:
Pin count: 2
Pin pitch: 0.1inch
Diameter: 5mm

Example Device:
LED
</description>
<packageinstances>
<packageinstance name="LED_5MM-KIT"/>
</packageinstances>
</package3d>
<package3d name="LED-1206-BOTTOM" urn="urn:adsk.eagle:package:39358/1" type="box" library_version="1">
<description>LED 1206 SMT

1206, surface mount. 

Specifications:
Pin count: 2
Area: 0.125" x 0.06"

Example device(s):
LED</description>
<packageinstances>
<packageinstance name="LED-1206-BOTTOM"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="LED" urn="urn:adsk.eagle:symbol:39303/1" library_version="1">
<description>&lt;h3&gt;LED&lt;/h3&gt;
&lt;p&gt;&lt;/p&gt;</description>
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="-3.429" y="-4.572" size="1.778" layer="95" font="vector" rot="R90">&gt;NAME</text>
<text x="1.905" y="-4.572" size="1.778" layer="96" font="vector" rot="R90" align="top-left">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" urn="urn:adsk.eagle:component:39399/1" prefix="D" uservalue="yes" library_version="1">
<description>&lt;b&gt;LED (Generic)&lt;/b&gt;
&lt;p&gt;Standard schematic elements and footprints for 5mm, 3mm, 1206, and 0603 sized LEDs. Generic LEDs with no color specified.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="5MM" package="LED_5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:39353/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED_3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:39361/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-08794" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:39352/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="LED-0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:39354/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED_10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:39351/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FKIT-1206" package="FKIT-LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:39355/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3MM-NO_SILK" package="LED_3MM-NS">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:39356/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM-KIT" package="LED_5MM-KIT">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:39357/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206-BOTTOM" package="LED-1206-BOTTOM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:39358/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Capacitors" urn="urn:adsk.eagle:library:510">
<description>&lt;h3&gt;SparkFun Capacitors&lt;/h3&gt;
This library contains capacitors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="PANASONIC_C" urn="urn:adsk.eagle:footprint:37405/1" library_version="1">
<description>&lt;b&gt;Panasonic Aluminium Electrolytic Capacitor VS-Serie Package E&lt;/b&gt;</description>
<wire x1="-2.6" y1="2.45" x2="1.6" y2="2.45" width="0.2032" layer="21"/>
<wire x1="1.6" y1="2.45" x2="2.7" y2="1.35" width="0.2032" layer="21"/>
<wire x1="2.7" y1="-1.75" x2="1.6" y2="-2.85" width="0.2032" layer="21"/>
<wire x1="1.6" y1="-2.85" x2="-2.6" y2="-2.85" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="2.45" x2="1.6" y2="2.45" width="0.1016" layer="51"/>
<wire x1="1.6" y1="2.45" x2="2.7" y2="1.35" width="0.1016" layer="51"/>
<wire x1="2.7" y1="-1.75" x2="1.6" y2="-2.85" width="0.1016" layer="51"/>
<wire x1="1.6" y1="-2.85" x2="-2.6" y2="-2.85" width="0.1016" layer="51"/>
<wire x1="-2.6" y1="2.45" x2="-2.6" y2="0.35" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="-2.85" x2="-2.6" y2="-0.75" width="0.2032" layer="21"/>
<wire x1="2.7" y1="1.35" x2="2.7" y2="0.35" width="0.2032" layer="21"/>
<wire x1="2.7" y1="-1.75" x2="2.7" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="-2.6" y1="2.45" x2="-2.6" y2="-2.85" width="0.1016" layer="51"/>
<wire x1="2.7" y1="1.35" x2="2.7" y2="-1.75" width="0.1016" layer="51"/>
<wire x1="-2.4" y1="0.35" x2="2.45" y2="0.3" width="0.2032" layer="21" curve="-156.699401"/>
<wire x1="2.5" y1="-0.7" x2="-2.4" y2="-0.75" width="0.2032" layer="21" curve="-154.694887"/>
<circle x="0.05" y="-0.2" radius="2.5004" width="0.1016" layer="51"/>
<smd name="-" x="-1.8" y="-0.2" dx="2.2" dy="0.65" layer="1"/>
<smd name="+" x="1.9" y="-0.2" dx="2.2" dy="0.65" layer="1"/>
<text x="0" y="2.667" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.048" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="PANASONIC_C" urn="urn:adsk.eagle:package:37430/1" type="box" library_version="1">
<description>Panasonic Aluminium Electrolytic Capacitor VS-Serie Package E</description>
<packageinstances>
<packageinstance name="PANASONIC_C"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="CAP_POL" urn="urn:adsk.eagle:symbol:37382/1" library_version="1">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.376341" cap="flat"/>
<text x="1.016" y="0.635" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94"/>
<rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="22UF-POLAR" urn="urn:adsk.eagle:component:37476/1" prefix="C" library_version="1">
<description>&lt;h3&gt;22µF polarized capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP_POL" x="0" y="0"/>
</gates>
<devices>
<device name="-PANASONIC_C-35V-20%" package="PANASONIC_C">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:37430/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08618"/>
<attribute name="VALUE" value="22uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="GND1" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="GND2" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="D2" library="Power Distribution Board" deviceset="ZENER_17_DO-214AA" device="" package3d_urn="urn:adsk.eagle:package:32511/1" value="17V">
<attribute name="DIGIKEY" value="SMB10J17AR5GDKR-ND"/>
<attribute name="MF" value="Taiwan Semiconductor Corporation"/>
</part>
<part name="D1" library="Power Distribution Board" deviceset="SCHOTTKY_DIODE_SDT30B100D1-13" device="">
<attribute name="DIGIKEY" value="SDT30B100D1-13DITR-ND"/>
<attribute name="MF" value="Diodes Incorporated"/>
<attribute name="MPN" value="SDT30B100D1-13"/>
</part>
<part name="F1" library="Power Distribution Board" deviceset="KEYSTONE_FUSE_HOLDER_3568" device=""/>
<part name="F'1" library="Power Distribution Board" deviceset="FUSE" device="" value="30A">
<attribute name="DIGIKEY" value="F995-ND"/>
<attribute name="MF" value="Littelfuse Inc."/>
<attribute name="MPN" value="0297030.WXNV"/>
</part>
<part name="A2" library="Power Distribution Board" deviceset="D24V150F6" device=""/>
<part name="A1" library="Power Distribution Board" deviceset="D24V150F12" device=""/>
<part name="GND3" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="GND5" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="GND7" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="GND8" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="Q2" library="Power Distribution Board" deviceset="BJT_ARRAY_MBT3904DW1T1G" device="">
<attribute name="DIGIKEY" value="MBT3904DW1T1GOSCT-ND"/>
<attribute name="MF" value="ON Semiconductor"/>
<attribute name="MPN" value="MBT3904DW1T1G"/>
</part>
<part name="GND9" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="R2" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="10K">
<attribute name="DIGIKEY" value="311-10.0KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-0710KL"/>
</part>
<part name="R3" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="10K">
<attribute name="DIGIKEY" value="311-10.0KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-0710KL"/>
</part>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_L" device=""/>
<part name="J5" library="Power Distribution Board" deviceset="PHX-2X1" device=""/>
<part name="IC2" library="Power Distribution Board" deviceset="ACS711KEXLT-31AB-T" device="">
<attribute name="DIGIKEY" value="620-1482-1-ND"/>
<attribute name="MF" value="Allegro MicroSystems"/>
<attribute name="MPN" value="ACS711KEXLT-31AB-T"/>
</part>
<part name="GND11" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="C13" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="0.1uF">
<attribute name="DIGIKEY" value="399-1177-1-ND"/>
<attribute name="MF" value="KEMET"/>
<attribute name="MPN" value="C0805C104Z5VACTU"/>
</part>
<part name="GND12" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="IC3" library="Power Distribution Board" deviceset="ACS711KEXLT-31AB-T" device="">
<attribute name="DIGIKEY" value="620-1482-1-ND"/>
<attribute name="MF" value="Allegro MicroSystems"/>
<attribute name="MPN" value="ACS711KEXLT-31AB-T"/>
</part>
<part name="GND13" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="C14" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="0.1uF">
<attribute name="DIGIKEY" value="399-1177-1-ND"/>
<attribute name="MF" value="KEMET"/>
<attribute name="MPN" value="C0805C104Z5VACTU"/>
</part>
<part name="GND14" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="IC4" library="Power Distribution Board" deviceset="ACS711KEXLT-31AB-T" device="">
<attribute name="DIGIKEY" value="620-1482-1-ND"/>
<attribute name="MF" value="Allegro MicroSystems"/>
<attribute name="MPN" value="ACS711KEXLT-31AB-T"/>
</part>
<part name="GND15" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="C15" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="0.1uF">
<attribute name="DIGIKEY" value="399-1177-1-ND"/>
<attribute name="MF" value="KEMET"/>
<attribute name="MPN" value="C0805C104Z5VACTU"/>
</part>
<part name="GND16" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="TP1" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="TP" device="B2,54" package3d_urn="urn:adsk.eagle:package:5829428/1"/>
<part name="TP2" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="TP" device="B2,54" package3d_urn="urn:adsk.eagle:package:5829428/1"/>
<part name="TP3" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="TP" device="B2,54" package3d_urn="urn:adsk.eagle:package:5829428/1"/>
<part name="R1" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="2k">
<attribute name="DIGIKEY" value="311-2.00KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-072KL"/>
</part>
<part name="R4" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="2k">
<attribute name="DIGIKEY" value="311-2.00KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-072KL"/>
</part>
<part name="R5" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="2k">
<attribute name="DIGIKEY" value="311-2.00KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-072KL"/>
</part>
<part name="D3" library="Power Distribution Board" deviceset="LED_DISPLAY_MODULE_LDT-N2804RI" device="">
<attribute name="DIGIKEY" value="67-2308-ND"/>
<attribute name="MF" value="Lumex Opto/Components Inc."/>
<attribute name="MPN" value="LDT-N2804RI"/>
</part>
<part name="IC5" library="Power Distribution Board" deviceset="LED_DRIVER_AS1108WL-T" device="">
<attribute name="DIGIKEY" value="AS1108WL-TTR-ND"/>
<attribute name="MF" value="ams"/>
<attribute name="MPN" value="AS1108WL-T"/>
</part>
<part name="GND17" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="R6" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="36K5">
<attribute name="DIGIKEY" value="311-36.5KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-0736K5L"/>
</part>
<part name="FRAME2" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_L" device=""/>
<part name="FRAME3" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_L" device=""/>
<part name="C17" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="0.1uF">
<attribute name="DIGIKEY" value="399-1177-1-ND"/>
<attribute name="MF" value="KEMET"/>
<attribute name="MPN" value="C0805C104Z5VACTU"/>
</part>
<part name="GND18" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="GND19" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="FRAME4" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_L" device=""/>
<part name="FRAME5" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_L" device=""/>
<part name="U1" library="Power Distribution Board" deviceset="STM32F103RCT6" device="">
<attribute name="DIGIKEY" value="497-11527-ND"/>
<attribute name="MPN" value="STM32F103RCT7"/>
</part>
<part name="GND20" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="C11" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="4.7uF">
<attribute name="DIGIKEY" value="1276-6465-1-ND"/>
<attribute name="MF" value="Samsung Electro-Mechanics"/>
<attribute name="MPN" value="CL21A475KQFNNNG"/>
</part>
<part name="C10" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="0.1uF">
<attribute name="DIGIKEY" value="399-1177-1-ND"/>
<attribute name="MF" value="KEMET"/>
<attribute name="MPN" value="C0805C104Z5VACTU"/>
</part>
<part name="GND21" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="C9" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="0.1uF">
<attribute name="DIGIKEY" value="399-1177-1-ND"/>
<attribute name="MF" value="KEMET"/>
<attribute name="MPN" value="C0805C104Z5VACTU"/>
</part>
<part name="GND22" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="C8" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="0.1uF">
<attribute name="DIGIKEY" value="399-1177-1-ND"/>
<attribute name="MF" value="KEMET"/>
<attribute name="MPN" value="C0805C104Z5VACTU"/>
</part>
<part name="GND23" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="C12" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="0.1uF">
<attribute name="DIGIKEY" value="399-1177-1-ND"/>
<attribute name="MF" value="KEMET"/>
<attribute name="MPN" value="C0805C104Z5VACTU"/>
</part>
<part name="GND24" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="C7" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="1uF">
<attribute name="DIGIKEY" value="1276-6471-1-ND"/>
<attribute name="MF" value="Samsung Electro-Mechanics"/>
<attribute name="MPN" value="CL21B105KOFNNNG"/>
</part>
<part name="C6" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="10nF">
<attribute name="DIGIKEY" value="399-17617-1-ND"/>
<attribute name="MF" value="KEMET"/>
<attribute name="MPN" value="C0805C103K1RAC7210"/>
</part>
<part name="GND25" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="C5" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="0.1uF">
<attribute name="DIGIKEY" value="399-1177-1-ND"/>
<attribute name="MF" value="KEMET"/>
<attribute name="MPN" value="C0805C104Z5VACTU"/>
</part>
<part name="GND26" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="J2" library="Power Distribution Board" deviceset="JTAG" device="" package3d_urn="urn:adsk.eagle:package:22470/2"/>
<part name="GND27" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="J3" library="Power Distribution Board" deviceset="SD_0472192001" device="">
<attribute name="DIGIKEY" value="WM6698CT-ND"/>
<attribute name="MF" value="Molex"/>
<attribute name="MPN" value="0472192001"/>
</part>
<part name="GND28" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="R12" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="10K">
<attribute name="DIGIKEY" value="311-10.0KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-0710KL"/>
</part>
<part name="R11" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="10K">
<attribute name="DIGIKEY" value="311-10.0KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-0710KL"/>
</part>
<part name="R10" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="10K">
<attribute name="DIGIKEY" value="311-10.0KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-0710KL"/>
</part>
<part name="R9" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="10K">
<attribute name="DIGIKEY" value="311-10.0KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-0710KL"/>
</part>
<part name="R8" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="10K">
<attribute name="DIGIKEY" value="311-10.0KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-0710KL"/>
</part>
<part name="R7" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="10K">
<attribute name="DIGIKEY" value="311-10.0KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-0710KL"/>
</part>
<part name="FRAME6" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_L" device=""/>
<part name="FRAME7" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_L" device=""/>
<part name="FRAME8" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_L" device=""/>
<part name="FRAME9" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_L" device=""/>
<part name="C18" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="0.1uF">
<attribute name="DIGIKEY" value="399-1177-1-ND"/>
<attribute name="MF" value="KEMET"/>
<attribute name="MPN" value="C0805C104Z5VACTU"/>
</part>
<part name="GND29" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="J1" library="Power Distribution Board" deviceset="XT60" device=""/>
<part name="J4" library="Power Distribution Board" deviceset="JST-5" device="">
<attribute name="DIGIKEY" value="455-1737-1-ND"/>
<attribute name="MF" value="JST Sales America Inc."/>
<attribute name="MPN" value="B5B-PH-SM4-TB(LF)(SN)"/>
</part>
<part name="GND30" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="R15" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="28K7">
<attribute name="DIGIKEY" value="311-28.7KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-0728K7L"/>
</part>
<part name="R16" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="100K">
<attribute name="DIGIKEY" value="311-100KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-07100KL"/>
</part>
<part name="GND31" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="R19" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="158K">
<attribute name="DIGIKEY" value="311-158KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-07158KL"/>
</part>
<part name="R20" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="100K">
<attribute name="DIGIKEY" value="311-100KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-07100KL"/>
</part>
<part name="GND32" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="R21" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="287K">
<attribute name="DIGIKEY" value="311-287KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="311-287KCRCT-ND"/>
</part>
<part name="R22" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="100K">
<attribute name="DIGIKEY" value="311-100KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-07100KL"/>
</part>
<part name="GND33" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="R23" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="422K">
<attribute name="DIGIKEY" value="311-422KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-07422KL"/>
</part>
<part name="R24" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="100K">
<attribute name="DIGIKEY" value="311-100KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-07100KL"/>
</part>
<part name="GND34" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="R13" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="28K7">
<attribute name="DIGIKEY" value="311-28.7KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-0728K7L"/>
</part>
<part name="R14" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="100K">
<attribute name="DIGIKEY" value="311-100KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-07100KL"/>
</part>
<part name="GND35" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="R17" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="158K">
<attribute name="DIGIKEY" value="311-158KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-07158KL"/>
</part>
<part name="R18" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="100K">
<attribute name="DIGIKEY" value="311-100KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-07100KL"/>
</part>
<part name="GND36" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="FRAME10" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_L" device=""/>
<part name="FRAME11" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_L" device=""/>
<part name="FRAME12" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_L" device=""/>
<part name="FRAME13" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_L" device=""/>
<part name="J7" library="Power Distribution Board" deviceset="PHX-12X2" device=""/>
<part name="GND37" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="J8" library="Power Distribution Board" deviceset="PHX-10X2" device=""/>
<part name="GND38" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="J6" library="Power Distribution Board" deviceset="PHX-6X2" device=""/>
<part name="GND39" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="FRAME14" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_L" device=""/>
<part name="FRAME15" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_L" device=""/>
<part name="JP1" library="adafruit" library_urn="urn:adsk.eagle:library:420" deviceset="PINHD-1X6" device="" package3d_urn="urn:adsk.eagle:package:6240711/1"/>
<part name="GND40" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="GND41" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="R25" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="10K">
<attribute name="DIGIKEY" value="311-10.0KCRCT-ND"/>
<attribute name="MF" value="Yageo"/>
<attribute name="MPN" value="RC0805FR-0710KL"/>
</part>
<part name="IC6" library="Power Distribution Board" deviceset="COMPARATOR_LM393DT" device="">
<attribute name="DIGIKEY" value="497-1593-1-ND"/>
<attribute name="MF" value="STMicroelectronics"/>
<attribute name="MPN" value="LM393DT"/>
</part>
<part name="GND10" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="R26" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="68">
<attribute name="DIGIKEY" value="RMCF0805JT68R0CT-ND"/>
<attribute name="MF" value="Stackpole Electronics Inc"/>
<attribute name="MPN" value="RMCF0805JT68R0"/>
</part>
<part name="D4" library="SparkFun-LED" library_urn="urn:adsk.eagle:library:529" deviceset="LED" device="1206" package3d_urn="urn:adsk.eagle:package:39352/1">
<attribute name="DIGIKEY" value="732-4991-1-ND"/>
<attribute name="MF" value="Würth Elektronik"/>
<attribute name="MPN" value="150120RS75000"/>
</part>
<part name="GND42" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="FRAME16" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_L" device=""/>
<part name="FRAME17" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA4_L" device=""/>
<part name="D5" library="Power Distribution Board" deviceset="LED-BIDIRECTIONAL-1206" device="" value="20mA 1.95V (red) 2.1V (green)">
<attribute name="DIGIKEY" value="1497-1314-1-ND"/>
<attribute name="MF" value="SunLED"/>
<attribute name="MPN" value="XZMDKVG55W-4"/>
</part>
<part name="R27" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="68">
<attribute name="DIGIKEY" value="RMCF0805JT68R0CT-ND"/>
<attribute name="MF" value="Stackpole Electronics Inc"/>
<attribute name="MPN" value="RMCF0805JT68R0"/>
</part>
<part name="D6" library="Power Distribution Board" deviceset="LED-BIDIRECTIONAL-1206" device="" value="20mA 1.95V (red) 2.1V (green)">
<attribute name="DIGIKEY" value="1497-1314-1-ND"/>
<attribute name="MF" value="SunLED"/>
<attribute name="MPN" value="XZMDKVG55W-4"/>
</part>
<part name="R28" library="Power Distribution Board" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:5829815/2" value="68">
<attribute name="DIGIKEY" value="RMCF0805JT68R0CT-ND"/>
<attribute name="MF" value="Stackpole Electronics Inc"/>
<attribute name="MPN" value="RMCF0805JT68R0"/>
</part>
<part name="IC1" library="Power Distribution Board" deviceset="SWITCHING-REGULATOR-AP6320X" device="">
<attribute name="DIGIKEY" value="AP63203WU-7DICT-ND"/>
<attribute name="MF" value="Diodes Incorporated"/>
<attribute name="MPN" value="AP63203WU-7"/>
</part>
<part name="GND4" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="GND6" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="C2" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="CAPACITOR-N" device="0805" package3d_urn="urn:adsk.eagle:package:5829534/4" value="0.1uF">
<attribute name="DIGIKEY" value="399-1177-1-ND"/>
<attribute name="MF" value="KEMET"/>
<attribute name="MPN" value="C0805C104Z5VACTU"/>
</part>
<part name="L1" library="Power Distribution Board" deviceset="INDUCTOR_3.9UH_1255AY-3R9N=P3" device="" value="3.9µH 4.2A">
<attribute name="DIGIKEY" value="490-14062-1-ND"/>
<attribute name="MF" value="Murata Electronics"/>
<attribute name="MPN" value="1255AY-3R9N=P3"/>
</part>
<part name="C3" library="Power Distribution Board" deviceset="CAPACITOR-φ6.3" device="" value="22µF 50V">
<attribute name="DIGIKEY" value="1189-2057-2-ND"/>
<attribute name="MF" value="Rubycon"/>
<attribute name="MPN" value="50THV22M6.3X8"/>
</part>
<part name="GND43" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="C4" library="Power Distribution Board" deviceset="CAPACITOR-φ6.3" device="" value="22µF 50V">
<attribute name="DIGIKEY" value="1189-2057-2-ND"/>
<attribute name="MF" value="Rubycon"/>
<attribute name="MPN" value="50THV22M6.3X8"/>
</part>
<part name="GND44" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="C1" library="SparkFun-Capacitors" library_urn="urn:adsk.eagle:library:510" deviceset="22UF-POLAR" device="-PANASONIC_C-35V-20%" package3d_urn="urn:adsk.eagle:package:37430/1" value="10µF 50V">
<attribute name="DIGIKEY" value="PCE5021TR-ND"/>
<attribute name="MF" value="Panasonic Electronic Components"/>
<attribute name="MPN" value="EEE-FT1H100AR"/>
</part>
<part name="C19" library="SparkFun-Capacitors" library_urn="urn:adsk.eagle:library:510" deviceset="22UF-POLAR" device="-PANASONIC_C-35V-20%" package3d_urn="urn:adsk.eagle:package:37430/1" value="10µF 50V">
<attribute name="DIGIKEY" value="PCE5021TR-ND"/>
<attribute name="MF" value="Panasonic Electronic Components"/>
<attribute name="MPN" value="EEE-FT1H100AR"/>
</part>
<part name="C16" library="SparkFun-Capacitors" library_urn="urn:adsk.eagle:library:510" deviceset="22UF-POLAR" device="-PANASONIC_C-35V-20%" package3d_urn="urn:adsk.eagle:package:37430/1" value="10µF 50V">
<attribute name="DIGIKEY" value="PCE5021TR-ND"/>
<attribute name="MF" value="Panasonic Electronic Components"/>
<attribute name="MPN" value="EEE-FT1H100AR"/>
</part>
<part name="Q1" library="Power Distribution Board" deviceset="MOSFET_ARRAY_SQJ951EP-T1_GE3" device="">
<attribute name="DATASHEET" value="http://www.vishay.com/docs/63658/sqj951ep.pdf"/>
<attribute name="DIGIKEY" value="SQJ951EP-T1_GE3CT-ND"/>
<attribute name="MF" value="Vishay Siliconix"/>
<attribute name="MPN" value="SQJ951EP-T1_GE3"/>
</part>
<part name="Q3" library="Power Distribution Board" deviceset="MOSFET_ARRAY_SQJ951EP-T1_GE3" device="">
<attribute name="DATASHEET" value="http://www.vishay.com/docs/63658/sqj951ep.pdf"/>
<attribute name="DIGIKEY" value="SQJ951EP-T1_GE3CT-ND"/>
<attribute name="MF" value="Vishay Siliconix"/>
<attribute name="MPN" value="SQJ951EP-T1_GE3"/>
</part>
<part name="Q4" library="Power Distribution Board" deviceset="MOSFET_ARRAY_SQJ951EP-T1_GE3" device="">
<attribute name="DATASHEET" value="http://www.vishay.com/docs/63658/sqj951ep.pdf"/>
<attribute name="DIGIKEY" value="SQJ951EP-T1_GE3CT-ND"/>
<attribute name="MF" value="Vishay Siliconix"/>
<attribute name="MPN" value="SQJ951EP-T1_GE3"/>
</part>
</parts>
<sheets>
<sheet>
<description>Power in</description>
<plain>
</plain>
<instances>
<instance part="GND1" gate="1" x="99.06" y="76.2" smashed="yes">
<attribute name="VALUE" x="99.06" y="75.946" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND2" gate="1" x="149.86" y="73.66" smashed="yes">
<attribute name="VALUE" x="149.86" y="73.406" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="D2" gate="G$1" x="149.86" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="147.828" y="81.28" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="151.892" y="81.28" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
<attribute name="DIGIKEY" x="149.86" y="83.82" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="149.86" y="83.82" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="D1" gate="G$1" x="121.92" y="88.9" smashed="yes">
<attribute name="NAME" x="118.11" y="91.44" size="1.27" layer="95" ratio="10"/>
<attribute name="DIGIKEY" x="121.92" y="88.9" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="121.92" y="88.9" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="121.92" y="88.9" size="1.27" layer="96" display="off"/>
</instance>
<instance part="F1" gate="G$1" x="139.7" y="88.9" smashed="yes">
<attribute name="NAME" x="134.62" y="91.44" size="1.778" layer="95"/>
</instance>
<instance part="F'1" gate="G$1" x="139.7" y="99.06" smashed="yes">
<attribute name="NAME" x="134.62" y="101.6" size="1.778" layer="95"/>
<attribute name="VALUE" x="134.62" y="95.25" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="139.7" y="99.06" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="139.7" y="99.06" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="139.7" y="99.06" size="1.27" layer="96" display="off"/>
</instance>
<instance part="FRAME5" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FRAME5" gate="G$2" x="162.56" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="175.26" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="248.92" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="180.34" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="J1" gate="G$1" x="91.44" y="86.36" smashed="yes" rot="R180"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<wire x1="96.52" y1="83.82" x2="99.06" y2="83.82" width="0.1524" layer="91"/>
<wire x1="99.06" y1="83.82" x2="99.06" y2="78.74" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="J1" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="149.86" y1="76.2" x2="149.86" y2="81.28" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="A"/>
</segment>
</net>
<net name="VBAT_DIRTY" class="0">
<segment>
<wire x1="96.52" y1="88.9" x2="99.06" y2="88.9" width="0.1524" layer="91"/>
<wire x1="99.06" y1="88.9" x2="99.06" y2="91.44" width="0.1524" layer="91"/>
<label x="99.06" y="91.44" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="J1" gate="G$1" pin="VCC"/>
</segment>
<segment>
<wire x1="109.22" y1="91.44" x2="109.22" y2="88.9" width="0.1524" layer="91"/>
<label x="109.22" y="91.44" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="D1" gate="G$1" pin="+"/>
<wire x1="118.11" y1="88.9" x2="109.22" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VBAT" class="0">
<segment>
<wire x1="160.02" y1="91.44" x2="160.02" y2="88.9" width="0.1524" layer="91"/>
<wire x1="160.02" y1="88.9" x2="149.86" y2="88.9" width="0.1524" layer="91"/>
<wire x1="149.86" y1="88.9" x2="149.86" y2="86.36" width="0.1524" layer="91"/>
<wire x1="144.78" y1="88.9" x2="149.86" y2="88.9" width="0.1524" layer="91"/>
<junction x="149.86" y="88.9"/>
<label x="160.02" y="91.44" size="1.778" layer="95" rot="R90" xref="yes"/>
<pinref part="D2" gate="G$1" pin="C"/>
<pinref part="F1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="-"/>
<wire x1="125.73" y1="88.9" x2="132.08" y2="88.9" width="0.1524" layer="91"/>
<pinref part="F1" gate="G$1" pin="1"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Regulators</description>
<plain>
<text x="137.16" y="162.56" size="3.81" layer="97" align="center">3.3V regulator</text>
<text x="137.16" y="114.3" size="3.81" layer="97" align="center">6V regulator</text>
<text x="137.16" y="71.12" size="3.81" layer="97" align="center">12V regulator</text>
</plain>
<instances>
<instance part="A2" gate="G$1" x="137.16" y="55.88" smashed="yes">
<attribute name="NAME" x="132.08" y="63.5" size="1.778" layer="95"/>
<attribute name="VALUE" x="132.08" y="45.72" size="1.778" layer="96"/>
</instance>
<instance part="A1" gate="G$1" x="137.16" y="99.06" smashed="yes">
<attribute name="NAME" x="132.08" y="106.68" size="1.778" layer="95"/>
<attribute name="VALUE" x="132.08" y="88.9" size="1.778" layer="96"/>
</instance>
<instance part="GND3" gate="1" x="119.38" y="88.9" smashed="yes">
<attribute name="VALUE" x="119.38" y="88.646" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND5" gate="1" x="119.38" y="45.72" smashed="yes">
<attribute name="VALUE" x="119.38" y="45.466" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND7" gate="1" x="152.4" y="88.9" smashed="yes">
<attribute name="VALUE" x="152.4" y="88.646" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND8" gate="1" x="152.4" y="45.72" smashed="yes">
<attribute name="VALUE" x="152.4" y="45.466" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="FRAME4" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FRAME4" gate="G$2" x="162.56" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="175.26" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="248.92" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="180.34" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="IC1" gate="G$1" x="114.3" y="142.24" smashed="yes">
<attribute name="NAME" x="114.3" y="149.86" size="1.778" layer="95" align="center"/>
<attribute name="DIGIKEY" x="114.3" y="142.24" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="114.3" y="142.24" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="114.3" y="142.24" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND4" gate="1" x="101.6" y="129.54" smashed="yes">
<attribute name="VALUE" x="101.6" y="129.286" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND6" gate="1" x="93.98" y="129.54" smashed="yes">
<attribute name="VALUE" x="93.98" y="129.286" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="C2" gate="G$1" x="132.08" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="129.159" y="146.304" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="134.239" y="146.304" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="132.08" y="144.78" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="132.08" y="144.78" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="132.08" y="144.78" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="L1" gate="G$1" x="144.78" y="142.24" smashed="yes" rot="R90">
<attribute name="NAME" x="142.24" y="143.51" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="147.32" y="143.51" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
<attribute name="DIGIKEY" x="144.78" y="142.24" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="144.78" y="142.24" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="144.78" y="142.24" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="C3" gate="G$1" x="157.48" y="137.16" smashed="yes">
<attribute name="NAME" x="158.496" y="137.795" size="1.778" layer="95"/>
<attribute name="VALUE" x="158.496" y="132.969" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="157.48" y="137.16" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="157.48" y="137.16" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="157.48" y="137.16" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND43" gate="1" x="157.48" y="129.54" smashed="yes">
<attribute name="VALUE" x="157.48" y="129.286" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="C4" gate="G$1" x="172.72" y="137.16" smashed="yes">
<attribute name="NAME" x="173.736" y="137.795" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.736" y="132.969" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="172.72" y="137.16" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="172.72" y="137.16" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="172.72" y="137.16" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND44" gate="1" x="172.72" y="129.54" smashed="yes">
<attribute name="VALUE" x="172.72" y="129.286" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="C1" gate="G$1" x="93.98" y="139.7" smashed="yes">
<attribute name="NAME" x="94.996" y="140.335" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="92.456" y="135.509" size="1.778" layer="96" font="vector" align="bottom-right"/>
<attribute name="DIGIKEY" x="93.98" y="139.7" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="93.98" y="139.7" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="93.98" y="139.7" size="1.27" layer="96" display="off"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="VBAT" class="0">
<segment>
<pinref part="A1" gate="G$1" pin="VIN1"/>
<wire x1="121.92" y1="104.14" x2="119.38" y2="104.14" width="0.1524" layer="91"/>
<pinref part="A1" gate="G$1" pin="VIN2"/>
<wire x1="119.38" y1="104.14" x2="116.84" y2="104.14" width="0.1524" layer="91"/>
<wire x1="121.92" y1="101.6" x2="119.38" y2="101.6" width="0.1524" layer="91"/>
<wire x1="119.38" y1="101.6" x2="119.38" y2="104.14" width="0.1524" layer="91"/>
<junction x="119.38" y="104.14"/>
<label x="116.84" y="104.14" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="VIN1"/>
<wire x1="121.92" y1="60.96" x2="119.38" y2="60.96" width="0.1524" layer="91"/>
<pinref part="A2" gate="G$1" pin="VIN2"/>
<wire x1="119.38" y1="60.96" x2="116.84" y2="60.96" width="0.1524" layer="91"/>
<wire x1="121.92" y1="58.42" x2="119.38" y2="58.42" width="0.1524" layer="91"/>
<wire x1="119.38" y1="58.42" x2="119.38" y2="60.96" width="0.1524" layer="91"/>
<junction x="119.38" y="60.96"/>
<label x="116.84" y="60.96" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="104.14" y1="144.78" x2="101.6" y2="144.78" width="0.1524" layer="91"/>
<label x="88.9" y="144.78" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="IC1" gate="G$1" pin="VIN"/>
<pinref part="IC1" gate="G$1" pin="EN"/>
<wire x1="101.6" y1="144.78" x2="93.98" y2="144.78" width="0.1524" layer="91"/>
<wire x1="93.98" y1="144.78" x2="88.9" y2="144.78" width="0.1524" layer="91"/>
<wire x1="104.14" y1="142.24" x2="101.6" y2="142.24" width="0.1524" layer="91"/>
<wire x1="101.6" y1="142.24" x2="101.6" y2="144.78" width="0.1524" layer="91"/>
<junction x="101.6" y="144.78"/>
<wire x1="93.98" y1="142.24" x2="93.98" y2="144.78" width="0.1524" layer="91"/>
<junction x="93.98" y="144.78"/>
<pinref part="C1" gate="G$1" pin="+"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="A1" gate="G$1" pin="GIN1"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="121.92" y1="99.06" x2="119.38" y2="99.06" width="0.1524" layer="91"/>
<wire x1="119.38" y1="99.06" x2="119.38" y2="96.52" width="0.1524" layer="91"/>
<pinref part="A1" gate="G$1" pin="GIN2"/>
<wire x1="119.38" y1="96.52" x2="119.38" y2="91.44" width="0.1524" layer="91"/>
<wire x1="121.92" y1="96.52" x2="119.38" y2="96.52" width="0.1524" layer="91"/>
<junction x="119.38" y="96.52"/>
</segment>
<segment>
<pinref part="A2" gate="G$1" pin="GIN1"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="121.92" y1="55.88" x2="119.38" y2="55.88" width="0.1524" layer="91"/>
<wire x1="119.38" y1="55.88" x2="119.38" y2="53.34" width="0.1524" layer="91"/>
<pinref part="A2" gate="G$1" pin="GIN2"/>
<wire x1="119.38" y1="53.34" x2="119.38" y2="48.26" width="0.1524" layer="91"/>
<wire x1="121.92" y1="53.34" x2="119.38" y2="53.34" width="0.1524" layer="91"/>
<junction x="119.38" y="53.34"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="A1" gate="G$1" pin="GOUT1"/>
<wire x1="152.4" y1="91.44" x2="152.4" y2="96.52" width="0.1524" layer="91"/>
<wire x1="152.4" y1="96.52" x2="152.4" y2="99.06" width="0.1524" layer="91"/>
<wire x1="152.4" y1="99.06" x2="149.86" y2="99.06" width="0.1524" layer="91"/>
<pinref part="A1" gate="G$1" pin="GOUT2"/>
<wire x1="149.86" y1="96.52" x2="152.4" y2="96.52" width="0.1524" layer="91"/>
<junction x="152.4" y="96.52"/>
</segment>
<segment>
<pinref part="GND8" gate="1" pin="GND"/>
<pinref part="A2" gate="G$1" pin="GOUT1"/>
<wire x1="152.4" y1="48.26" x2="152.4" y2="53.34" width="0.1524" layer="91"/>
<wire x1="152.4" y1="53.34" x2="152.4" y2="55.88" width="0.1524" layer="91"/>
<wire x1="152.4" y1="55.88" x2="149.86" y2="55.88" width="0.1524" layer="91"/>
<pinref part="A2" gate="G$1" pin="GOUT2"/>
<wire x1="149.86" y1="53.34" x2="152.4" y2="53.34" width="0.1524" layer="91"/>
<junction x="152.4" y="53.34"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GND"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="104.14" y1="139.7" x2="101.6" y2="139.7" width="0.1524" layer="91"/>
<wire x1="101.6" y1="139.7" x2="101.6" y2="132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="93.98" y1="132.08" x2="93.98" y2="134.62" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="-"/>
</segment>
<segment>
<pinref part="GND43" gate="1" pin="GND"/>
<pinref part="C3" gate="G$1" pin="-"/>
</segment>
<segment>
<pinref part="GND44" gate="1" pin="GND"/>
<pinref part="C4" gate="G$1" pin="-"/>
</segment>
</net>
<net name="6V_UNCHECKED" class="0">
<segment>
<pinref part="A1" gate="G$1" pin="VOUT1"/>
<wire x1="149.86" y1="104.14" x2="152.4" y2="104.14" width="0.1524" layer="91"/>
<pinref part="A1" gate="G$1" pin="VOUT2"/>
<wire x1="152.4" y1="104.14" x2="154.94" y2="104.14" width="0.1524" layer="91"/>
<wire x1="149.86" y1="101.6" x2="152.4" y2="101.6" width="0.1524" layer="91"/>
<wire x1="152.4" y1="101.6" x2="152.4" y2="104.14" width="0.1524" layer="91"/>
<junction x="152.4" y="104.14"/>
<label x="154.94" y="104.14" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="12V_UNCHECKED" class="0">
<segment>
<pinref part="A2" gate="G$1" pin="VOUT1"/>
<wire x1="149.86" y1="60.96" x2="152.4" y2="60.96" width="0.1524" layer="91"/>
<pinref part="A2" gate="G$1" pin="VOUT2"/>
<wire x1="152.4" y1="60.96" x2="154.94" y2="60.96" width="0.1524" layer="91"/>
<wire x1="149.86" y1="58.42" x2="152.4" y2="58.42" width="0.1524" layer="91"/>
<wire x1="152.4" y1="58.42" x2="152.4" y2="60.96" width="0.1524" layer="91"/>
<junction x="152.4" y="60.96"/>
<label x="154.94" y="60.96" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="C2" gate="G$1" pin="1"/>
<pinref part="IC1" gate="G$1" pin="BST"/>
<wire x1="127" y1="144.78" x2="124.46" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="SW"/>
<wire x1="124.46" y1="142.24" x2="137.16" y2="142.24" width="0.1524" layer="91"/>
<wire x1="137.16" y1="142.24" x2="137.16" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="137.16" y1="144.78" x2="134.62" y2="144.78" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="139.7" y1="142.24" x2="137.16" y2="142.24" width="0.1524" layer="91"/>
<junction x="137.16" y="142.24"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="C4" gate="G$1" pin="+"/>
<wire x1="172.72" y1="139.7" x2="172.72" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="+"/>
<wire x1="157.48" y1="139.7" x2="157.48" y2="142.24" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="157.48" y1="142.24" x2="152.4" y2="142.24" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="FB"/>
<wire x1="152.4" y1="142.24" x2="149.86" y2="142.24" width="0.1524" layer="91"/>
<wire x1="124.46" y1="139.7" x2="152.4" y2="139.7" width="0.1524" layer="91"/>
<wire x1="152.4" y1="139.7" x2="152.4" y2="142.24" width="0.1524" layer="91"/>
<junction x="152.4" y="142.24"/>
<wire x1="172.72" y1="142.24" x2="157.48" y2="142.24" width="0.1524" layer="91"/>
<junction x="157.48" y="142.24"/>
<wire x1="172.72" y1="142.24" x2="182.88" y2="142.24" width="0.1524" layer="91"/>
<junction x="172.72" y="142.24"/>
<label x="182.88" y="142.24" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>MCU</description>
<plain>
<text x="157.48" y="81.28" size="1.778" layer="97" rot="R90" align="center-left">Connect as close as possible to VDDA</text>
<text x="175.26" y="81.28" size="1.778" layer="97" rot="R90" align="center-left">Connect as close as possible to VDD_1</text>
<text x="187.96" y="81.28" size="1.778" layer="97" rot="R90" align="center-left">Connect as close as possible to VDD_2</text>
<text x="205.74" y="81.28" size="1.778" layer="97" rot="R90" align="center-left">Connect as close as possible to VDD_3</text>
<text x="223.52" y="81.28" size="1.778" layer="97" rot="R90" align="center-left">Connect as close as possible to VDD_4</text>
</plain>
<instances>
<instance part="U1" gate="A" x="68.58" y="99.06" smashed="yes">
<attribute name="NAME" x="63.213140625" y="164.1491" size="2.0857" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="64.437859375" y="25.8484" size="2.08376875" layer="96" ratio="10" rot="SR0"/>
<attribute name="DIGIKEY" x="68.58" y="99.06" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="68.58" y="99.06" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND20" gate="1" x="35.56" y="30.48" smashed="yes">
<attribute name="VALUE" x="35.56" y="30.226" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="C11" gate="G$1" x="210.82" y="58.42" smashed="yes">
<attribute name="NAME" x="212.344" y="61.341" size="1.778" layer="95"/>
<attribute name="VALUE" x="212.344" y="56.261" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="210.82" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="210.82" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="210.82" y="58.42" size="1.27" layer="96" display="off"/>
</instance>
<instance part="C10" gate="G$1" x="200.66" y="58.42" smashed="yes">
<attribute name="NAME" x="202.184" y="61.341" size="1.778" layer="95"/>
<attribute name="VALUE" x="202.184" y="56.261" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="200.66" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="200.66" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="200.66" y="58.42" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND21" gate="1" x="205.74" y="48.26" smashed="yes">
<attribute name="VALUE" x="205.74" y="48.006" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="C9" gate="G$1" x="187.96" y="58.42" smashed="yes">
<attribute name="NAME" x="189.484" y="61.341" size="1.778" layer="95"/>
<attribute name="VALUE" x="189.484" y="56.261" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="187.96" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="187.96" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="187.96" y="58.42" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND22" gate="1" x="187.96" y="48.26" smashed="yes">
<attribute name="VALUE" x="187.96" y="48.006" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="C8" gate="G$1" x="175.26" y="58.42" smashed="yes">
<attribute name="NAME" x="176.784" y="61.341" size="1.778" layer="95"/>
<attribute name="VALUE" x="176.784" y="56.261" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="175.26" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="175.26" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="175.26" y="58.42" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND23" gate="1" x="175.26" y="48.26" smashed="yes">
<attribute name="VALUE" x="175.26" y="48.006" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="C12" gate="G$1" x="223.52" y="58.42" smashed="yes">
<attribute name="NAME" x="225.044" y="61.341" size="1.778" layer="95"/>
<attribute name="VALUE" x="225.044" y="56.261" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="223.52" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="223.52" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="223.52" y="58.42" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND24" gate="1" x="223.52" y="48.26" smashed="yes">
<attribute name="VALUE" x="223.52" y="48.006" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="C7" gate="G$1" x="162.56" y="58.42" smashed="yes">
<attribute name="NAME" x="164.084" y="61.341" size="1.778" layer="95"/>
<attribute name="VALUE" x="164.084" y="56.261" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="162.56" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="162.56" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="162.56" y="58.42" size="1.27" layer="96" display="off"/>
</instance>
<instance part="C6" gate="G$1" x="152.4" y="58.42" smashed="yes">
<attribute name="NAME" x="153.924" y="61.341" size="1.778" layer="95"/>
<attribute name="VALUE" x="153.924" y="56.261" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="152.4" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="152.4" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="152.4" y="58.42" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND25" gate="1" x="157.48" y="48.26" smashed="yes">
<attribute name="VALUE" x="157.48" y="48.006" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="C5" gate="G$1" x="132.08" y="147.32" smashed="yes">
<attribute name="NAME" x="133.604" y="150.241" size="1.778" layer="95"/>
<attribute name="VALUE" x="133.604" y="145.161" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="132.08" y="147.32" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="132.08" y="147.32" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="132.08" y="147.32" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND26" gate="1" x="132.08" y="142.24" smashed="yes">
<attribute name="VALUE" x="132.08" y="141.986" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="J2" gate="G$1" x="200.66" y="147.32" smashed="yes">
<attribute name="NAME" x="200.66" y="157.48" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="200.66" y="137.16" size="1.778" layer="96" align="center"/>
</instance>
<instance part="GND27" gate="1" x="180.34" y="137.16" smashed="yes">
<attribute name="VALUE" x="180.34" y="136.906" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="FRAME6" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FRAME7" gate="G$2" x="162.56" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="175.26" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="248.92" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="180.34" y="19.05" size="2.54" layer="94"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND20" gate="1" pin="GND"/>
<pinref part="U1" gate="A" pin="VSS_1"/>
<wire x1="35.56" y1="33.02" x2="35.56" y2="35.56" width="0.1524" layer="91"/>
<wire x1="35.56" y1="35.56" x2="35.56" y2="38.1" width="0.1524" layer="91"/>
<wire x1="35.56" y1="38.1" x2="35.56" y2="40.64" width="0.1524" layer="91"/>
<wire x1="35.56" y1="40.64" x2="35.56" y2="43.18" width="0.1524" layer="91"/>
<wire x1="35.56" y1="43.18" x2="35.56" y2="45.72" width="0.1524" layer="91"/>
<wire x1="35.56" y1="45.72" x2="38.1" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VSS_4"/>
<wire x1="38.1" y1="43.18" x2="35.56" y2="43.18" width="0.1524" layer="91"/>
<junction x="35.56" y="43.18"/>
<pinref part="U1" gate="A" pin="VSSA"/>
<wire x1="38.1" y1="40.64" x2="35.56" y2="40.64" width="0.1524" layer="91"/>
<junction x="35.56" y="40.64"/>
<pinref part="U1" gate="A" pin="VSS_2"/>
<wire x1="38.1" y1="38.1" x2="35.56" y2="38.1" width="0.1524" layer="91"/>
<junction x="35.56" y="38.1"/>
<pinref part="U1" gate="A" pin="VSS_3"/>
<wire x1="38.1" y1="35.56" x2="35.56" y2="35.56" width="0.1524" layer="91"/>
<junction x="35.56" y="35.56"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="200.66" y1="53.34" x2="200.66" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="210.82" y1="55.88" x2="210.82" y2="53.34" width="0.1524" layer="91"/>
<wire x1="210.82" y1="53.34" x2="205.74" y2="53.34" width="0.1524" layer="91"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="205.74" y1="53.34" x2="200.66" y2="53.34" width="0.1524" layer="91"/>
<wire x1="205.74" y1="50.8" x2="205.74" y2="53.34" width="0.1524" layer="91"/>
<junction x="205.74" y="53.34"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="187.96" y1="50.8" x2="187.96" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="175.26" y1="50.8" x2="175.26" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="223.52" y1="50.8" x2="223.52" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="152.4" y1="53.34" x2="152.4" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="162.56" y1="55.88" x2="162.56" y2="53.34" width="0.1524" layer="91"/>
<wire x1="162.56" y1="53.34" x2="157.48" y2="53.34" width="0.1524" layer="91"/>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="157.48" y1="53.34" x2="152.4" y2="53.34" width="0.1524" layer="91"/>
<wire x1="157.48" y1="50.8" x2="157.48" y2="53.34" width="0.1524" layer="91"/>
<junction x="157.48" y="53.34"/>
</segment>
<segment>
<pinref part="GND26" gate="1" pin="GND"/>
<pinref part="C5" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="GND1"/>
<pinref part="GND27" gate="1" pin="GND"/>
<wire x1="182.88" y1="149.86" x2="180.34" y2="149.86" width="0.1524" layer="91"/>
<wire x1="180.34" y1="149.86" x2="180.34" y2="147.32" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="GND2"/>
<wire x1="180.34" y1="147.32" x2="180.34" y2="142.24" width="0.1524" layer="91"/>
<wire x1="180.34" y1="142.24" x2="180.34" y2="139.7" width="0.1524" layer="91"/>
<wire x1="182.88" y1="147.32" x2="180.34" y2="147.32" width="0.1524" layer="91"/>
<junction x="180.34" y="147.32"/>
<pinref part="J2" gate="G$1" pin="GND_DETECT"/>
<wire x1="182.88" y1="142.24" x2="180.34" y2="142.24" width="0.1524" layer="91"/>
<junction x="180.34" y="142.24"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="U1" gate="A" pin="VDD_1"/>
<wire x1="38.1" y1="144.78" x2="35.56" y2="144.78" width="0.1524" layer="91"/>
<wire x1="35.56" y1="144.78" x2="35.56" y2="147.32" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VDDA"/>
<wire x1="35.56" y1="147.32" x2="35.56" y2="149.86" width="0.1524" layer="91"/>
<wire x1="35.56" y1="149.86" x2="35.56" y2="152.4" width="0.1524" layer="91"/>
<wire x1="35.56" y1="152.4" x2="35.56" y2="154.94" width="0.1524" layer="91"/>
<wire x1="35.56" y1="154.94" x2="35.56" y2="160.02" width="0.1524" layer="91"/>
<wire x1="38.1" y1="154.94" x2="35.56" y2="154.94" width="0.1524" layer="91"/>
<junction x="35.56" y="154.94"/>
<pinref part="U1" gate="A" pin="VDD_2"/>
<wire x1="38.1" y1="152.4" x2="35.56" y2="152.4" width="0.1524" layer="91"/>
<junction x="35.56" y="152.4"/>
<pinref part="U1" gate="A" pin="VDD_3"/>
<wire x1="38.1" y1="149.86" x2="35.56" y2="149.86" width="0.1524" layer="91"/>
<junction x="35.56" y="149.86"/>
<pinref part="U1" gate="A" pin="VDD_4"/>
<wire x1="38.1" y1="147.32" x2="35.56" y2="147.32" width="0.1524" layer="91"/>
<junction x="35.56" y="147.32"/>
<label x="35.56" y="160.02" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="U1" gate="A" pin="VBAT"/>
<wire x1="38.1" y1="139.7" x2="35.56" y2="139.7" width="0.1524" layer="91"/>
<wire x1="35.56" y1="139.7" x2="35.56" y2="144.78" width="0.1524" layer="91"/>
<junction x="35.56" y="144.78"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="200.66" y1="63.5" x2="200.66" y2="66.04" width="0.1524" layer="91"/>
<wire x1="200.66" y1="66.04" x2="205.74" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="205.74" y1="66.04" x2="210.82" y2="66.04" width="0.1524" layer="91"/>
<wire x1="210.82" y1="66.04" x2="210.82" y2="63.5" width="0.1524" layer="91"/>
<wire x1="205.74" y1="66.04" x2="205.74" y2="68.58" width="0.1524" layer="91"/>
<junction x="205.74" y="66.04"/>
<label x="205.74" y="68.58" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="187.96" y1="63.5" x2="187.96" y2="68.58" width="0.1524" layer="91"/>
<label x="187.96" y="68.58" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="175.26" y1="63.5" x2="175.26" y2="68.58" width="0.1524" layer="91"/>
<label x="175.26" y="68.58" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="223.52" y1="63.5" x2="223.52" y2="68.58" width="0.1524" layer="91"/>
<label x="223.52" y="68.58" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="152.4" y1="63.5" x2="152.4" y2="66.04" width="0.1524" layer="91"/>
<wire x1="152.4" y1="66.04" x2="157.48" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="157.48" y1="66.04" x2="162.56" y2="66.04" width="0.1524" layer="91"/>
<wire x1="162.56" y1="66.04" x2="162.56" y2="63.5" width="0.1524" layer="91"/>
<wire x1="157.48" y1="66.04" x2="157.48" y2="68.58" width="0.1524" layer="91"/>
<junction x="157.48" y="66.04"/>
<label x="157.48" y="68.58" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="VCC"/>
<wire x1="182.88" y1="152.4" x2="180.34" y2="152.4" width="0.1524" layer="91"/>
<wire x1="180.34" y1="152.4" x2="180.34" y2="154.94" width="0.1524" layer="91"/>
<label x="180.34" y="154.94" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="SWDIO" class="0">
<segment>
<pinref part="U1" gate="A" pin="PA13"/>
<wire x1="38.1" y1="99.06" x2="35.56" y2="99.06" width="0.1524" layer="91"/>
<label x="35.56" y="99.06" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="SWDIO/TMS"/>
<wire x1="218.44" y1="152.4" x2="220.98" y2="152.4" width="0.1524" layer="91"/>
<label x="220.98" y="152.4" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SWCLK" class="0">
<segment>
<pinref part="U1" gate="A" pin="PA14"/>
<wire x1="38.1" y1="96.52" x2="35.56" y2="96.52" width="0.1524" layer="91"/>
<label x="35.56" y="96.52" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="SWCLK/TCK"/>
<wire x1="218.44" y1="149.86" x2="220.98" y2="149.86" width="0.1524" layer="91"/>
<label x="220.98" y="149.86" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="MCU_NRST" class="0">
<segment>
<pinref part="U1" gate="A" pin="NRST"/>
<wire x1="99.06" y1="154.94" x2="132.08" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="132.08" y1="154.94" x2="137.16" y2="154.94" width="0.1524" layer="91"/>
<wire x1="132.08" y1="152.4" x2="132.08" y2="154.94" width="0.1524" layer="91"/>
<junction x="132.08" y="154.94"/>
<label x="137.16" y="154.94" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="NRESET"/>
<wire x1="218.44" y1="142.24" x2="220.98" y2="142.24" width="0.1524" layer="91"/>
<label x="220.98" y="142.24" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="!LED_CS" class="0">
<segment>
<pinref part="U1" gate="A" pin="PB6"/>
<wire x1="38.1" y1="73.66" x2="35.56" y2="73.66" width="0.1524" layer="91"/>
<label x="35.56" y="73.66" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SPI1_MOSI" class="0">
<segment>
<pinref part="U1" gate="A" pin="PB5"/>
<wire x1="38.1" y1="76.2" x2="35.56" y2="76.2" width="0.1524" layer="91"/>
<label x="35.56" y="76.2" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SPI1_SCK" class="0">
<segment>
<pinref part="U1" gate="A" pin="PB3"/>
<wire x1="38.1" y1="81.28" x2="35.56" y2="81.28" width="0.1524" layer="91"/>
<label x="35.56" y="81.28" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SDIO_CMD" class="0">
<segment>
<pinref part="U1" gate="A" pin="PD2"/>
<wire x1="99.06" y1="144.78" x2="101.6" y2="144.78" width="0.1524" layer="91"/>
<label x="101.6" y="144.78" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SDIO_CLK" class="0">
<segment>
<pinref part="U1" gate="A" pin="PC12"/>
<wire x1="99.06" y1="109.22" x2="101.6" y2="109.22" width="0.1524" layer="91"/>
<label x="101.6" y="109.22" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SDIO_D3" class="0">
<segment>
<pinref part="U1" gate="A" pin="PC11"/>
<wire x1="99.06" y1="111.76" x2="101.6" y2="111.76" width="0.1524" layer="91"/>
<label x="101.6" y="111.76" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SDIO_D2" class="0">
<segment>
<pinref part="U1" gate="A" pin="PC10"/>
<wire x1="99.06" y1="114.3" x2="101.6" y2="114.3" width="0.1524" layer="91"/>
<label x="101.6" y="114.3" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SDIO_D1" class="0">
<segment>
<pinref part="U1" gate="A" pin="PC9"/>
<wire x1="99.06" y1="116.84" x2="101.6" y2="116.84" width="0.1524" layer="91"/>
<label x="101.6" y="116.84" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SDIO_D0" class="0">
<segment>
<pinref part="U1" gate="A" pin="PC8"/>
<wire x1="99.06" y1="119.38" x2="101.6" y2="119.38" width="0.1524" layer="91"/>
<label x="101.6" y="119.38" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="UART_RX" class="0">
<segment>
<pinref part="U1" gate="A" pin="PA9"/>
<wire x1="38.1" y1="109.22" x2="35.56" y2="109.22" width="0.1524" layer="91"/>
<label x="35.56" y="109.22" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="UART_TX" class="0">
<segment>
<pinref part="U1" gate="A" pin="PA10"/>
<wire x1="38.1" y1="106.68" x2="35.56" y2="106.68" width="0.1524" layer="91"/>
<label x="35.56" y="106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="POWER_OK" class="0">
<segment>
<pinref part="U1" gate="A" pin="PC0"/>
<wire x1="99.06" y1="139.7" x2="101.6" y2="139.7" width="0.1524" layer="91"/>
<label x="101.6" y="139.7" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="BATTERY_STATUS_N" class="0">
<segment>
<pinref part="U1" gate="A" pin="PC1"/>
<wire x1="99.06" y1="137.16" x2="101.6" y2="137.16" width="0.1524" layer="91"/>
<label x="101.6" y="137.16" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="BATTERY_STATUS_P" class="0">
<segment>
<pinref part="U1" gate="A" pin="PC2"/>
<wire x1="99.06" y1="134.62" x2="101.6" y2="134.62" width="0.1524" layer="91"/>
<label x="101.6" y="134.62" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="REGULATOR_STATUS_N" class="0">
<segment>
<pinref part="U1" gate="A" pin="PC3"/>
<wire x1="99.06" y1="132.08" x2="101.6" y2="132.08" width="0.1524" layer="91"/>
<label x="101.6" y="132.08" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="REGULATOR_STATUS_P" class="0">
<segment>
<pinref part="U1" gate="A" pin="PC4"/>
<wire x1="99.06" y1="129.54" x2="101.6" y2="129.54" width="0.1524" layer="91"/>
<label x="101.6" y="129.54" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="3S_SENSE_NORMALIZED" class="0">
<segment>
<wire x1="38.1" y1="129.54" x2="35.56" y2="129.54" width="0.1524" layer="91"/>
<label x="35.56" y="129.54" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U1" gate="A" pin="PA1"/>
</segment>
</net>
<net name="2S_SENSE_NORMALIZED" class="0">
<segment>
<wire x1="38.1" y1="127" x2="35.56" y2="127" width="0.1524" layer="91"/>
<label x="35.56" y="127" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U1" gate="A" pin="PA2"/>
</segment>
</net>
<net name="12V_ISENSE" class="0">
<segment>
<wire x1="38.1" y1="114.3" x2="35.56" y2="114.3" width="0.1524" layer="91"/>
<label x="35.56" y="114.3" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U1" gate="A" pin="PA7"/>
</segment>
</net>
<net name="6V_ISENSE" class="0">
<segment>
<wire x1="38.1" y1="88.9" x2="35.56" y2="88.9" width="0.1524" layer="91"/>
<label x="35.56" y="88.9" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U1" gate="A" pin="PB0"/>
</segment>
</net>
<net name="1S_SENSE_NORMALIZED" class="0">
<segment>
<wire x1="38.1" y1="116.84" x2="35.56" y2="116.84" width="0.1524" layer="91"/>
<label x="35.56" y="116.84" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U1" gate="A" pin="PA6"/>
</segment>
</net>
<net name="4S_SENSE_NORMALIZED" class="0">
<segment>
<wire x1="38.1" y1="119.38" x2="35.56" y2="119.38" width="0.1524" layer="91"/>
<label x="35.56" y="119.38" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U1" gate="A" pin="PA5"/>
</segment>
</net>
<net name="6V_VSENSE" class="0">
<segment>
<wire x1="38.1" y1="124.46" x2="35.56" y2="124.46" width="0.1524" layer="91"/>
<label x="35.56" y="124.46" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U1" gate="A" pin="PA3"/>
</segment>
</net>
<net name="12V_VSENSE" class="0">
<segment>
<wire x1="38.1" y1="121.92" x2="35.56" y2="121.92" width="0.1524" layer="91"/>
<label x="35.56" y="121.92" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U1" gate="A" pin="PA4"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Switches</description>
<plain>
</plain>
<instances>
<instance part="Q2" gate="G$1" x="132.08" y="124.46" smashed="yes">
<attribute name="NAME" x="134.62" y="124.46" size="1.778" layer="95" font="vector"/>
<attribute name="DIGIKEY" x="132.08" y="124.46" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="132.08" y="124.46" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="132.08" y="124.46" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND9" gate="1" x="134.62" y="114.3" smashed="yes">
<attribute name="VALUE" x="134.62" y="114.046" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R2" gate="G$1" x="121.92" y="124.46" smashed="yes">
<attribute name="NAME" x="118.11" y="125.9586" size="1.778" layer="95"/>
<attribute name="VALUE" x="118.11" y="121.158" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="121.92" y="124.46" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="121.92" y="124.46" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="121.92" y="124.46" size="1.27" layer="96" display="off"/>
</instance>
<instance part="R3" gate="G$1" x="134.62" y="139.7" smashed="yes" rot="R90">
<attribute name="NAME" x="133.1214" y="135.89" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="137.922" y="135.89" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="134.62" y="139.7" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="134.62" y="139.7" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="134.62" y="139.7" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FRAME1" gate="G$2" x="162.56" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="175.26" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="248.92" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="180.34" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="IC2" gate="G$1" x="53.34" y="83.82" smashed="yes">
<attribute name="NAME" x="52.07" y="91.44" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="44.45" y="76.2" size="1.778" layer="96" align="center-left"/>
<attribute name="DIGIKEY" x="53.34" y="83.82" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="53.34" y="83.82" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="53.34" y="83.82" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND11" gate="1" x="38.1" y="68.58" smashed="yes">
<attribute name="VALUE" x="38.1" y="68.326" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="C13" gate="G$1" x="27.94" y="76.2" smashed="yes">
<attribute name="NAME" x="29.464" y="79.121" size="1.778" layer="95"/>
<attribute name="VALUE" x="29.464" y="74.041" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="27.94" y="76.2" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="27.94" y="76.2" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="27.94" y="76.2" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND12" gate="1" x="27.94" y="68.58" smashed="yes">
<attribute name="VALUE" x="27.94" y="68.326" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="IC3" gate="G$1" x="134.62" y="83.82" smashed="yes">
<attribute name="NAME" x="133.35" y="91.44" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="125.73" y="76.2" size="1.778" layer="96" align="center-left"/>
<attribute name="DIGIKEY" x="134.62" y="83.82" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="134.62" y="83.82" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="134.62" y="83.82" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND13" gate="1" x="119.38" y="68.58" smashed="yes">
<attribute name="VALUE" x="119.38" y="68.326" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="C14" gate="G$1" x="109.22" y="76.2" smashed="yes">
<attribute name="NAME" x="110.744" y="79.121" size="1.778" layer="95"/>
<attribute name="VALUE" x="110.744" y="74.041" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="109.22" y="76.2" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="109.22" y="76.2" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="109.22" y="76.2" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND14" gate="1" x="109.22" y="68.58" smashed="yes">
<attribute name="VALUE" x="109.22" y="68.326" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="IC4" gate="G$1" x="215.9" y="83.82" smashed="yes">
<attribute name="NAME" x="214.63" y="91.44" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="207.01" y="76.2" size="1.778" layer="96" align="center-left"/>
<attribute name="DIGIKEY" x="215.9" y="83.82" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="215.9" y="83.82" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="215.9" y="83.82" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND15" gate="1" x="200.66" y="68.58" smashed="yes">
<attribute name="VALUE" x="200.66" y="68.326" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="C15" gate="G$1" x="190.5" y="76.2" smashed="yes">
<attribute name="NAME" x="192.024" y="79.121" size="1.778" layer="95"/>
<attribute name="VALUE" x="192.024" y="74.041" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="190.5" y="76.2" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="190.5" y="76.2" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="190.5" y="76.2" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND16" gate="1" x="190.5" y="68.58" smashed="yes">
<attribute name="VALUE" x="190.5" y="68.326" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="TP1" gate="G$1" x="78.74" y="86.36" smashed="yes" rot="R270">
<attribute name="NAME" x="80.01" y="85.09" size="1.778" layer="95"/>
</instance>
<instance part="TP2" gate="G$1" x="160.02" y="86.36" smashed="yes" rot="R270">
<attribute name="NAME" x="161.29" y="85.09" size="1.778" layer="95"/>
</instance>
<instance part="TP3" gate="G$1" x="241.3" y="86.36" smashed="yes" rot="R270">
<attribute name="NAME" x="242.57" y="85.09" size="1.778" layer="95"/>
</instance>
<instance part="R1" gate="G$1" x="73.66" y="93.98" smashed="yes" rot="R90">
<attribute name="NAME" x="72.1614" y="90.17" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="76.962" y="90.17" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="73.66" y="93.98" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="73.66" y="93.98" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="73.66" y="93.98" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="R4" gate="G$1" x="154.94" y="93.98" smashed="yes" rot="R90">
<attribute name="NAME" x="153.4414" y="90.17" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="158.242" y="90.17" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="154.94" y="93.98" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="154.94" y="93.98" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="154.94" y="93.98" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="R5" gate="G$1" x="236.22" y="93.98" smashed="yes" rot="R90">
<attribute name="NAME" x="234.7214" y="90.17" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="239.522" y="90.17" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="236.22" y="93.98" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="236.22" y="93.98" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="236.22" y="93.98" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="Q2" gate="G$2" x="165.1" y="124.46" smashed="yes">
<attribute name="NAME" x="167.64" y="124.46" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="Q1" gate="G$2" x="71.12" y="68.58" smashed="yes" rot="MR180">
<attribute name="NAME" x="76.2" y="68.58" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="DATASHEET" x="71.12" y="68.58" size="1.27" layer="96" rot="MR180" display="off"/>
</instance>
<instance part="Q3" gate="G$2" x="152.4" y="66.04" smashed="yes" rot="MR180">
<attribute name="NAME" x="157.48" y="66.04" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="DATASHEET" x="152.4" y="66.04" size="1.27" layer="96" rot="MR180" display="off"/>
</instance>
<instance part="Q4" gate="G$2" x="233.68" y="66.04" smashed="yes" rot="MR180">
<attribute name="NAME" x="238.76" y="66.04" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="DATASHEET" x="233.68" y="66.04" size="1.27" layer="96" rot="MR180" display="off"/>
</instance>
<instance part="Q1" gate="G$1" x="71.12" y="53.34" smashed="yes" rot="MR180">
<attribute name="NAME" x="76.2" y="53.34" size="1.778" layer="95" font="vector" rot="MR180"/>
</instance>
<instance part="Q3" gate="G$1" x="152.4" y="50.8" smashed="yes" rot="MR180">
<attribute name="NAME" x="157.48" y="50.8" size="1.778" layer="95" font="vector" rot="MR180"/>
</instance>
<instance part="Q4" gate="G$1" x="233.68" y="50.8" smashed="yes" rot="MR180">
<attribute name="NAME" x="238.76" y="50.8" size="1.778" layer="95" font="vector" rot="MR180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="Q2" gate="G$1" pin="E"/>
<wire x1="134.62" y1="116.84" x2="134.62" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="GND"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="40.64" y1="81.28" x2="38.1" y2="81.28" width="0.1524" layer="91"/>
<wire x1="38.1" y1="81.28" x2="38.1" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND12" gate="1" pin="GND"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="27.94" y1="71.12" x2="27.94" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="GND"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="121.92" y1="81.28" x2="119.38" y2="81.28" width="0.1524" layer="91"/>
<wire x1="119.38" y1="81.28" x2="119.38" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND14" gate="1" pin="GND"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="109.22" y1="71.12" x2="109.22" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC4" gate="G$1" pin="GND"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="203.2" y1="81.28" x2="200.66" y2="81.28" width="0.1524" layer="91"/>
<wire x1="200.66" y1="81.28" x2="200.66" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="190.5" y1="71.12" x2="190.5" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="Q2" gate="G$1" pin="B"/>
<wire x1="127" y1="124.46" x2="129.54" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="POWER_OK" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="116.84" y1="124.46" x2="114.3" y2="124.46" width="0.1524" layer="91"/>
<label x="114.3" y="124.46" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="POWER_OK_DRIVEN" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="C"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="134.62" y1="134.62" x2="134.62" y2="132.08" width="0.1524" layer="91"/>
<wire x1="134.62" y1="132.08" x2="134.62" y2="129.54" width="0.1524" layer="91"/>
<wire x1="134.62" y1="132.08" x2="137.16" y2="132.08" width="0.1524" layer="91"/>
<junction x="134.62" y="132.08"/>
<label x="137.16" y="132.08" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="66.04" y1="71.12" x2="63.5" y2="71.12" width="0.1524" layer="91"/>
<label x="63.5" y="71.12" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="Q1" gate="G$2" pin="G"/>
</segment>
<segment>
<wire x1="147.32" y1="68.58" x2="144.78" y2="68.58" width="0.1524" layer="91"/>
<label x="144.78" y="68.58" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="Q3" gate="G$2" pin="G"/>
</segment>
<segment>
<wire x1="228.6" y1="68.58" x2="226.06" y2="68.58" width="0.1524" layer="91"/>
<label x="226.06" y="68.58" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="Q4" gate="G$2" pin="G"/>
</segment>
</net>
<net name="12V_UNCHECKED" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="IP+"/>
<wire x1="121.92" y1="86.36" x2="119.38" y2="86.36" width="0.1524" layer="91"/>
<label x="119.38" y="86.36" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="VBAT_CHECKED" class="0">
<segment>
<wire x1="73.66" y1="60.96" x2="76.2" y2="60.96" width="0.1524" layer="91"/>
<label x="76.2" y="60.96" size="1.27" layer="95" xref="yes"/>
<wire x1="73.66" y1="63.5" x2="73.66" y2="60.96" width="0.1524" layer="91"/>
<wire x1="73.66" y1="60.96" x2="73.66" y2="58.42" width="0.1524" layer="91"/>
<junction x="73.66" y="60.96"/>
<pinref part="Q1" gate="G$2" pin="D"/>
<pinref part="Q1" gate="G$1" pin="S"/>
</segment>
</net>
<net name="VBAT" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="IP+"/>
<wire x1="40.64" y1="86.36" x2="38.1" y2="86.36" width="0.1524" layer="91"/>
<label x="38.1" y="86.36" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="134.62" y1="144.78" x2="134.62" y2="149.86" width="0.1524" layer="91"/>
<label x="134.62" y="149.86" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="VBAT_SAFE" class="0">
<segment>
<wire x1="73.66" y1="43.18" x2="76.2" y2="43.18" width="0.1524" layer="91"/>
<label x="76.2" y="43.18" size="1.27" layer="95" xref="yes"/>
<wire x1="73.66" y1="48.26" x2="73.66" y2="43.18" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="D"/>
</segment>
</net>
<net name="SAFETY_OK_DRIVEN" class="0">
<segment>
<wire x1="66.04" y1="55.88" x2="63.5" y2="55.88" width="0.1524" layer="91"/>
<label x="63.5" y="55.88" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="Q1" gate="G$1" pin="G"/>
</segment>
<segment>
<wire x1="147.32" y1="53.34" x2="144.78" y2="53.34" width="0.1524" layer="91"/>
<label x="144.78" y="53.34" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="Q3" gate="G$1" pin="G"/>
</segment>
<segment>
<wire x1="228.6" y1="53.34" x2="226.06" y2="53.34" width="0.1524" layer="91"/>
<label x="226.06" y="53.34" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="Q4" gate="G$1" pin="G"/>
</segment>
</net>
<net name="12V_CHECKED" class="0">
<segment>
<wire x1="154.94" y1="58.42" x2="157.48" y2="58.42" width="0.1524" layer="91"/>
<label x="157.48" y="58.42" size="1.27" layer="95" xref="yes"/>
<wire x1="154.94" y1="55.88" x2="154.94" y2="58.42" width="0.1524" layer="91"/>
<wire x1="154.94" y1="58.42" x2="154.94" y2="60.96" width="0.1524" layer="91"/>
<junction x="154.94" y="58.42"/>
<pinref part="Q3" gate="G$2" pin="D"/>
<pinref part="Q3" gate="G$1" pin="S"/>
</segment>
</net>
<net name="6V_CHECKED" class="0">
<segment>
<wire x1="236.22" y1="58.42" x2="238.76" y2="58.42" width="0.1524" layer="91"/>
<label x="238.76" y="58.42" size="1.27" layer="95" xref="yes"/>
<wire x1="236.22" y1="55.88" x2="236.22" y2="58.42" width="0.1524" layer="91"/>
<wire x1="236.22" y1="58.42" x2="236.22" y2="60.96" width="0.1524" layer="91"/>
<junction x="236.22" y="58.42"/>
<pinref part="Q4" gate="G$2" pin="D"/>
<pinref part="Q4" gate="G$1" pin="S"/>
</segment>
</net>
<net name="12V_SAFE" class="0">
<segment>
<wire x1="154.94" y1="43.18" x2="157.48" y2="43.18" width="0.1524" layer="91"/>
<label x="157.48" y="43.18" size="1.27" layer="95" xref="yes"/>
<wire x1="154.94" y1="45.72" x2="154.94" y2="43.18" width="0.1524" layer="91"/>
<pinref part="Q3" gate="G$1" pin="D"/>
</segment>
</net>
<net name="6V_UNCHECKED" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="IP+"/>
<wire x1="203.2" y1="86.36" x2="200.66" y2="86.36" width="0.1524" layer="91"/>
<label x="200.66" y="86.36" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="6V_SAFE" class="0">
<segment>
<wire x1="236.22" y1="43.18" x2="238.76" y2="43.18" width="0.1524" layer="91"/>
<label x="238.76" y="43.18" size="1.27" layer="95" xref="yes"/>
<wire x1="236.22" y1="45.72" x2="236.22" y2="43.18" width="0.1524" layer="91"/>
<pinref part="Q4" gate="G$1" pin="D"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="IP-"/>
<wire x1="66.04" y1="81.28" x2="73.66" y2="81.28" width="0.1524" layer="91"/>
<wire x1="73.66" y1="73.66" x2="73.66" y2="81.28" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$2" pin="S"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="C13" gate="G$1" pin="1"/>
<pinref part="IC2" gate="G$1" pin="VCC"/>
<wire x1="27.94" y1="81.28" x2="27.94" y2="83.82" width="0.1524" layer="91"/>
<wire x1="27.94" y1="83.82" x2="40.64" y2="83.82" width="0.1524" layer="91"/>
<wire x1="27.94" y1="83.82" x2="25.4" y2="83.82" width="0.1524" layer="91"/>
<junction x="27.94" y="83.82"/>
<label x="25.4" y="83.82" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="1"/>
<pinref part="IC3" gate="G$1" pin="VCC"/>
<wire x1="109.22" y1="81.28" x2="109.22" y2="83.82" width="0.1524" layer="91"/>
<wire x1="109.22" y1="83.82" x2="121.92" y2="83.82" width="0.1524" layer="91"/>
<wire x1="109.22" y1="83.82" x2="106.68" y2="83.82" width="0.1524" layer="91"/>
<junction x="109.22" y="83.82"/>
<label x="106.68" y="83.82" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="1"/>
<pinref part="IC4" gate="G$1" pin="VCC"/>
<wire x1="190.5" y1="81.28" x2="190.5" y2="83.82" width="0.1524" layer="91"/>
<wire x1="190.5" y1="83.82" x2="203.2" y2="83.82" width="0.1524" layer="91"/>
<wire x1="190.5" y1="83.82" x2="187.96" y2="83.82" width="0.1524" layer="91"/>
<junction x="190.5" y="83.82"/>
<label x="187.96" y="83.82" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="73.66" y1="99.06" x2="73.66" y2="101.6" width="0.1524" layer="91"/>
<label x="73.66" y="101.6" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="154.94" y1="99.06" x2="154.94" y2="101.6" width="0.1524" layer="91"/>
<label x="154.94" y="101.6" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="236.22" y1="99.06" x2="236.22" y2="101.6" width="0.1524" layer="91"/>
<label x="236.22" y="101.6" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="IP-"/>
<wire x1="147.32" y1="81.28" x2="154.94" y2="81.28" width="0.1524" layer="91"/>
<wire x1="154.94" y1="71.12" x2="154.94" y2="81.28" width="0.1524" layer="91"/>
<pinref part="Q3" gate="G$2" pin="S"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="IP-"/>
<wire x1="228.6" y1="81.28" x2="236.22" y2="81.28" width="0.1524" layer="91"/>
<wire x1="236.22" y1="71.12" x2="236.22" y2="81.28" width="0.1524" layer="91"/>
<pinref part="Q4" gate="G$2" pin="S"/>
</segment>
</net>
<net name="VBAT_ISENSE" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="VIOUT"/>
<wire x1="66.04" y1="83.82" x2="76.2" y2="83.82" width="0.1524" layer="91"/>
<label x="76.2" y="83.82" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="12V_ISENSE" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="VIOUT"/>
<wire x1="147.32" y1="83.82" x2="157.48" y2="83.82" width="0.1524" layer="91"/>
<label x="157.48" y="83.82" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="6V_ISENSE" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="VIOUT"/>
<wire x1="228.6" y1="83.82" x2="238.76" y2="83.82" width="0.1524" layer="91"/>
<label x="238.76" y="83.82" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="TP1" gate="G$1" pin="TP"/>
<pinref part="IC2" gate="G$1" pin="!FAULT"/>
<wire x1="76.2" y1="86.36" x2="73.66" y2="86.36" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="73.66" y1="86.36" x2="66.04" y2="86.36" width="0.1524" layer="91"/>
<wire x1="73.66" y1="88.9" x2="73.66" y2="86.36" width="0.1524" layer="91"/>
<junction x="73.66" y="86.36"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="TP2" gate="G$1" pin="TP"/>
<wire x1="157.48" y1="86.36" x2="154.94" y2="86.36" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="!FAULT"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="154.94" y1="86.36" x2="147.32" y2="86.36" width="0.1524" layer="91"/>
<wire x1="154.94" y1="88.9" x2="154.94" y2="86.36" width="0.1524" layer="91"/>
<junction x="154.94" y="86.36"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="TP3" gate="G$1" pin="TP"/>
<wire x1="238.76" y1="86.36" x2="236.22" y2="86.36" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="!FAULT"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="236.22" y1="86.36" x2="228.6" y2="86.36" width="0.1524" layer="91"/>
<wire x1="236.22" y1="88.9" x2="236.22" y2="86.36" width="0.1524" layer="91"/>
<junction x="236.22" y="86.36"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Voltage Display</description>
<plain>
</plain>
<instances>
<instance part="D3" gate="G$1" x="165.1" y="101.6" smashed="yes">
<attribute name="NAME" x="165.1" y="114.3" size="1.27" layer="95" align="center"/>
<attribute name="DIGIKEY" x="165.1" y="101.6" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="165.1" y="101.6" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="165.1" y="101.6" size="1.27" layer="96" display="off"/>
</instance>
<instance part="IC5" gate="G$1" x="116.84" y="93.98" smashed="yes">
<attribute name="NAME" x="116.84" y="114.3" size="1.27" layer="95" align="center"/>
<attribute name="VALUE" x="116.84" y="73.66" size="1.27" layer="95" align="center"/>
<attribute name="DIGIKEY" x="116.84" y="93.98" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="116.84" y="93.98" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="116.84" y="93.98" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND17" gate="1" x="99.06" y="73.66" smashed="yes">
<attribute name="VALUE" x="99.06" y="73.406" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R6" gate="G$1" x="93.98" y="106.68" smashed="yes">
<attribute name="NAME" x="87.63" y="103.0986" size="1.778" layer="95"/>
<attribute name="VALUE" x="92.71" y="103.378" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="93.98" y="106.68" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="93.98" y="106.68" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="93.98" y="106.68" size="1.27" layer="96" display="off"/>
</instance>
<instance part="FRAME2" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FRAME3" gate="G$2" x="162.56" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="175.26" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="248.92" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="180.34" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="C17" gate="G$1" x="71.12" y="99.06" smashed="yes">
<attribute name="NAME" x="72.644" y="101.981" size="1.778" layer="95"/>
<attribute name="VALUE" x="72.644" y="96.901" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="71.12" y="99.06" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="71.12" y="99.06" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="71.12" y="99.06" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND18" gate="1" x="71.12" y="91.44" smashed="yes">
<attribute name="VALUE" x="71.12" y="91.186" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND19" gate="1" x="63.5" y="91.44" smashed="yes">
<attribute name="VALUE" x="63.5" y="91.186" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="C16" gate="G$1" x="63.5" y="101.6" smashed="yes" rot="MR0">
<attribute name="NAME" x="62.484" y="102.235" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="62.484" y="97.409" size="1.778" layer="96" font="vector" rot="MR0"/>
<attribute name="DIGIKEY" x="63.5" y="101.6" size="1.27" layer="96" rot="MR0" display="off"/>
<attribute name="MF" x="63.5" y="101.6" size="1.27" layer="96" rot="MR0" display="off"/>
<attribute name="MPN" x="63.5" y="101.6" size="1.27" layer="96" rot="MR0" display="off"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$11" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="SEG_A"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="132.08" y1="109.22" x2="154.94" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="B"/>
<pinref part="IC5" gate="G$1" pin="SEG_B"/>
<wire x1="154.94" y1="106.68" x2="132.08" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="SEG_C"/>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="132.08" y1="104.14" x2="154.94" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="D"/>
<pinref part="IC5" gate="G$1" pin="SEG_D"/>
<wire x1="154.94" y1="101.6" x2="132.08" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="SEG_E"/>
<pinref part="D3" gate="G$1" pin="E"/>
<wire x1="132.08" y1="99.06" x2="154.94" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="F"/>
<pinref part="IC5" gate="G$1" pin="SEG_F"/>
<wire x1="154.94" y1="96.52" x2="132.08" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="SEG_G"/>
<pinref part="D3" gate="G$1" pin="G"/>
<wire x1="132.08" y1="93.98" x2="154.94" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="DP"/>
<wire x1="175.26" y1="99.06" x2="177.8" y2="99.06" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="SEG_DP"/>
<wire x1="177.8" y1="99.06" x2="177.8" y2="91.44" width="0.1524" layer="91"/>
<wire x1="177.8" y1="91.44" x2="132.08" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="DIG0"/>
<wire x1="132.08" y1="86.36" x2="180.34" y2="86.36" width="0.1524" layer="91"/>
<wire x1="180.34" y1="86.36" x2="180.34" y2="101.6" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="DIG1"/>
<wire x1="180.34" y1="101.6" x2="175.26" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="DIG1"/>
<wire x1="132.08" y1="83.82" x2="182.88" y2="83.82" width="0.1524" layer="91"/>
<wire x1="182.88" y1="83.82" x2="182.88" y2="104.14" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="DIG2"/>
<wire x1="182.88" y1="104.14" x2="175.26" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="DIG2"/>
<wire x1="132.08" y1="81.28" x2="185.42" y2="81.28" width="0.1524" layer="91"/>
<wire x1="185.42" y1="81.28" x2="185.42" y2="106.68" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="DIG3"/>
<wire x1="185.42" y1="106.68" x2="175.26" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="GND17" gate="1" pin="GND"/>
<pinref part="IC5" gate="G$1" pin="GND"/>
<wire x1="99.06" y1="76.2" x2="99.06" y2="78.74" width="0.1524" layer="91"/>
<wire x1="99.06" y1="78.74" x2="101.6" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND18" gate="1" pin="GND"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="71.12" y1="93.98" x2="71.12" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="63.5" y1="93.98" x2="63.5" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="-"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="VDD"/>
<wire x1="101.6" y1="109.22" x2="86.36" y2="109.22" width="0.1524" layer="91"/>
<label x="60.96" y="109.22" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="86.36" y1="109.22" x2="71.12" y2="109.22" width="0.1524" layer="91"/>
<wire x1="71.12" y1="109.22" x2="63.5" y2="109.22" width="0.1524" layer="91"/>
<wire x1="63.5" y1="109.22" x2="60.96" y2="109.22" width="0.1524" layer="91"/>
<wire x1="88.9" y1="106.68" x2="86.36" y2="106.68" width="0.1524" layer="91"/>
<wire x1="86.36" y1="106.68" x2="86.36" y2="109.22" width="0.1524" layer="91"/>
<junction x="86.36" y="109.22"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="71.12" y1="104.14" x2="71.12" y2="109.22" width="0.1524" layer="91"/>
<junction x="71.12" y="109.22"/>
<wire x1="63.5" y1="104.14" x2="63.5" y2="109.22" width="0.1524" layer="91"/>
<junction x="63.5" y="109.22"/>
<pinref part="C16" gate="G$1" pin="+"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="IC5" gate="G$1" pin="ISET"/>
<wire x1="99.06" y1="106.68" x2="101.6" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SPI1_MOSI" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="DIN"/>
<wire x1="101.6" y1="96.52" x2="99.06" y2="96.52" width="0.1524" layer="91"/>
<label x="99.06" y="96.52" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="!LED_CS" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="LOAD/CSN"/>
<wire x1="101.6" y1="88.9" x2="99.06" y2="88.9" width="0.1524" layer="91"/>
<label x="99.06" y="88.9" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SPI1_SCK" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="CLK"/>
<wire x1="101.6" y1="86.36" x2="99.06" y2="86.36" width="0.1524" layer="91"/>
<label x="99.06" y="86.36" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>SD Card</description>
<plain>
</plain>
<instances>
<instance part="J3" gate="G$1" x="157.48" y="96.52" smashed="yes">
<attribute name="NAME" x="154.94" y="114.3" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="154.94" y="81.28" size="1.778" layer="96" align="center"/>
<attribute name="DIGIKEY" x="157.48" y="96.52" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="157.48" y="96.52" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="157.48" y="96.52" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND28" gate="1" x="142.24" y="78.74" smashed="yes">
<attribute name="VALUE" x="142.24" y="78.486" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R12" gate="G$1" x="134.62" y="116.84" smashed="yes" rot="R90">
<attribute name="NAME" x="133.1214" y="113.03" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="137.922" y="113.03" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="134.62" y="116.84" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="134.62" y="116.84" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="134.62" y="116.84" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="R11" gate="G$1" x="127" y="116.84" smashed="yes" rot="R90">
<attribute name="NAME" x="125.5014" y="113.03" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="130.302" y="113.03" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="127" y="116.84" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="127" y="116.84" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="127" y="116.84" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="R10" gate="G$1" x="119.38" y="116.84" smashed="yes" rot="R90">
<attribute name="NAME" x="117.8814" y="113.03" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="122.682" y="113.03" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="119.38" y="116.84" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="119.38" y="116.84" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="119.38" y="116.84" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="R9" gate="G$1" x="111.76" y="116.84" smashed="yes" rot="R90">
<attribute name="NAME" x="110.2614" y="113.03" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="115.062" y="113.03" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="111.76" y="116.84" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="111.76" y="116.84" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="111.76" y="116.84" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="R8" gate="G$1" x="104.14" y="116.84" smashed="yes" rot="R90">
<attribute name="NAME" x="102.6414" y="113.03" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="107.442" y="113.03" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="104.14" y="116.84" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="104.14" y="116.84" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="104.14" y="116.84" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="R7" gate="G$1" x="96.52" y="116.84" smashed="yes" rot="R90">
<attribute name="NAME" x="95.0214" y="113.03" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="99.822" y="113.03" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="96.52" y="116.84" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="96.52" y="116.84" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="96.52" y="116.84" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="FRAME8" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FRAME9" gate="G$2" x="162.56" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="175.26" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="248.92" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="180.34" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="C18" gate="G$1" x="175.26" y="96.52" smashed="yes">
<attribute name="NAME" x="176.784" y="99.441" size="1.778" layer="95"/>
<attribute name="VALUE" x="176.784" y="94.361" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="175.26" y="96.52" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="175.26" y="96.52" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="175.26" y="96.52" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND29" gate="1" x="180.34" y="86.36" smashed="yes">
<attribute name="VALUE" x="180.34" y="86.106" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="C19" gate="G$1" x="185.42" y="99.06" smashed="yes">
<attribute name="NAME" x="186.436" y="99.695" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="186.436" y="94.869" size="1.778" layer="96" font="vector"/>
<attribute name="DIGIKEY" x="185.42" y="99.06" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="185.42" y="99.06" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="185.42" y="99.06" size="1.27" layer="96" display="off"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND28" gate="1" pin="GND"/>
<pinref part="J3" gate="G$1" pin="VSS"/>
<wire x1="142.24" y1="81.28" x2="142.24" y2="86.36" width="0.1524" layer="91"/>
<wire x1="142.24" y1="86.36" x2="142.24" y2="96.52" width="0.1524" layer="91"/>
<wire x1="142.24" y1="96.52" x2="144.78" y2="96.52" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="SHIELD"/>
<wire x1="144.78" y1="86.36" x2="142.24" y2="86.36" width="0.1524" layer="91"/>
<junction x="142.24" y="86.36"/>
</segment>
<segment>
<wire x1="185.42" y1="93.98" x2="185.42" y2="91.44" width="0.1524" layer="91"/>
<wire x1="185.42" y1="91.44" x2="180.34" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="180.34" y1="91.44" x2="175.26" y2="91.44" width="0.1524" layer="91"/>
<wire x1="175.26" y1="91.44" x2="175.26" y2="93.98" width="0.1524" layer="91"/>
<wire x1="180.34" y1="91.44" x2="180.34" y2="88.9" width="0.1524" layer="91"/>
<junction x="180.34" y="91.44"/>
<pinref part="GND29" gate="1" pin="GND"/>
<pinref part="C19" gate="G$1" pin="-"/>
</segment>
</net>
<net name="SDIO_D2" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="DAT2"/>
<wire x1="144.78" y1="109.22" x2="134.62" y2="109.22" width="0.1524" layer="91"/>
<label x="91.44" y="109.22" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="134.62" y1="109.22" x2="91.44" y2="109.22" width="0.1524" layer="91"/>
<wire x1="134.62" y1="111.76" x2="134.62" y2="109.22" width="0.1524" layer="91"/>
<junction x="134.62" y="109.22"/>
</segment>
</net>
<net name="SDIO_D3" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="CD/DAT3"/>
<wire x1="144.78" y1="106.68" x2="127" y2="106.68" width="0.1524" layer="91"/>
<label x="91.44" y="106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="127" y1="106.68" x2="91.44" y2="106.68" width="0.1524" layer="91"/>
<wire x1="127" y1="111.76" x2="127" y2="106.68" width="0.1524" layer="91"/>
<junction x="127" y="106.68"/>
</segment>
</net>
<net name="SDIO_CMD" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="CMD"/>
<wire x1="144.78" y1="104.14" x2="119.38" y2="104.14" width="0.1524" layer="91"/>
<label x="91.44" y="104.14" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="119.38" y1="104.14" x2="91.44" y2="104.14" width="0.1524" layer="91"/>
<wire x1="119.38" y1="111.76" x2="119.38" y2="104.14" width="0.1524" layer="91"/>
<junction x="119.38" y="104.14"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="VDD"/>
<wire x1="144.78" y1="101.6" x2="142.24" y2="101.6" width="0.1524" layer="91"/>
<wire x1="142.24" y1="101.6" x2="142.24" y2="124.46" width="0.1524" layer="91"/>
<wire x1="142.24" y1="124.46" x2="134.62" y2="124.46" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="134.62" y1="124.46" x2="127" y2="124.46" width="0.1524" layer="91"/>
<wire x1="127" y1="124.46" x2="119.38" y2="124.46" width="0.1524" layer="91"/>
<wire x1="119.38" y1="124.46" x2="111.76" y2="124.46" width="0.1524" layer="91"/>
<wire x1="111.76" y1="124.46" x2="104.14" y2="124.46" width="0.1524" layer="91"/>
<wire x1="104.14" y1="124.46" x2="96.52" y2="124.46" width="0.1524" layer="91"/>
<wire x1="96.52" y1="121.92" x2="96.52" y2="124.46" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="104.14" y1="121.92" x2="104.14" y2="124.46" width="0.1524" layer="91"/>
<junction x="104.14" y="124.46"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="111.76" y1="121.92" x2="111.76" y2="124.46" width="0.1524" layer="91"/>
<junction x="111.76" y="124.46"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="119.38" y1="121.92" x2="119.38" y2="124.46" width="0.1524" layer="91"/>
<junction x="119.38" y="124.46"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="127" y1="121.92" x2="127" y2="124.46" width="0.1524" layer="91"/>
<junction x="127" y="124.46"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="134.62" y1="121.92" x2="134.62" y2="124.46" width="0.1524" layer="91"/>
<junction x="134.62" y="124.46"/>
<wire x1="96.52" y1="124.46" x2="91.44" y2="124.46" width="0.1524" layer="91"/>
<junction x="96.52" y="124.46"/>
<label x="91.44" y="124.46" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
<segment>
<wire x1="185.42" y1="101.6" x2="185.42" y2="104.14" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="185.42" y1="104.14" x2="180.34" y2="104.14" width="0.1524" layer="91"/>
<wire x1="180.34" y1="104.14" x2="175.26" y2="104.14" width="0.1524" layer="91"/>
<wire x1="175.26" y1="104.14" x2="175.26" y2="101.6" width="0.1524" layer="91"/>
<wire x1="180.34" y1="104.14" x2="180.34" y2="106.68" width="0.1524" layer="91"/>
<junction x="180.34" y="104.14"/>
<label x="180.34" y="106.68" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="C19" gate="G$1" pin="+"/>
</segment>
</net>
<net name="SDIO_CLK" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="CLK"/>
<wire x1="144.78" y1="99.06" x2="111.76" y2="99.06" width="0.1524" layer="91"/>
<label x="91.44" y="99.06" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="111.76" y1="99.06" x2="91.44" y2="99.06" width="0.1524" layer="91"/>
<wire x1="111.76" y1="111.76" x2="111.76" y2="99.06" width="0.1524" layer="91"/>
<junction x="111.76" y="99.06"/>
</segment>
</net>
<net name="SDIO_D0" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="DAT0"/>
<wire x1="144.78" y1="93.98" x2="104.14" y2="93.98" width="0.1524" layer="91"/>
<label x="91.44" y="93.98" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="104.14" y1="93.98" x2="91.44" y2="93.98" width="0.1524" layer="91"/>
<wire x1="104.14" y1="111.76" x2="104.14" y2="93.98" width="0.1524" layer="91"/>
<junction x="104.14" y="93.98"/>
</segment>
</net>
<net name="SDIO_D1" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="DAT1"/>
<wire x1="144.78" y1="91.44" x2="96.52" y2="91.44" width="0.1524" layer="91"/>
<label x="91.44" y="91.44" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="96.52" y1="91.44" x2="91.44" y2="91.44" width="0.1524" layer="91"/>
<wire x1="96.52" y1="111.76" x2="96.52" y2="91.44" width="0.1524" layer="91"/>
<junction x="96.52" y="91.44"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Sense voltage dividers</description>
<plain>
</plain>
<instances>
<instance part="J4" gate="G$1" x="43.18" y="71.12" smashed="yes" rot="MR0">
<attribute name="NAME" x="43.18" y="83.82" size="1.778" layer="95" rot="MR0" align="center"/>
<attribute name="NAME" x="43.18" y="58.42" size="1.778" layer="96" rot="MR0" align="center"/>
<attribute name="DIGIKEY" x="43.18" y="71.12" size="1.27" layer="96" rot="MR0" display="off"/>
<attribute name="MF" x="43.18" y="71.12" size="1.27" layer="96" rot="MR0" display="off"/>
<attribute name="MPN" x="43.18" y="71.12" size="1.27" layer="96" rot="MR0" display="off"/>
</instance>
<instance part="GND30" gate="1" x="55.88" y="58.42" smashed="yes">
<attribute name="VALUE" x="55.88" y="58.166" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R15" gate="G$1" x="96.52" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="95.0214" y="80.01" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="99.822" y="80.01" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="96.52" y="83.82" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="96.52" y="83.82" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="96.52" y="83.82" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="R16" gate="G$1" x="96.52" y="68.58" smashed="yes" rot="R90">
<attribute name="NAME" x="95.0214" y="64.77" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="99.822" y="64.77" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="96.52" y="68.58" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="96.52" y="68.58" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="96.52" y="68.58" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="GND31" gate="1" x="96.52" y="58.42" smashed="yes">
<attribute name="VALUE" x="96.52" y="58.166" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R19" gate="G$1" x="132.08" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="130.5814" y="80.01" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="135.382" y="80.01" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="132.08" y="83.82" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="132.08" y="83.82" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="132.08" y="83.82" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="R20" gate="G$1" x="132.08" y="68.58" smashed="yes" rot="R90">
<attribute name="NAME" x="130.5814" y="64.77" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="135.382" y="64.77" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="132.08" y="68.58" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="132.08" y="68.58" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="132.08" y="68.58" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="GND32" gate="1" x="132.08" y="58.42" smashed="yes">
<attribute name="VALUE" x="132.08" y="58.166" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R21" gate="G$1" x="167.64" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="166.1414" y="80.01" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="170.942" y="80.01" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="167.64" y="83.82" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="167.64" y="83.82" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="167.64" y="83.82" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="R22" gate="G$1" x="167.64" y="68.58" smashed="yes" rot="R90">
<attribute name="NAME" x="166.1414" y="64.77" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="170.942" y="64.77" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="167.64" y="68.58" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="167.64" y="68.58" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="167.64" y="68.58" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="GND33" gate="1" x="167.64" y="58.42" smashed="yes">
<attribute name="VALUE" x="167.64" y="58.166" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R23" gate="G$1" x="203.2" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="201.7014" y="80.01" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="206.502" y="80.01" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="203.2" y="83.82" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="203.2" y="83.82" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="203.2" y="83.82" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="R24" gate="G$1" x="203.2" y="68.58" smashed="yes" rot="R90">
<attribute name="NAME" x="201.7014" y="64.77" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="206.502" y="64.77" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="203.2" y="68.58" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="203.2" y="68.58" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="203.2" y="68.58" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="GND34" gate="1" x="203.2" y="58.42" smashed="yes">
<attribute name="VALUE" x="203.2" y="58.166" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R13" gate="G$1" x="96.52" y="134.62" smashed="yes" rot="R90">
<attribute name="NAME" x="95.0214" y="130.81" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="99.822" y="130.81" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="96.52" y="134.62" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="96.52" y="134.62" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="96.52" y="134.62" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="R14" gate="G$1" x="96.52" y="119.38" smashed="yes" rot="R90">
<attribute name="NAME" x="95.0214" y="115.57" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="99.822" y="115.57" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="96.52" y="119.38" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="96.52" y="119.38" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="96.52" y="119.38" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="GND35" gate="1" x="96.52" y="109.22" smashed="yes">
<attribute name="VALUE" x="96.52" y="108.966" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R17" gate="G$1" x="132.08" y="134.62" smashed="yes" rot="R90">
<attribute name="NAME" x="130.5814" y="130.81" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="135.382" y="130.81" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="132.08" y="134.62" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="132.08" y="134.62" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="132.08" y="134.62" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="R18" gate="G$1" x="132.08" y="119.38" smashed="yes" rot="R90">
<attribute name="NAME" x="130.5814" y="115.57" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="135.382" y="115.57" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="132.08" y="119.38" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="132.08" y="119.38" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="132.08" y="119.38" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="GND36" gate="1" x="132.08" y="109.22" smashed="yes">
<attribute name="VALUE" x="132.08" y="108.966" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="FRAME10" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FRAME11" gate="G$2" x="162.56" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="175.26" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="248.92" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="180.34" y="19.05" size="2.54" layer="94"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND30" gate="1" pin="GND"/>
<pinref part="J4" gate="G$1" pin="1"/>
<wire x1="55.88" y1="60.96" x2="55.88" y2="63.5" width="0.1524" layer="91"/>
<wire x1="55.88" y1="63.5" x2="55.88" y2="68.58" width="0.1524" layer="91"/>
<wire x1="55.88" y1="68.58" x2="53.34" y2="68.58" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$1" pin="SHIELD"/>
<wire x1="53.34" y1="63.5" x2="55.88" y2="63.5" width="0.1524" layer="91"/>
<junction x="55.88" y="63.5"/>
</segment>
<segment>
<pinref part="GND31" gate="1" pin="GND"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="96.52" y1="60.96" x2="96.52" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND32" gate="1" pin="GND"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="132.08" y1="60.96" x2="132.08" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND33" gate="1" pin="GND"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="167.64" y1="60.96" x2="167.64" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND34" gate="1" pin="GND"/>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="203.2" y1="60.96" x2="203.2" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND35" gate="1" pin="GND"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="96.52" y1="111.76" x2="96.52" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND36" gate="1" pin="GND"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="132.08" y1="111.76" x2="132.08" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="1S_SENSE" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="2"/>
<wire x1="53.34" y1="71.12" x2="58.42" y2="71.12" width="0.1524" layer="91"/>
<label x="58.42" y="71.12" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="96.52" y1="91.44" x2="96.52" y2="88.9" width="0.1524" layer="91"/>
<label x="96.52" y="91.44" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="2S_SENSE" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="3"/>
<wire x1="53.34" y1="73.66" x2="58.42" y2="73.66" width="0.1524" layer="91"/>
<label x="58.42" y="73.66" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="132.08" y1="91.44" x2="132.08" y2="88.9" width="0.1524" layer="91"/>
<label x="132.08" y="91.44" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="3S_SENSE" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="4"/>
<wire x1="53.34" y1="76.2" x2="58.42" y2="76.2" width="0.1524" layer="91"/>
<label x="58.42" y="76.2" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="167.64" y1="91.44" x2="167.64" y2="88.9" width="0.1524" layer="91"/>
<label x="167.64" y="91.44" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="4S_SENSE" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="5"/>
<wire x1="53.34" y1="78.74" x2="58.42" y2="78.74" width="0.1524" layer="91"/>
<label x="58.42" y="78.74" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="203.2" y1="91.44" x2="203.2" y2="88.9" width="0.1524" layer="91"/>
<label x="203.2" y="91.44" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="1S_SENSE_NORMALIZED" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="96.52" y1="73.66" x2="96.52" y2="76.2" width="0.1524" layer="91"/>
<wire x1="96.52" y1="76.2" x2="96.52" y2="78.74" width="0.1524" layer="91"/>
<wire x1="96.52" y1="76.2" x2="99.06" y2="76.2" width="0.1524" layer="91"/>
<junction x="96.52" y="76.2"/>
<label x="99.06" y="76.2" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="2S_SENSE_NORMALIZED" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="2"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="132.08" y1="73.66" x2="132.08" y2="76.2" width="0.1524" layer="91"/>
<wire x1="132.08" y1="76.2" x2="132.08" y2="78.74" width="0.1524" layer="91"/>
<wire x1="132.08" y1="76.2" x2="134.62" y2="76.2" width="0.1524" layer="91"/>
<junction x="132.08" y="76.2"/>
<label x="134.62" y="76.2" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="3S_SENSE_NORMALIZED" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="2"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="167.64" y1="73.66" x2="167.64" y2="76.2" width="0.1524" layer="91"/>
<wire x1="167.64" y1="76.2" x2="167.64" y2="78.74" width="0.1524" layer="91"/>
<wire x1="167.64" y1="76.2" x2="170.18" y2="76.2" width="0.1524" layer="91"/>
<junction x="167.64" y="76.2"/>
<label x="170.18" y="76.2" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="4S_SENSE_NORMALIZED" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="2"/>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="203.2" y1="73.66" x2="203.2" y2="76.2" width="0.1524" layer="91"/>
<wire x1="203.2" y1="76.2" x2="203.2" y2="78.74" width="0.1524" layer="91"/>
<wire x1="203.2" y1="76.2" x2="205.74" y2="76.2" width="0.1524" layer="91"/>
<junction x="203.2" y="76.2"/>
<label x="205.74" y="76.2" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="6V_UNCHECKED" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="96.52" y1="142.24" x2="96.52" y2="139.7" width="0.1524" layer="91"/>
<label x="96.52" y="142.24" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="12V_UNCHECKED" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="132.08" y1="142.24" x2="132.08" y2="139.7" width="0.1524" layer="91"/>
<label x="132.08" y="142.24" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="6V_VSENSE" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="96.52" y1="124.46" x2="96.52" y2="127" width="0.1524" layer="91"/>
<wire x1="96.52" y1="127" x2="96.52" y2="129.54" width="0.1524" layer="91"/>
<wire x1="96.52" y1="127" x2="99.06" y2="127" width="0.1524" layer="91"/>
<junction x="96.52" y="127"/>
<label x="99.06" y="127" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="12V_VSENSE" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="2"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="132.08" y1="124.46" x2="132.08" y2="127" width="0.1524" layer="91"/>
<wire x1="132.08" y1="127" x2="132.08" y2="129.54" width="0.1524" layer="91"/>
<wire x1="132.08" y1="127" x2="134.62" y2="127" width="0.1524" layer="91"/>
<junction x="132.08" y="127"/>
<label x="134.62" y="127" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Outputs</description>
<plain>
</plain>
<instances>
<instance part="J5" gate="G$1" x="73.66" y="132.08" smashed="yes">
<attribute name="NAME" x="71.12" y="142.24" size="1.27" layer="95"/>
<attribute name="VALUE" x="71.12" y="124.46" size="1.27" layer="96"/>
</instance>
<instance part="FRAME12" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FRAME13" gate="G$2" x="162.56" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="175.26" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="248.92" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="180.34" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="J7" gate="G$1" x="200.66" y="129.54" smashed="yes">
<attribute name="NAME" x="198.12" y="147.32" size="1.27" layer="95"/>
<attribute name="VALUE" x="198.12" y="109.22" size="1.27" layer="96"/>
</instance>
<instance part="GND37" gate="1" x="185.42" y="109.22" smashed="yes">
<attribute name="VALUE" x="185.42" y="108.966" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="J8" gate="G$1" x="200.66" y="83.82" smashed="yes">
<attribute name="NAME" x="198.12" y="99.06" size="1.27" layer="95"/>
<attribute name="VALUE" x="198.12" y="66.04" size="1.27" layer="96"/>
</instance>
<instance part="GND38" gate="1" x="185.42" y="66.04" smashed="yes">
<attribute name="VALUE" x="185.42" y="65.786" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="J6" gate="G$1" x="86.36" y="78.74" smashed="yes">
<attribute name="NAME" x="83.82" y="91.44" size="1.27" layer="95"/>
<attribute name="VALUE" x="83.82" y="68.58" size="1.27" layer="96"/>
</instance>
<instance part="GND39" gate="1" x="71.12" y="68.58" smashed="yes">
<attribute name="VALUE" x="71.12" y="68.326" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND41" gate="1" x="86.36" y="124.46" smashed="yes">
<attribute name="VALUE" x="86.36" y="124.206" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R25" gate="G$1" x="86.36" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="84.8614" y="140.97" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="89.662" y="140.97" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="86.36" y="144.78" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="86.36" y="144.78" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="86.36" y="144.78" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$4" class="0">
<segment>
<pinref part="J5" gate="G$1" pin="P$4"/>
<wire x1="83.82" y1="134.62" x2="86.36" y2="134.62" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="P$2"/>
<wire x1="86.36" y1="134.62" x2="86.36" y2="132.08" width="0.1524" layer="91"/>
<wire x1="86.36" y1="132.08" x2="83.82" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="GND37" gate="1" pin="GND"/>
<pinref part="J7" gate="G$1" pin="P$1"/>
<wire x1="185.42" y1="111.76" x2="185.42" y2="114.3" width="0.1524" layer="91"/>
<wire x1="185.42" y1="114.3" x2="185.42" y2="116.84" width="0.1524" layer="91"/>
<wire x1="185.42" y1="116.84" x2="185.42" y2="119.38" width="0.1524" layer="91"/>
<wire x1="185.42" y1="119.38" x2="185.42" y2="121.92" width="0.1524" layer="91"/>
<wire x1="185.42" y1="121.92" x2="185.42" y2="124.46" width="0.1524" layer="91"/>
<wire x1="185.42" y1="124.46" x2="185.42" y2="127" width="0.1524" layer="91"/>
<wire x1="185.42" y1="127" x2="185.42" y2="129.54" width="0.1524" layer="91"/>
<wire x1="185.42" y1="129.54" x2="185.42" y2="132.08" width="0.1524" layer="91"/>
<wire x1="185.42" y1="132.08" x2="185.42" y2="134.62" width="0.1524" layer="91"/>
<wire x1="185.42" y1="134.62" x2="185.42" y2="137.16" width="0.1524" layer="91"/>
<wire x1="185.42" y1="137.16" x2="185.42" y2="139.7" width="0.1524" layer="91"/>
<wire x1="185.42" y1="139.7" x2="185.42" y2="142.24" width="0.1524" layer="91"/>
<wire x1="185.42" y1="142.24" x2="187.96" y2="142.24" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="P$2"/>
<wire x1="187.96" y1="139.7" x2="185.42" y2="139.7" width="0.1524" layer="91"/>
<junction x="185.42" y="139.7"/>
<pinref part="J7" gate="G$1" pin="P$3"/>
<wire x1="187.96" y1="137.16" x2="185.42" y2="137.16" width="0.1524" layer="91"/>
<junction x="185.42" y="137.16"/>
<pinref part="J7" gate="G$1" pin="P$4"/>
<wire x1="187.96" y1="134.62" x2="185.42" y2="134.62" width="0.1524" layer="91"/>
<junction x="185.42" y="134.62"/>
<pinref part="J7" gate="G$1" pin="P$5"/>
<wire x1="187.96" y1="132.08" x2="185.42" y2="132.08" width="0.1524" layer="91"/>
<junction x="185.42" y="132.08"/>
<pinref part="J7" gate="G$1" pin="P$6"/>
<wire x1="187.96" y1="129.54" x2="185.42" y2="129.54" width="0.1524" layer="91"/>
<junction x="185.42" y="129.54"/>
<pinref part="J7" gate="G$1" pin="P$7"/>
<wire x1="187.96" y1="127" x2="185.42" y2="127" width="0.1524" layer="91"/>
<junction x="185.42" y="127"/>
<pinref part="J7" gate="G$1" pin="P$8"/>
<wire x1="187.96" y1="124.46" x2="185.42" y2="124.46" width="0.1524" layer="91"/>
<junction x="185.42" y="124.46"/>
<pinref part="J7" gate="G$1" pin="P$9"/>
<wire x1="187.96" y1="121.92" x2="185.42" y2="121.92" width="0.1524" layer="91"/>
<junction x="185.42" y="121.92"/>
<pinref part="J7" gate="G$1" pin="P$10"/>
<wire x1="187.96" y1="119.38" x2="185.42" y2="119.38" width="0.1524" layer="91"/>
<junction x="185.42" y="119.38"/>
<pinref part="J7" gate="G$1" pin="P$11"/>
<wire x1="187.96" y1="116.84" x2="185.42" y2="116.84" width="0.1524" layer="91"/>
<junction x="185.42" y="116.84"/>
<pinref part="J7" gate="G$1" pin="P$12"/>
<wire x1="187.96" y1="114.3" x2="185.42" y2="114.3" width="0.1524" layer="91"/>
<junction x="185.42" y="114.3"/>
</segment>
<segment>
<pinref part="GND38" gate="1" pin="GND"/>
<pinref part="J8" gate="G$1" pin="P$1"/>
<wire x1="185.42" y1="68.58" x2="185.42" y2="71.12" width="0.1524" layer="91"/>
<wire x1="185.42" y1="71.12" x2="185.42" y2="73.66" width="0.1524" layer="91"/>
<wire x1="185.42" y1="73.66" x2="185.42" y2="76.2" width="0.1524" layer="91"/>
<wire x1="185.42" y1="76.2" x2="185.42" y2="78.74" width="0.1524" layer="91"/>
<wire x1="185.42" y1="78.74" x2="185.42" y2="81.28" width="0.1524" layer="91"/>
<wire x1="185.42" y1="81.28" x2="185.42" y2="83.82" width="0.1524" layer="91"/>
<wire x1="185.42" y1="83.82" x2="185.42" y2="86.36" width="0.1524" layer="91"/>
<wire x1="185.42" y1="86.36" x2="185.42" y2="88.9" width="0.1524" layer="91"/>
<wire x1="185.42" y1="88.9" x2="185.42" y2="91.44" width="0.1524" layer="91"/>
<wire x1="185.42" y1="91.44" x2="185.42" y2="93.98" width="0.1524" layer="91"/>
<wire x1="185.42" y1="93.98" x2="187.96" y2="93.98" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="P$2"/>
<wire x1="185.42" y1="91.44" x2="187.96" y2="91.44" width="0.1524" layer="91"/>
<junction x="185.42" y="91.44"/>
<pinref part="J8" gate="G$1" pin="P$3"/>
<wire x1="187.96" y1="88.9" x2="185.42" y2="88.9" width="0.1524" layer="91"/>
<junction x="185.42" y="88.9"/>
<pinref part="J8" gate="G$1" pin="P$4"/>
<wire x1="187.96" y1="86.36" x2="185.42" y2="86.36" width="0.1524" layer="91"/>
<junction x="185.42" y="86.36"/>
<pinref part="J8" gate="G$1" pin="P$5"/>
<wire x1="187.96" y1="83.82" x2="185.42" y2="83.82" width="0.1524" layer="91"/>
<junction x="185.42" y="83.82"/>
<pinref part="J8" gate="G$1" pin="P$6"/>
<wire x1="187.96" y1="81.28" x2="185.42" y2="81.28" width="0.1524" layer="91"/>
<junction x="185.42" y="81.28"/>
<pinref part="J8" gate="G$1" pin="P$7"/>
<wire x1="187.96" y1="78.74" x2="185.42" y2="78.74" width="0.1524" layer="91"/>
<junction x="185.42" y="78.74"/>
<pinref part="J8" gate="G$1" pin="P$8"/>
<wire x1="187.96" y1="76.2" x2="185.42" y2="76.2" width="0.1524" layer="91"/>
<junction x="185.42" y="76.2"/>
<pinref part="J8" gate="G$1" pin="P$9"/>
<wire x1="187.96" y1="73.66" x2="185.42" y2="73.66" width="0.1524" layer="91"/>
<junction x="185.42" y="73.66"/>
<pinref part="J8" gate="G$1" pin="P$10"/>
<wire x1="187.96" y1="71.12" x2="185.42" y2="71.12" width="0.1524" layer="91"/>
<junction x="185.42" y="71.12"/>
</segment>
<segment>
<pinref part="GND39" gate="1" pin="GND"/>
<pinref part="J6" gate="G$1" pin="P$1"/>
<wire x1="71.12" y1="71.12" x2="71.12" y2="73.66" width="0.1524" layer="91"/>
<wire x1="71.12" y1="73.66" x2="71.12" y2="76.2" width="0.1524" layer="91"/>
<wire x1="71.12" y1="76.2" x2="71.12" y2="78.74" width="0.1524" layer="91"/>
<wire x1="71.12" y1="78.74" x2="71.12" y2="81.28" width="0.1524" layer="91"/>
<wire x1="71.12" y1="81.28" x2="71.12" y2="83.82" width="0.1524" layer="91"/>
<wire x1="71.12" y1="83.82" x2="71.12" y2="86.36" width="0.1524" layer="91"/>
<wire x1="71.12" y1="86.36" x2="73.66" y2="86.36" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="P$2"/>
<wire x1="73.66" y1="83.82" x2="71.12" y2="83.82" width="0.1524" layer="91"/>
<junction x="71.12" y="83.82"/>
<pinref part="J6" gate="G$1" pin="P$3"/>
<wire x1="73.66" y1="81.28" x2="71.12" y2="81.28" width="0.1524" layer="91"/>
<junction x="71.12" y="81.28"/>
<pinref part="J6" gate="G$1" pin="P$4"/>
<wire x1="73.66" y1="78.74" x2="71.12" y2="78.74" width="0.1524" layer="91"/>
<junction x="71.12" y="78.74"/>
<pinref part="J6" gate="G$1" pin="P$5"/>
<wire x1="73.66" y1="76.2" x2="71.12" y2="76.2" width="0.1524" layer="91"/>
<junction x="71.12" y="76.2"/>
<pinref part="J6" gate="G$1" pin="P$6"/>
<wire x1="73.66" y1="73.66" x2="71.12" y2="73.66" width="0.1524" layer="91"/>
<junction x="71.12" y="73.66"/>
</segment>
<segment>
<pinref part="GND41" gate="1" pin="GND"/>
<pinref part="J5" gate="G$1" pin="P$3"/>
<wire x1="86.36" y1="127" x2="86.36" y2="129.54" width="0.1524" layer="91"/>
<wire x1="86.36" y1="129.54" x2="83.82" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="6V_CHECKED" class="0">
<segment>
<pinref part="J7" gate="G$1" pin="P$23"/>
<pinref part="J7" gate="G$1" pin="P$24"/>
<wire x1="213.36" y1="142.24" x2="215.9" y2="142.24" width="0.1524" layer="91"/>
<wire x1="215.9" y1="142.24" x2="218.44" y2="142.24" width="0.1524" layer="91"/>
<junction x="215.9" y="142.24"/>
<wire x1="215.9" y1="142.24" x2="215.9" y2="139.7" width="0.1524" layer="91"/>
<wire x1="215.9" y1="139.7" x2="213.36" y2="139.7" width="0.1524" layer="91"/>
<junction x="215.9" y="139.7"/>
<wire x1="215.9" y1="139.7" x2="215.9" y2="137.16" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="P$22"/>
<wire x1="213.36" y1="137.16" x2="215.9" y2="137.16" width="0.1524" layer="91"/>
<label x="218.44" y="142.24" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="6V_SAFE" class="0">
<segment>
<pinref part="J7" gate="G$1" pin="P$21"/>
<wire x1="215.9" y1="134.62" x2="213.36" y2="134.62" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="P$13"/>
<wire x1="213.36" y1="114.3" x2="215.9" y2="114.3" width="0.1524" layer="91"/>
<wire x1="215.9" y1="114.3" x2="215.9" y2="116.84" width="0.1524" layer="91"/>
<wire x1="215.9" y1="116.84" x2="215.9" y2="119.38" width="0.1524" layer="91"/>
<wire x1="215.9" y1="119.38" x2="215.9" y2="121.92" width="0.1524" layer="91"/>
<wire x1="215.9" y1="121.92" x2="215.9" y2="124.46" width="0.1524" layer="91"/>
<wire x1="215.9" y1="124.46" x2="215.9" y2="127" width="0.1524" layer="91"/>
<wire x1="215.9" y1="127" x2="215.9" y2="129.54" width="0.1524" layer="91"/>
<wire x1="215.9" y1="129.54" x2="215.9" y2="132.08" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="P$20"/>
<wire x1="213.36" y1="132.08" x2="215.9" y2="132.08" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="P$19"/>
<wire x1="213.36" y1="129.54" x2="215.9" y2="129.54" width="0.1524" layer="91"/>
<junction x="215.9" y="129.54"/>
<pinref part="J7" gate="G$1" pin="P$18"/>
<wire x1="213.36" y1="127" x2="215.9" y2="127" width="0.1524" layer="91"/>
<junction x="215.9" y="127"/>
<pinref part="J7" gate="G$1" pin="P$17"/>
<wire x1="213.36" y1="124.46" x2="215.9" y2="124.46" width="0.1524" layer="91"/>
<junction x="215.9" y="124.46"/>
<pinref part="J7" gate="G$1" pin="P$16"/>
<wire x1="213.36" y1="121.92" x2="215.9" y2="121.92" width="0.1524" layer="91"/>
<junction x="215.9" y="121.92"/>
<pinref part="J7" gate="G$1" pin="P$15"/>
<wire x1="213.36" y1="119.38" x2="215.9" y2="119.38" width="0.1524" layer="91"/>
<junction x="215.9" y="119.38"/>
<pinref part="J7" gate="G$1" pin="P$14"/>
<wire x1="213.36" y1="116.84" x2="215.9" y2="116.84" width="0.1524" layer="91"/>
<junction x="215.9" y="116.84"/>
<wire x1="215.9" y1="134.62" x2="215.9" y2="132.08" width="0.1524" layer="91"/>
<junction x="215.9" y="132.08"/>
<wire x1="215.9" y1="134.62" x2="218.44" y2="134.62" width="0.1524" layer="91"/>
<junction x="215.9" y="134.62"/>
<label x="218.44" y="134.62" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="12V_SAFE" class="0">
<segment>
<pinref part="J8" gate="G$1" pin="P$11"/>
<wire x1="213.36" y1="71.12" x2="215.9" y2="71.12" width="0.1524" layer="91"/>
<wire x1="215.9" y1="71.12" x2="215.9" y2="73.66" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="P$18"/>
<wire x1="215.9" y1="73.66" x2="215.9" y2="76.2" width="0.1524" layer="91"/>
<wire x1="215.9" y1="76.2" x2="215.9" y2="78.74" width="0.1524" layer="91"/>
<wire x1="215.9" y1="78.74" x2="215.9" y2="81.28" width="0.1524" layer="91"/>
<wire x1="215.9" y1="81.28" x2="215.9" y2="83.82" width="0.1524" layer="91"/>
<wire x1="215.9" y1="83.82" x2="215.9" y2="86.36" width="0.1524" layer="91"/>
<wire x1="215.9" y1="86.36" x2="215.9" y2="88.9" width="0.1524" layer="91"/>
<wire x1="215.9" y1="88.9" x2="213.36" y2="88.9" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="P$17"/>
<wire x1="213.36" y1="86.36" x2="215.9" y2="86.36" width="0.1524" layer="91"/>
<junction x="215.9" y="86.36"/>
<pinref part="J8" gate="G$1" pin="P$16"/>
<wire x1="213.36" y1="83.82" x2="215.9" y2="83.82" width="0.1524" layer="91"/>
<junction x="215.9" y="83.82"/>
<pinref part="J8" gate="G$1" pin="P$15"/>
<wire x1="213.36" y1="81.28" x2="215.9" y2="81.28" width="0.1524" layer="91"/>
<junction x="215.9" y="81.28"/>
<pinref part="J8" gate="G$1" pin="P$14"/>
<wire x1="213.36" y1="78.74" x2="215.9" y2="78.74" width="0.1524" layer="91"/>
<junction x="215.9" y="78.74"/>
<pinref part="J8" gate="G$1" pin="P$13"/>
<wire x1="213.36" y1="76.2" x2="215.9" y2="76.2" width="0.1524" layer="91"/>
<junction x="215.9" y="76.2"/>
<pinref part="J8" gate="G$1" pin="P$12"/>
<wire x1="213.36" y1="73.66" x2="215.9" y2="73.66" width="0.1524" layer="91"/>
<junction x="215.9" y="73.66"/>
<wire x1="215.9" y1="88.9" x2="218.44" y2="88.9" width="0.1524" layer="91"/>
<junction x="215.9" y="88.9"/>
<label x="218.44" y="88.9" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="12V_CHECKED" class="0">
<segment>
<pinref part="J8" gate="G$1" pin="P$19"/>
<wire x1="213.36" y1="91.44" x2="215.9" y2="91.44" width="0.1524" layer="91"/>
<wire x1="215.9" y1="91.44" x2="215.9" y2="93.98" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="P$20"/>
<wire x1="215.9" y1="93.98" x2="213.36" y2="93.98" width="0.1524" layer="91"/>
<wire x1="215.9" y1="93.98" x2="218.44" y2="93.98" width="0.1524" layer="91"/>
<junction x="215.9" y="93.98"/>
<label x="218.44" y="93.98" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="VBAT_CHECKED" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="P$12"/>
<wire x1="99.06" y1="86.36" x2="101.6" y2="86.36" width="0.1524" layer="91"/>
<wire x1="101.6" y1="86.36" x2="101.6" y2="83.82" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="P$11"/>
<wire x1="101.6" y1="83.82" x2="99.06" y2="83.82" width="0.1524" layer="91"/>
<wire x1="101.6" y1="86.36" x2="104.14" y2="86.36" width="0.1524" layer="91"/>
<junction x="101.6" y="86.36"/>
<label x="104.14" y="86.36" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="VBAT_SAFE" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="P$10"/>
<wire x1="99.06" y1="81.28" x2="101.6" y2="81.28" width="0.1524" layer="91"/>
<wire x1="101.6" y1="81.28" x2="101.6" y2="78.74" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="P$7"/>
<wire x1="101.6" y1="78.74" x2="101.6" y2="76.2" width="0.1524" layer="91"/>
<wire x1="101.6" y1="76.2" x2="101.6" y2="73.66" width="0.1524" layer="91"/>
<wire x1="101.6" y1="73.66" x2="99.06" y2="73.66" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="P$8"/>
<wire x1="99.06" y1="76.2" x2="101.6" y2="76.2" width="0.1524" layer="91"/>
<junction x="101.6" y="76.2"/>
<pinref part="J6" gate="G$1" pin="P$9"/>
<wire x1="99.06" y1="78.74" x2="101.6" y2="78.74" width="0.1524" layer="91"/>
<junction x="101.6" y="78.74"/>
<wire x1="101.6" y1="81.28" x2="104.14" y2="81.28" width="0.1524" layer="91"/>
<junction x="101.6" y="81.28"/>
<label x="104.14" y="81.28" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SAFETY_OK_DRIVEN" class="0">
<segment>
<wire x1="83.82" y1="137.16" x2="86.36" y2="137.16" width="0.1524" layer="91"/>
<label x="91.44" y="137.16" size="1.27" layer="95" xref="yes"/>
<pinref part="J5" gate="G$1" pin="P$1"/>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="86.36" y1="137.16" x2="91.44" y2="137.16" width="0.1524" layer="91"/>
<wire x1="86.36" y1="139.7" x2="86.36" y2="137.16" width="0.1524" layer="91"/>
<junction x="86.36" y="137.16"/>
</segment>
</net>
<net name="VBAT" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="86.36" y1="149.86" x2="86.36" y2="152.4" width="0.1524" layer="91"/>
<label x="86.36" y="152.4" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>UART debugger</description>
<plain>
<text x="121.92" y="96.52" size="1.27" layer="97" align="center-left">Note: The RX and TX lines go respectively to the pins PA9 (USART1_TX)
 and PA10 (USART1_RX) because the naming changes between this header,
 where the USB is sending and receiving, and the MCU, where it is receiving 
and sending.</text>
</plain>
<instances>
<instance part="FRAME14" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FRAME15" gate="G$2" x="162.56" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="175.26" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="248.92" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="180.34" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="JP1" gate="A" x="93.98" y="96.52" smashed="yes" rot="R180">
<attribute name="NAME" x="100.33" y="85.725" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="100.33" y="106.68" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND40" gate="1" x="104.14" y="81.28" smashed="yes">
<attribute name="VALUE" x="104.14" y="81.026" size="1.778" layer="96" align="top-center"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND40" gate="1" pin="GND"/>
<wire x1="104.14" y1="83.82" x2="104.14" y2="88.9" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="1"/>
<wire x1="104.14" y1="88.9" x2="96.52" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="UART_CST" class="0">
<segment>
<pinref part="JP1" gate="A" pin="2"/>
<wire x1="96.52" y1="91.44" x2="104.14" y2="91.44" width="0.1524" layer="91"/>
<label x="104.14" y="91.44" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="UART_TX" class="0">
<segment>
<pinref part="JP1" gate="A" pin="4"/>
<wire x1="96.52" y1="96.52" x2="104.14" y2="96.52" width="0.1524" layer="91"/>
<label x="104.14" y="96.52" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="UART_RX" class="0">
<segment>
<pinref part="JP1" gate="A" pin="5"/>
<wire x1="96.52" y1="99.06" x2="104.14" y2="99.06" width="0.1524" layer="91"/>
<label x="104.14" y="99.06" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="UART_DRT" class="0">
<segment>
<pinref part="JP1" gate="A" pin="6"/>
<wire x1="96.52" y1="101.6" x2="104.14" y2="101.6" width="0.1524" layer="91"/>
<label x="104.14" y="101.6" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Status LEDs</description>
<plain>
<text x="76.2" y="134.62" size="2.54" layer="97" rot="R90" align="center-left">E-STOP Indicator</text>
<text x="139.7" y="134.62" size="2.54" layer="97" rot="R90" align="center-left">Battery status indicator</text>
<text x="170.18" y="134.62" size="2.54" layer="97" rot="R90" align="center-left">Regulator status indicator</text>
</plain>
<instances>
<instance part="IC6" gate="G$1" x="71.12" y="106.68" smashed="yes">
<attribute name="VALUE" x="49.53" y="99.06" size="1.27" layer="96" ratio="10"/>
<attribute name="NAME" x="66.04" y="111.76" size="1.27" layer="95" ratio="10"/>
<attribute name="DIGIKEY" x="71.12" y="106.68" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="71.12" y="106.68" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="71.12" y="106.68" size="1.27" layer="96" display="off"/>
</instance>
<instance part="IC6" gate="G$2" x="71.12" y="45.72" smashed="yes">
<attribute name="NAME" x="64.77" y="52.07" size="1.778" layer="95"/>
</instance>
<instance part="GND10" gate="1" x="71.12" y="91.44" smashed="yes">
<attribute name="VALUE" x="71.12" y="91.186" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R26" gate="G$1" x="86.36" y="106.68" smashed="yes">
<attribute name="NAME" x="82.55" y="108.1786" size="1.778" layer="95"/>
<attribute name="VALUE" x="82.55" y="103.378" size="1.778" layer="96"/>
<attribute name="DIGIKEY" x="86.36" y="106.68" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="86.36" y="106.68" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="86.36" y="106.68" size="1.27" layer="96" display="off"/>
</instance>
<instance part="D4" gate="G$1" x="93.98" y="101.6" smashed="yes">
<attribute name="NAME" x="90.551" y="97.028" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="95.885" y="97.028" size="1.778" layer="96" font="vector" rot="R90" align="top-left"/>
<attribute name="DIGIKEY" x="93.98" y="101.6" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="93.98" y="101.6" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="93.98" y="101.6" size="1.27" layer="96" display="off"/>
</instance>
<instance part="GND42" gate="1" x="93.98" y="91.44" smashed="yes">
<attribute name="VALUE" x="93.98" y="91.186" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="FRAME16" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="FRAME17" gate="G$2" x="162.56" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="175.26" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="248.92" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="180.34" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="D5" gate="G$1" x="139.7" y="93.98" smashed="yes">
<attribute name="NAME" x="132.08" y="93.98" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="147.32" y="93.98" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
<attribute name="DIGIKEY" x="139.7" y="93.98" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="139.7" y="93.98" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="139.7" y="93.98" size="1.27" layer="96" display="off"/>
</instance>
<instance part="R27" gate="G$1" x="139.7" y="78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="138.2014" y="74.93" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="143.002" y="74.93" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="139.7" y="78.74" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="139.7" y="78.74" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="139.7" y="78.74" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="D6" gate="G$1" x="170.18" y="93.98" smashed="yes">
<attribute name="NAME" x="162.56" y="93.98" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="177.8" y="93.98" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
<attribute name="DIGIKEY" x="170.18" y="93.98" size="1.27" layer="96" display="off"/>
<attribute name="MF" x="170.18" y="93.98" size="1.27" layer="96" display="off"/>
<attribute name="MPN" x="170.18" y="93.98" size="1.27" layer="96" display="off"/>
</instance>
<instance part="R28" gate="G$1" x="170.18" y="78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="168.6814" y="74.93" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="173.482" y="74.93" size="1.778" layer="96" rot="R90"/>
<attribute name="DIGIKEY" x="170.18" y="78.74" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="170.18" y="78.74" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="170.18" y="78.74" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="3V3" class="0">
<segment>
<wire x1="63.5" y1="109.22" x2="60.96" y2="109.22" width="0.1524" layer="91"/>
<label x="60.96" y="109.22" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="IC6" gate="G$1" pin="-IN1"/>
</segment>
<segment>
<pinref part="IC6" gate="G$1" pin="+V"/>
<wire x1="71.12" y1="111.76" x2="71.12" y2="114.3" width="0.1524" layer="91"/>
<label x="71.12" y="114.3" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="SAFETY_OK_DRIVEN" class="0">
<segment>
<wire x1="63.5" y1="104.14" x2="60.96" y2="104.14" width="0.1524" layer="91"/>
<label x="60.96" y="104.14" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="IC6" gate="G$1" pin="+IN1"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="GND10" gate="1" pin="GND"/>
<pinref part="IC6" gate="G$1" pin="-V"/>
<wire x1="71.12" y1="93.98" x2="71.12" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND42" gate="1" pin="GND"/>
<pinref part="D4" gate="G$1" pin="C"/>
<wire x1="93.98" y1="93.98" x2="93.98" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="OUT1"/>
<pinref part="R26" gate="G$1" pin="1"/>
<wire x1="81.28" y1="106.68" x2="78.74" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="A"/>
<pinref part="R26" gate="G$1" pin="2"/>
<wire x1="93.98" y1="104.14" x2="93.98" y2="106.68" width="0.1524" layer="91"/>
<wire x1="93.98" y1="106.68" x2="91.44" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="C"/>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="139.7" y1="83.82" x2="139.7" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BATTERY_STATUS_P" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="A"/>
<wire x1="139.7" y1="99.06" x2="139.7" y2="104.14" width="0.1524" layer="91"/>
<label x="139.7" y="104.14" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="BATTERY_STATUS_N" class="0">
<segment>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="139.7" y1="73.66" x2="139.7" y2="68.58" width="0.1524" layer="91"/>
<label x="139.7" y="68.58" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="D6" gate="G$1" pin="C"/>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="170.18" y1="83.82" x2="170.18" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="REGULATOR_STATUS_P" class="0">
<segment>
<pinref part="D6" gate="G$1" pin="A"/>
<wire x1="170.18" y1="99.06" x2="170.18" y2="104.14" width="0.1524" layer="91"/>
<label x="170.18" y="104.14" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="REGULATOR_STATUS_N" class="0">
<segment>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="170.18" y1="73.66" x2="170.18" y2="68.58" width="0.1524" layer="91"/>
<label x="170.18" y="68.58" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>

# Power Distribution Board

The primary purpose of this board is to route power through the robot and to provide all required voltages. More advanced features are presented below.

![test](./power_distribution_board_screenshot.png)

Features:
- 4S battery input
- Overvoltage, overcurrent, undervoltage protection
- 12 6V outputs (for servos and Raspberry Pi)
- 10 12V outputs
- 6 raw battery voltage outputs
- Voltage and current monitoring on each rail and logging to SD card
- Battery voltage display
